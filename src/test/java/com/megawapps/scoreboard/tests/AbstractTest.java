/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.tests;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.mvc.MvcFeature;
import org.glassfish.jersey.server.mvc.MvcProperties;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Before;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.megawapps.scoreboard.rest.model.ingame.UserInfo;

/**
 * The Class AbstractTest.
 */
public class AbstractTest extends JerseyTest {

	/** The Constant email. */
	protected static final String email = "marin.xavier@gmail.com";

	/** The Constant passwd. */
	protected static final String passwd = "zaza666";

	/** The gson. */
	protected Gson gson;

	/**
	 * Inits the.
	 */
	@Before
	public void init() {
		gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
	}

	/**
	 * Login.
	 * 
	 * @return the user
	 */
	protected UserInfo login() {
		final Form form = new Form();
		form.param("login", email);
		form.param("passwd", passwd);
		form.param("idgame", "1");
		return target("mobile/login").register(JacksonFeature.class).request(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), UserInfo.class);
	}

	/**
	 * Login.
	 * 
	 * @param email
	 *            the email
	 * @param passwd
	 *            the passwd
	 * @return the user info
	 */
	protected UserInfo login(final String email, final String passwd) {
		final Form form = new Form();
		form.param("login", email);
		form.param("passwd", passwd);
		form.param("idgame", "1");
		return target("mobile/login").register(JacksonFeature.class).request(MediaType.APPLICATION_JSON_TYPE).post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), UserInfo.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.glassfish.jersey.test.JerseyTest#configure()
	 */
	@Override
	protected Application configure() {
		enable(TestProperties.LOG_TRAFFIC);
		enable(TestProperties.DUMP_ENTITY);
		final ResourceConfig rc = new ResourceConfig();
		rc.packages("com.megawapps.scoreboard.rest");
		rc.property(MvcProperties.TEMPLATE_BASE_PATH, "");
		rc.register(MultiPartFeature.class);
		rc.register(MvcFeature.class);
		rc.register(new JacksonFeature());
		return rc;
	}
}
