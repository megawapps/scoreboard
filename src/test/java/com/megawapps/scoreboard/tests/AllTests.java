/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * The Class AllTests.
 */
@RunWith(Suite.class)
@SuiteClasses({ LoginTest.class, NewBestScoreTest.class })
public class AllTests {

}
