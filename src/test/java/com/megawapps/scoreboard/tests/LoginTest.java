/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.tests;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.junit.Test;

import com.megawapps.scoreboard.dao.UserManagerDao;
import com.megawapps.scoreboard.rest.model.ingame.UserInfo;

/**
 * The Class LoginTest.
 * 
 * @author xavier
 */
public class LoginTest extends AbstractTest {

	/**
	 * Test.
	 * 
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void loginTest() throws JsonParseException, JsonMappingException, IOException {
		final UserInfo u = login();
		assertEquals(email, u.getEmail());
	}

	/**
	 * Login failed test.
	 * 
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void loginFailedTest() throws JsonParseException, JsonMappingException, IOException {
		final UserInfo u = login("toto@free.fr", "123456");
		assertEquals("unknown.login", u.getErrorMap().get("error.code"));
	}

	/**
	 * Register user test.
	 */
	@Test
	public void registerUserTest() {
		final String email = "marin_xavier@yahoo.fr";
		final String passwd = "tutu";
		final Form form = new Form();
		form.param("login", email);
		form.param("passwd", passwd);
		final UserInfo u = target("mobile/register").register(JacksonFeature.class).request(MediaType.APPLICATION_JSON_TYPE)
				.post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE), UserInfo.class);
		assertEquals(email, u.getEmail());
		final UserInfo u2 = login(email, passwd);
		assertEquals(u.getEmail(), u2.getEmail());
		// Del User
		UserManagerDao.getInstance().delete(u2.getIduser());
		final UserInfo u3 = login("tutu@free.fr", "tutu");
		assertEquals("unknown.login", u3.getErrorMap().get("error.code"));
	}
}
