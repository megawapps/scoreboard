/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.junit.Test;

import com.megawapps.scoreboard.dao.ScoreManagerDao;
import com.megawapps.scoreboard.rest.model.ingame.ScoreInfo;

/**
 * The Class NewBestScoreTest.
 * 
 * @author xavier
 */
public class NewBestScoreTest extends AbstractTest {

	/**
	 * New score test.
	 * 
	 * @throws JsonParseException
	 *             the json parse exception
	 * @throws JsonMappingException
	 *             the json mapping exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Test
	public void newScoreTest() throws JsonParseException, JsonMappingException, IOException {
		final BigInteger currentScore = BigInteger.valueOf(Double.valueOf(Math.random() * 1000).longValue());
		final ScoreInfo score = new ScoreInfo();
		score.setCoins(BigInteger.valueOf(500l));
		score.setScore(currentScore);
		score.setScroreDate(new Date());
		final ScoreInfo s = target("mobile/score/1/" + login().getIduser()).register(JacksonFeature.class).request(MediaType.APPLICATION_JSON_TYPE)
				.post(Entity.entity(score, MediaType.APPLICATION_JSON_TYPE), ScoreInfo.class);
		assertEquals(currentScore, s.getScore());
		ScoreManagerDao.getInstance().delete(s.getIdScores());
	}

	/**
	 * List of scores test.
	 */
	@Test
	public void listOfScoresTest() {
		final List<ScoreInfo> s = target("mobile/bestscores/1").register(JacksonFeature.class).request(MediaType.APPLICATION_JSON_TYPE).get(List.class);
		assertTrue(s.size() > 0);
	}
}
