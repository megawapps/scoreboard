/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.beans.missions;

/**
 * The Class MissionEvent.
 */
public class MissionEvent {
	
	/** The nb target shooted. */
	private int nbTargetShooted;
	
	/** The nb bottle shooted. */
	private int nbBottleShooted;
	
	/** The nb life shooted. */
	private int nbLifeShooted;
	
	/** The nb gifts shooted. */
	private int nbGiftsShooted;
	
	/** The nb coins earned. */
	private int nbCoinsEarned;
	
	/** The nb points earned. */
	private int nbPointsEarned;
}
