/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.beans.missions;

/**
 * The Enum Stock.
 */
public enum Stock {
	
	/** The life. */
	LIFE, 
 /** The minigun. */
 MINIGUN, 
 /** The dynamite. */
 DYNAMITE;
}
