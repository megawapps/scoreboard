/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.beans.missions;

/**
 * The Enum Target.
 */
public enum Target {
	
	/** The pie. */
	PIE, 
 /** The bottle. */
 BOTTLE, 
 /** The gift. */
 GIFT, 
 /** The life. */
 LIFE;
}
