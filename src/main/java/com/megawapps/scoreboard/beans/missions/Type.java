/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.beans.missions;

/**
 * The Enum Type.
 */
public enum Type {
	
	/** The coin. */
	COIN, 
 /** The target. */
 TARGET, 
 /** The time. */
 TIME, 
 /** The visit. */
 VISIT, 
 /** The event. */
 EVENT, 
 /** The points. */
 POINTS, 
 /** The stock. */
 STOCK;
}
