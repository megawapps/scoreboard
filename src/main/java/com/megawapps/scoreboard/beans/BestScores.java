/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.beans;

import java.util.List;

import com.megawapps.scoreboard.entities.Game;
import com.megawapps.scoreboard.entities.Score;

/**
 * The Class BestScores.
 */
public class BestScores {

	/** The games. */
	private Game games;

	/** The list of scores. */
	private List<Score> listOfScores;

	/**
	 * Gets the games.
	 * 
	 * @return the games
	 */
	public Game getGames() {
		return games;
	}

	/**
	 * Sets the games.
	 * 
	 * @param games
	 *            the new games
	 */
	public void setGames(final Game games) {
		this.games = games;
	}

	/**
	 * Gets the list of scores.
	 * 
	 * @return the list of scores
	 */
	public List<Score> getListOfScores() {
		return listOfScores;
	}

	/**
	 * Sets the list of scores.
	 * 
	 * @param listOfScores
	 *            the new list of scores
	 */
	public void setListOfScores(final List<Score> listOfScores) {
		this.listOfScores = listOfScores;
	}
}
