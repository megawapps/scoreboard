/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.tools;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * The Class EntityManagerUtil.
 * 
 * @author Giwi Softwares
 */
public class EntityManagerUtil {

	/** The Constant EMF_INSTANCE. */
	private static final EntityManagerFactory EMF_INSTANCE = Persistence.createEntityManagerFactory("scoreboard");

	/**
	 * Constructeur non instanciable.
	 */
	private EntityManagerUtil() {
		// vide exprès
	}

	/**
	 * Gets the entity manager.
	 * 
	 * @return EntityManager
	 */
	public static EntityManager getEntityManager() {
		return EMF_INSTANCE.createEntityManager();
	}

	/**
	 * Gets the factory.
	 * 
	 * @return EntityManagerFactory
	 */
	public static EntityManagerFactory getFactory() {
		return EMF_INSTANCE.createEntityManager().getEntityManagerFactory();
	}

}
