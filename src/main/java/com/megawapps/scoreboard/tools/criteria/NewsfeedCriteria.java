/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.tools.criteria;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.megawapps.scoreboard.entities.Newsfeed;
import com.megawapps.scoreboard.entities.Newsfeed_;
import com.megawapps.scoreboard.tools.paging.EntityPaging;
import com.megawapps.scoreboard.tools.paging.EntityPaging.Type;
import com.megawapps.scoreboard.tools.paging.NewsfeedFirstMaxPaging;
import com.megawapps.scoreboard.tools.paging.NewsfeedIdInPaging;

/**
 * The Class NewsfeedCriteria.
 */
public class NewsfeedCriteria extends GenericCriteria {

	/**
	 * The Constructor.
	 * 
	 * @param pageSize
	 *            the page size
	 */
	public NewsfeedCriteria(final int pageSize) {
		super();
		setPageSize(pageSize);
	}

	/**
	 * The Constructor.
	 */
	public NewsfeedCriteria() {
		super();
	}

	/**
	 * Creates the query.
	 * 
	 * @param emf
	 *            the emf
	 * @return the criteria query< game>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Newsfeed> createQuery(final EntityManagerFactory emf) {
		final CriteriaBuilder cb = emf.getCriteriaBuilder();
		final CriteriaQuery<Newsfeed> query = cb.createQuery(Newsfeed.class);
		final Root<Newsfeed> news = query.from(Newsfeed.class);
		return (CriteriaQuery<Newsfeed>) addWhereOrder(cb, query, news, true);
	}

	/**
	 * Creates the query.
	 * 
	 * @param em
	 *            the em
	 * @return the criteria query< game>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Newsfeed> createQuery(final EntityManager em) {
		final CriteriaBuilder cb = em.getCriteriaBuilder();
		final CriteriaQuery<Newsfeed> query = cb.createQuery(Newsfeed.class);
		final Root<Newsfeed> news = query.from(Newsfeed.class);
		return (CriteriaQuery<Newsfeed>) addWhereOrder(cb, query, news, true);
	}

	/**
	 * Creates the id query.
	 * 
	 * @param emf
	 *            the emf
	 * @return the criteria query< number>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Number> createIdQuery(final EntityManagerFactory emf) {
		final CriteriaBuilder cb = emf.getCriteriaBuilder();
		final CriteriaQuery<Number> query = cb.createQuery(Number.class);
		final Root<Newsfeed> news = query.from(Newsfeed.class);
		query.select(news.get(Newsfeed_.idnews));
		return (CriteriaQuery<Number>) addWhereOrder(cb, query, news, true);
	}

	/**
	 * Creates the count query.
	 * 
	 * @param emf
	 *            the emf
	 * @return the criteria query< number>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Number> createCountQuery(final EntityManagerFactory emf) {
		final CriteriaBuilder cb = emf.getCriteriaBuilder();
		final CriteriaQuery<Number> query = cb.createQuery(Number.class);
		final Root<Newsfeed> news = query.from(Newsfeed.class);
		query.select(cb.count(news.get(Newsfeed_.idnews)));
		return (CriteriaQuery<Number>) addWhereOrder(cb, query, news, false);
	}

	/**
	 * Adds the where order.
	 * 
	 * @param cb
	 *            the cb
	 * @param query
	 *            the query
	 * @param news
	 *            the news
	 * @param order
	 *            the order
	 * @return the criteria query
	 */
	private CriteriaQuery<?> addWhereOrder(final CriteriaBuilder cb, final CriteriaQuery<?> query, final Root<Newsfeed> news, final boolean order) {
		final Predicate where = cb.conjunction();
		query.where(where);
		if (order) {
			query.orderBy(cb.asc(news.get(Newsfeed_.idnews)));
		}

		return query;
	}

	/**
	 * Gets the paging.
	 * 
	 * @param emf
	 *            the emf
	 * @return the paging
	 */
	public EntityPaging<Newsfeed> getPaging(final EntityManagerFactory emf) {
		if (getPagingType() != null) {
			final Type type = EntityPaging.Type.valueOf(getPagingType());
			switch (type) {
			case NONE:
				return null;
			case PAGE:
				return new NewsfeedFirstMaxPaging(emf, createQuery(emf), createCountQuery(emf), getPageSize());
			case PAGE_IN:
				return new NewsfeedIdInPaging(emf, createIdQuery(emf), getPageSize());
			}
		}
		return null;
	}

}
