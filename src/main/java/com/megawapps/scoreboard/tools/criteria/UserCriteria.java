/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.tools.criteria;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.megawapps.scoreboard.entities.User;
import com.megawapps.scoreboard.entities.User_;
import com.megawapps.scoreboard.tools.paging.EntityPaging;
import com.megawapps.scoreboard.tools.paging.EntityPaging.Type;
import com.megawapps.scoreboard.tools.paging.UserFirstMaxPaging;
import com.megawapps.scoreboard.tools.paging.UserIdInPaging;

/**
 * The Class UserCriteria.
 * 
 * @author Giwi Softwares
 */
public class UserCriteria extends GenericCriteria {

	/** The pseudo. */
	private String pseudo = "%";

	/** The email. */
	private String email = "%";

	/**
	 * Instantiates a new user criteria.
	 */
	public UserCriteria() {
		super();
	}

	/**
	 * The Constructor.
	 * 
	 * @param pageSize
	 *            the page size
	 */
	public UserCriteria(final int pageSize) {
		super();
		setPageSize(pageSize);
	}

	/**
	 * Gets the pseudo.
	 * 
	 * @return the pseudo
	 */
	public String getPseudo() {
		return pseudo;
	}

	/**
	 * Sets the pseudo.
	 * 
	 * @param pseudo
	 *            the pseudo to set
	 */
	public void setPseudo(final String pseudo) {
		this.pseudo = pseudo;
	}

	/**
	 * Creates the query.
	 * 
	 * @param emf
	 *            the emf
	 * @return the criteria query< user>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<User> createQuery(final EntityManagerFactory emf) {
		final CriteriaBuilder cb = emf.getCriteriaBuilder();
		final CriteriaQuery<User> query = cb.createQuery(User.class);
		final Root<User> user = query.from(User.class);
		return (CriteriaQuery<User>) addWhereOrder(cb, query, user, true);
	}

	/**
	 * Creates the query.
	 * 
	 * @param em
	 *            the em
	 * @return the criteria query< user>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<User> createQuery(final EntityManager em) {
		final CriteriaBuilder cb = em.getCriteriaBuilder();
		final CriteriaQuery<User> query = cb.createQuery(User.class);
		final Root<User> user = query.from(User.class);
		return (CriteriaQuery<User>) addWhereOrder(cb, query, user, true);
	}

	/**
	 * Creates the id query.
	 * 
	 * @param emf
	 *            the emf
	 * @return the criteria query< number>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Number> createIdQuery(final EntityManagerFactory emf) {
		final CriteriaBuilder cb = emf.getCriteriaBuilder();
		final CriteriaQuery<Number> query = cb.createQuery(Number.class);
		final Root<User> user = query.from(User.class);
		query.select(user.get(User_.iduser));
		return (CriteriaQuery<Number>) addWhereOrder(cb, query, user, true);
	}

	/**
	 * Creates the count query.
	 * 
	 * @param emf
	 *            the emf
	 * @return the criteria query< number>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Number> createCountQuery(final EntityManagerFactory emf) {
		final CriteriaBuilder cb = emf.getCriteriaBuilder();
		final CriteriaQuery<Number> query = cb.createQuery(Number.class);
		final Root<User> user = query.from(User.class);
		query.select(cb.count(user.get(User_.iduser)));
		return (CriteriaQuery<Number>) addWhereOrder(cb, query, user, false);
	}

	/**
	 * Adds the where order.
	 * 
	 * @param cb
	 *            the cb
	 * @param query
	 *            the query
	 * @param user
	 *            the user
	 * @param order
	 *            the order
	 * @return the criteria query
	 */
	private CriteriaQuery<?> addWhereOrder(final CriteriaBuilder cb, final CriteriaQuery<?> query, final Root<User> user, final boolean order) {
		Predicate where = cb.conjunction();
		if ((getPseudo() != null) && !getPseudo().isEmpty()) {
			where = cb.and(where, cb.like(user.get(User_.pseudo), getPseudo()));
		}
		if ((getEmail() != null) && !getEmail().isEmpty()) {
			where = cb.and(where, cb.like(user.get(User_.email), getEmail()));
		}
		query.where(where);

		if (order) {
			query.orderBy(cb.asc(user.get(User_.name)));
		}
		return query;
	}

	/**
	 * Gets the paging.
	 * 
	 * @param emf
	 *            the emf
	 * @return the paging
	 */
	public EntityPaging<User> getPaging(final EntityManagerFactory emf) {
		if (getPagingType() != null) {
			final Type type = EntityPaging.Type.valueOf(getPagingType());
			switch (type) {
			case NONE:
				return null;
			case PAGE:
				return new UserFirstMaxPaging(emf, createQuery(emf), createCountQuery(emf), getPageSize());
			case PAGE_IN:
				return new UserIdInPaging(emf, createIdQuery(emf), getPageSize());
			}
		}
		return null;
	}

	/**
	 * Reset.
	 */
	@Override
	public void reset() {
		super.reset();
		pseudo = "%";
	}

	/**
	 * Gets the email.
	 * 
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 * 
	 * @param email
	 *            the email to set
	 */
	public void setEmail(final String email) {
		this.email = email;
	}
}
