/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.tools.criteria;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.megawapps.scoreboard.dao.GameManagerDao;
import com.megawapps.scoreboard.dao.UserManagerDao;
import com.megawapps.scoreboard.entities.Score;
import com.megawapps.scoreboard.entities.Score_;
import com.megawapps.scoreboard.tools.paging.EntityPaging;
import com.megawapps.scoreboard.tools.paging.EntityPaging.Type;
import com.megawapps.scoreboard.tools.paging.ScoreFirstMaxPaging;
import com.megawapps.scoreboard.tools.paging.ScoreIdInPaging;

/**
 * The Class ScoreCriteria.
 * 
 * @author Giwi Softwares
 */
public class ScoreCriteria extends GenericCriteria {

	/** The userid. */
	private Long userid = 0l;

	/** The gameid. */
	private Long gameid = 0l;

	/**
	 * The Constructor.
	 * 
	 * @param pageSize
	 *            the page size
	 */
	public ScoreCriteria(final int pageSize) {
		super();
		setPageSize(pageSize);
	}

	/**
	 * The Constructor.
	 */
	public ScoreCriteria() {
		super();
	}

	/**
	 * Creates the query.
	 * 
	 * @param emf
	 *            the emf
	 * @return the criteria query< Score>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Score> createQuery(final EntityManagerFactory emf) {
		final CriteriaBuilder cb = emf.getCriteriaBuilder();
		final CriteriaQuery<Score> query = cb.createQuery(Score.class);
		final Root<Score> Score = query.from(Score.class);
		return (CriteriaQuery<Score>) addWhereOrder(cb, query, Score, true);
	}

	/**
	 * Creates the query.
	 * 
	 * @param em
	 *            the em
	 * @return the criteria query< Score>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Score> createQuery(final EntityManager em) {
		final CriteriaBuilder cb = em.getCriteriaBuilder();
		final CriteriaQuery<Score> query = cb.createQuery(Score.class);
		final Root<Score> Score = query.from(Score.class);
		return (CriteriaQuery<Score>) addWhereOrder(cb, query, Score, true);
	}

	/**
	 * Creates the id query.
	 * 
	 * @param emf
	 *            the emf
	 * @return the criteria query< number>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Number> createIdQuery(final EntityManagerFactory emf) {
		final CriteriaBuilder cb = emf.getCriteriaBuilder();
		final CriteriaQuery<Number> query = cb.createQuery(Number.class);
		final Root<Score> Score = query.from(Score.class);
		query.select(Score.get(Score_.idscore));
		return (CriteriaQuery<Number>) addWhereOrder(cb, query, Score, true);
	}

	/**
	 * Creates the count query.
	 * 
	 * @param emf
	 *            the emf
	 * @return the criteria query< number>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Number> createCountQuery(final EntityManagerFactory emf) {
		final CriteriaBuilder cb = emf.getCriteriaBuilder();
		final CriteriaQuery<Number> query = cb.createQuery(Number.class);
		final Root<Score> Score = query.from(Score.class);
		query.select(cb.count(Score.get(Score_.idscore)));
		return (CriteriaQuery<Number>) addWhereOrder(cb, query, Score, false);
	}

	/**
	 * Adds the where order.
	 * 
	 * @param cb
	 *            the cb
	 * @param query
	 *            the query
	 * @param score
	 *            the Score
	 * @param order
	 *            the order
	 * @return the criteria query<?>
	 */
	private CriteriaQuery<?> addWhereOrder(final CriteriaBuilder cb, final CriteriaQuery<?> query, final Root<Score> score, final boolean order) {
		Predicate where = cb.conjunction();
		if (userid > 0) {
			where = cb.and(where, cb.equal(score.get(Score_.user), UserManagerDao.getInstance().find(userid, false)));
		}
		if (gameid > 0) {
			where = cb.and(where, cb.equal(score.get(Score_.game), GameManagerDao.getInstance().find(gameid, false)));
		}
		query.where(where);

		if (order) {
			query.orderBy(cb.desc(score.get(Score_.score)));
		}

		return query;
	}

	/**
	 * Gets the paging.
	 * 
	 * @param emf
	 *            the emf
	 * @return the paging
	 */
	public EntityPaging<Score> getPaging(final EntityManagerFactory emf) {
		if (getPagingType() != null) {
			final Type type = EntityPaging.Type.valueOf(getPagingType());

			switch (type) {
			case NONE:
				return null;
			case PAGE:
				return new ScoreFirstMaxPaging(emf, createQuery(emf), createCountQuery(emf), getPageSize());
			case PAGE_IN:
				return new ScoreIdInPaging(emf, createIdQuery(emf), getPageSize());
			}
		}
		return null;
	}

	/**
	 * Reset.
	 */
	@Override
	public void reset() {
		super.reset();
		gameid = 0l;
		userid = 0l;
	}

	/**
	 * Gets the userid.
	 * 
	 * @return the userid
	 */
	public Long getUserid() {
		return userid;
	}

	/**
	 * Sets the userid.
	 * 
	 * @param userid
	 *            the new userid
	 */
	public void setUserid(final Long userid) {
		this.userid = userid;
	}

	/**
	 * Gets the gameid.
	 * 
	 * @return the gameid
	 */
	public Long getGameid() {
		return gameid;
	}

	/**
	 * Sets the gameid.
	 * 
	 * @param gameid
	 *            the new gameid
	 */
	public void setGameid(final Long gameid) {
		this.gameid = gameid;
	}

}
