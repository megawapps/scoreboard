/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.tools.criteria;

/**
 * The Class GenericCriteria.
 */
public class GenericCriteria {
	/** The paging type. */
	private String pagingType = "NONE";

	/** The page size. */
	private int pageSize = 10;

	/**
	 * Reset.
	 */
	public void reset() {
		setPagingType("NONE");
		setPageSize(10);
	}

	/**
	 * Gets the paging type.
	 * 
	 * @return the paging type
	 */
	public String getPagingType() {
		return pagingType;
	}

	/**
	 * Sets the paging type.
	 * 
	 * @param pagingType
	 *            the new paging type
	 */
	public void setPagingType(final String pagingType) {
		this.pagingType = pagingType;
	}

	/**
	 * Gets the page size.
	 * 
	 * @return the page size
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * Sets the page size.
	 * 
	 * @param pageSize
	 *            the new page size
	 */
	public void setPageSize(final int pageSize) {
		this.pageSize = pageSize;
	}
}
