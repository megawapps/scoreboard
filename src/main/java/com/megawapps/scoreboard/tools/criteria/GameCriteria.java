/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.tools.criteria;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.megawapps.scoreboard.entities.Game;
import com.megawapps.scoreboard.entities.Game_;
import com.megawapps.scoreboard.tools.paging.EntityPaging;
import com.megawapps.scoreboard.tools.paging.EntityPaging.Type;
import com.megawapps.scoreboard.tools.paging.GameFirstMaxPaging;
import com.megawapps.scoreboard.tools.paging.GameIdInPaging;

/**
 * The Class GameCriteria.
 * 
 * @author Giwi Softwares
 */
public class GameCriteria extends GenericCriteria {

	/** The name. */
	private String name = "%";

	/** The featured. */
	private boolean featured = false;

	/**
	 * The Constructor.
	 * 
	 * @param pageSize
	 *            the page size
	 */
	public GameCriteria(final int pageSize) {
		super();
		setPageSize(pageSize);
	}

	/**
	 * The Constructor.
	 */
	public GameCriteria() {
		super();
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the name
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Creates the query.
	 * 
	 * @param emf
	 *            the emf
	 * @return the criteria query< game>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Game> createQuery(final EntityManagerFactory emf) {
		final CriteriaBuilder cb = emf.getCriteriaBuilder();
		final CriteriaQuery<Game> query = cb.createQuery(Game.class);
		final Root<Game> game = query.from(Game.class);
		return (CriteriaQuery<Game>) addWhereOrder(cb, query, game, true);
	}

	/**
	 * Creates the query.
	 * 
	 * @param em
	 *            the em
	 * @return the criteria query< game>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Game> createQuery(final EntityManager em) {
		final CriteriaBuilder cb = em.getCriteriaBuilder();
		final CriteriaQuery<Game> query = cb.createQuery(Game.class);
		final Root<Game> game = query.from(Game.class);
		return (CriteriaQuery<Game>) addWhereOrder(cb, query, game, true);
	}

	/**
	 * Creates the id query.
	 * 
	 * @param emf
	 *            the emf
	 * @return the criteria query< number>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Number> createIdQuery(final EntityManagerFactory emf) {
		final CriteriaBuilder cb = emf.getCriteriaBuilder();
		final CriteriaQuery<Number> query = cb.createQuery(Number.class);
		final Root<Game> game = query.from(Game.class);
		query.select(game.get(Game_.idgame));
		return (CriteriaQuery<Number>) addWhereOrder(cb, query, game, true);
	}

	/**
	 * Creates the count query.
	 * 
	 * @param emf
	 *            the emf
	 * @return the criteria query< number>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Number> createCountQuery(final EntityManagerFactory emf) {
		final CriteriaBuilder cb = emf.getCriteriaBuilder();
		final CriteriaQuery<Number> query = cb.createQuery(Number.class);
		final Root<Game> game = query.from(Game.class);
		query.select(cb.count(game.get(Game_.idgame)));
		return (CriteriaQuery<Number>) addWhereOrder(cb, query, game, false);
	}

	/**
	 * Adds the where order.
	 * 
	 * @param cb
	 *            the cb
	 * @param query
	 *            the query
	 * @param game
	 *            the game
	 * @param order
	 *            the order
	 * @return the criteria query<?>
	 */
	private CriteriaQuery<?> addWhereOrder(final CriteriaBuilder cb, final CriteriaQuery<?> query, final Root<Game> game, final boolean order) {
		Predicate where = cb.conjunction();

		if ((getName() != null) && !getName().isEmpty()) {
			where = cb.and(where, cb.like(game.get(Game_.name), getName()));
		}
		if (isFeatured()) {
			where = cb.and(where, cb.equal(game.get(Game_.featured), isFeatured()));
		}
		query.where(where);

		if (order) {
			query.orderBy(cb.asc(game.get(Game_.idgame)));
		}

		return query;
	}

	/**
	 * Gets the paging.
	 * 
	 * @param emf
	 *            the emf
	 * @return the paging
	 */
	public EntityPaging<Game> getPaging(final EntityManagerFactory emf) {
		if (getPagingType() != null) {
			final Type type = EntityPaging.Type.valueOf(getPagingType());

			switch (type) {
			case NONE:
				return null;
			case PAGE:
				return new GameFirstMaxPaging(emf, createQuery(emf), createCountQuery(emf), getPageSize());
			case PAGE_IN:
				return new GameIdInPaging(emf, createIdQuery(emf), getPageSize());
			}
		}
		return null;
	}

	/**
	 * Reset.
	 */
	@Override
	public void reset() {
		super.reset();
		name = "%";
	}

	/**
	 * Checks if is featured.
	 * 
	 * @return the featured
	 */
	public boolean isFeatured() {
		return featured;
	}

	/**
	 * Sets the featured.
	 * 
	 * @param featured
	 *            the featured to set
	 */
	public void setFeatured(final boolean featured) {
		this.featured = featured;
	}
}
