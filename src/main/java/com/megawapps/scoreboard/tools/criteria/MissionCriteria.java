/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.tools.criteria;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.megawapps.scoreboard.entities.Mission;
import com.megawapps.scoreboard.entities.Mission_;
import com.megawapps.scoreboard.tools.paging.EntityPaging;
import com.megawapps.scoreboard.tools.paging.EntityPaging.Type;
import com.megawapps.scoreboard.tools.paging.MissionFirstMaxPaging;
import com.megawapps.scoreboard.tools.paging.MissionIdInPaging;

/**
 * The Class NewsfeedCriteria.
 */
public class MissionCriteria extends GenericCriteria {

	/** The game id. */
	private Long gameId;

	/**
	 * The Constructor.
	 * 
	 * @param pageSize
	 *            the page size
	 */
	public MissionCriteria(final int pageSize) {
		super();
		setPageSize(pageSize);
	}

	/**
	 * The Constructor.
	 */
	public MissionCriteria() {
		super();
	}

	/**
	 * Creates the query.
	 * 
	 * @param emf
	 *            the emf
	 * @return the criteria query< Mission>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Mission> createQuery(final EntityManagerFactory emf) {
		final CriteriaBuilder cb = emf.getCriteriaBuilder();
		final CriteriaQuery<Mission> query = cb.createQuery(Mission.class);
		final Root<Mission> news = query.from(Mission.class);
		return (CriteriaQuery<Mission>) addWhereOrder(cb, query, news, true);
	}

	/**
	 * Creates the query.
	 * 
	 * @param em
	 *            the em
	 * @return the criteria query< Mission>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Mission> createQuery(final EntityManager em) {
		final CriteriaBuilder cb = em.getCriteriaBuilder();
		final CriteriaQuery<Mission> query = cb.createQuery(Mission.class);
		final Root<Mission> news = query.from(Mission.class);
		return (CriteriaQuery<Mission>) addWhereOrder(cb, query, news, true);
	}

	/**
	 * Creates the id query.
	 * 
	 * @param emf
	 *            the emf
	 * @return the criteria query< number>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Number> createIdQuery(final EntityManagerFactory emf) {
		final CriteriaBuilder cb = emf.getCriteriaBuilder();
		final CriteriaQuery<Number> query = cb.createQuery(Number.class);
		final Root<Mission> news = query.from(Mission.class);
		query.select(news.get(Mission_.idmission));
		return (CriteriaQuery<Number>) addWhereOrder(cb, query, news, true);
	}

	/**
	 * Creates the count query.
	 * 
	 * @param emf
	 *            the emf
	 * @return the criteria query< number>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Number> createCountQuery(final EntityManagerFactory emf) {
		final CriteriaBuilder cb = emf.getCriteriaBuilder();
		final CriteriaQuery<Number> query = cb.createQuery(Number.class);
		final Root<Mission> news = query.from(Mission.class);
		query.select(cb.count(news.get(Mission_.idmission)));
		return (CriteriaQuery<Number>) addWhereOrder(cb, query, news, false);
	}

	/**
	 * Adds the where order.
	 * 
	 * @param cb
	 *            the cb
	 * @param query
	 *            the query
	 * @param mission
	 *            the news
	 * @param order
	 *            the order
	 * @return the criteria query
	 */
	private CriteriaQuery<?> addWhereOrder(final CriteriaBuilder cb, final CriteriaQuery<?> query, final Root<Mission> mission, final boolean order) {
		Predicate where = cb.conjunction();
		if ((getGameId() != null) && (getGameId() > 0)) {
			where = cb.and(where, cb.equal(mission.get(Mission_.game), getGameId()));
		}
		query.where(where);
		if (order) {
			query.orderBy(cb.asc(mission.get(Mission_.idmission)));
		}

		return query;
	}

	/**
	 * Gets the paging.
	 * 
	 * @param emf
	 *            the emf
	 * @return the paging
	 */
	public EntityPaging<Mission> getPaging(final EntityManagerFactory emf) {
		if (getPagingType() != null) {
			final Type type = EntityPaging.Type.valueOf(getPagingType());
			switch (type) {
			case NONE:
				return null;
			case PAGE:
				return new MissionFirstMaxPaging(emf, createQuery(emf), createCountQuery(emf), getPageSize());
			case PAGE_IN:
				return new MissionIdInPaging(emf, createIdQuery(emf), getPageSize());
			}
		}
		return null;
	}

	/**
	 * Gets the game id.
	 * 
	 * @return the gameId
	 */
	public Long getGameId() {
		return gameId;
	}

	/**
	 * Sets the game id.
	 * 
	 * @param gameId
	 *            the gameId to set
	 */
	public void setGameId(final Long gameId) {
		this.gameId = gameId;
	}

}
