/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.tools.criteria;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.megawapps.scoreboard.dao.GameManagerDao;
import com.megawapps.scoreboard.entities.Storeitem;
import com.megawapps.scoreboard.entities.Storeitem_;
import com.megawapps.scoreboard.tools.paging.EntityPaging;
import com.megawapps.scoreboard.tools.paging.EntityPaging.Type;
import com.megawapps.scoreboard.tools.paging.ShopFirstMaxPaging;
import com.megawapps.scoreboard.tools.paging.ShopIdInPaging;

/**
 * The Class ShopCriteria.
 * 
 * @author Giwi Softwares
 */
public class ShopCriteria extends GenericCriteria {

	/** The title. */
	private String title = "%";

	/** The active. */
	private boolean active = false;

	/** The gameid. */
	private Long gameid = 0l;

	/**
	 * The Constructor.
	 * 
	 * @param pageSize
	 *            the page size
	 */
	public ShopCriteria(final int pageSize) {
		super();
		setPageSize(pageSize);
	}

	/**
	 * The Constructor.
	 */
	public ShopCriteria() {
		super();
	}

	/**
	 * Creates the query.
	 * 
	 * @param emf
	 *            the emf
	 * @return the criteria query< game>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Storeitem> createQuery(final EntityManagerFactory emf) {
		final CriteriaBuilder cb = emf.getCriteriaBuilder();
		final CriteriaQuery<Storeitem> query = cb.createQuery(Storeitem.class);
		final Root<Storeitem> game = query.from(Storeitem.class);
		return (CriteriaQuery<Storeitem>) addWhereOrder(cb, query, game, true);
	}

	/**
	 * Creates the query.
	 * 
	 * @param em
	 *            the em
	 * @return the criteria query< game>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Storeitem> createQuery(final EntityManager em) {
		final CriteriaBuilder cb = em.getCriteriaBuilder();
		final CriteriaQuery<Storeitem> query = cb.createQuery(Storeitem.class);
		final Root<Storeitem> game = query.from(Storeitem.class);
		return (CriteriaQuery<Storeitem>) addWhereOrder(cb, query, game, true);
	}

	/**
	 * Creates the id query.
	 * 
	 * @param emf
	 *            the emf
	 * @return the criteria query< number>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Number> createIdQuery(final EntityManagerFactory emf) {
		final CriteriaBuilder cb = emf.getCriteriaBuilder();
		final CriteriaQuery<Number> query = cb.createQuery(Number.class);
		final Root<Storeitem> game = query.from(Storeitem.class);
		query.select(game.get(Storeitem_.idstoreitem));
		return (CriteriaQuery<Number>) addWhereOrder(cb, query, game, true);
	}

	/**
	 * Creates the count query.
	 * 
	 * @param emf
	 *            the emf
	 * @return the criteria query< number>
	 */
	@SuppressWarnings("unchecked")
	public CriteriaQuery<Number> createCountQuery(final EntityManagerFactory emf) {
		final CriteriaBuilder cb = emf.getCriteriaBuilder();
		final CriteriaQuery<Number> query = cb.createQuery(Number.class);
		final Root<Storeitem> game = query.from(Storeitem.class);
		query.select(cb.count(game.get(Storeitem_.idstoreitem)));
		return (CriteriaQuery<Number>) addWhereOrder(cb, query, game, false);
	}

	/**
	 * Adds the where order.
	 * 
	 * @param cb
	 *            the cb
	 * @param query
	 *            the query
	 * @param storeItem
	 *            the game
	 * @param order
	 *            the order
	 * @return the criteria query<?>
	 */
	private CriteriaQuery<?> addWhereOrder(final CriteriaBuilder cb, final CriteriaQuery<?> query, final Root<Storeitem> storeItem, final boolean order) {
		Predicate where = cb.conjunction();

		if ((getTitle() != null) && !getTitle().isEmpty()) {
			where = cb.and(where, cb.like(storeItem.get(Storeitem_.title), getTitle()));
		}
		if (isActive()) {
			where = cb.and(where, cb.equal(storeItem.get(Storeitem_.active), isActive()));
		}
		if (gameid > 0) {
			where = cb.and(where, cb.equal(storeItem.get(Storeitem_.game), GameManagerDao.getInstance().find(gameid, false)));
		}

		query.where(where);

		if (order) {
			query.orderBy(cb.asc(storeItem.get(Storeitem_.idstoreitem)));
		}

		return query;
	}

	/**
	 * Gets the paging.
	 * 
	 * @param emf
	 *            the emf
	 * @return the paging
	 */
	public EntityPaging<Storeitem> getPaging(final EntityManagerFactory emf) {
		if (getPagingType() != null) {
			final Type type = EntityPaging.Type.valueOf(getPagingType());

			switch (type) {
			case NONE:
				return null;
			case PAGE:
				return new ShopFirstMaxPaging(emf, createQuery(emf), createCountQuery(emf), getPageSize());
			case PAGE_IN:
				return new ShopIdInPaging(emf, createIdQuery(emf), getPageSize());
			}
		}
		return null;
	}

	/**
	 * Reset.
	 */
	@Override
	public void reset() {
		super.reset();
		title = "%";
	}

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the new title
	 */
	public void setTitle(final String title) {
		this.title = title;
	}

	/**
	 * Checks if is the active.
	 * 
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 * 
	 * @param active
	 *            the new active
	 */
	public void setActive(final boolean active) {
		this.active = active;
	}

	/**
	 * Gets the gameid.
	 * 
	 * @return the gameid
	 */
	public Long getGameid() {
		return gameid;
	}

	/**
	 * Sets the gameid.
	 * 
	 * @param gameid
	 *            the new gameid
	 */
	public void setGameid(final Long gameid) {
		this.gameid = gameid;
	}

}
