/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.tools.paging;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;

import com.megawapps.scoreboard.entities.Score;
import com.megawapps.scoreboard.entities.User;

/**
 * Example of paging a collection of {@link User} using id IN queries. This approach uses an initial query to retrieve all of the entity identifiers (generally requires single part keys). Each query
 * for a page uses a separate query for the set of entities using an IN with the page's IDs.
 * 
 * @since EclipseLink 2.4.2
 */
public class ScoreIdInPaging extends EntityPaging<Score> {

	/**
	 * A named query is used with result caching enabled to minimize retrieving the same page of entities multiple times.
	 */
	private static final String QUERY_NAME = "Score.idsIn";

	/** The size. */
	private final int size;

	/** The id pages. */
	private final List<List<Number>> idPages = new ArrayList<List<Number>>();

	/**
	 * The Constructor.
	 * 
	 * @param emf
	 *            the emf
	 * @param criteria
	 *            the criteria
	 * @param pageSize
	 *            the page size
	 */
	public ScoreIdInPaging(final EntityManagerFactory emf, final CriteriaQuery<Number> criteria, final int pageSize) {
		super(emf, pageSize);

		final EntityManager em = emf.createEntityManager();
		final List<Number> ids = em.createQuery(criteria).getResultList();
		em.close();
		size = ids.size();

		int start = 0;
		while (start < ids.size()) {
			int end = start + pageSize;
			if (end > size) {
				end = size;
			}
			final List<Number> subList = ids.subList(start, end);
			idPages.add(subList);
			start = end;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.megawapps.scoreboard.tools.paging.EntityPaging#getNumPages()
	 */
	@Override
	public int getNumPages() {
		return idPages.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.megawapps.scoreboard.tools.paging.EntityPaging#size()
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * Retrieve a page of User instances.
	 * 
	 * @param pageNum
	 *            the page num
	 * @return the list< user>
	 */
	@Override
	public List<Score> get(final int pageNum) {
		final List<Number> ids = idPages.get(pageNum - 1);
		final EntityManager em = getEmf().createEntityManager();

		try {
			final TypedQuery<Score> empsQuery = em.createNamedQuery(QUERY_NAME, Score.class);
			empsQuery.setParameter("IDS", ids);
			currentPage = pageNum;
			return empsQuery.getResultList();
		} finally {
			em.close();
		}
	}

}
