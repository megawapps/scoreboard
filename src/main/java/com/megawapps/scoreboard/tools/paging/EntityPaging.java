/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.tools.paging;

import java.util.List;

import javax.persistence.EntityManagerFactory;

/**
 * The Class EntityPaging.
 * 
 * @param <T>
 *            the generic type
 * @author Giwi Softwares
 */
public abstract class EntityPaging<T> {

	/** The emf. */
	private final EntityManagerFactory emf;

	/** The page size. */
	private final int pageSize;

	/** The current page. */
	protected int currentPage = 0;

	/**
	 * The Constructor.
	 * 
	 * @param emf
	 *            the emf
	 * @param pageSize
	 *            the page size
	 */
	protected EntityPaging(final EntityManagerFactory emf, final int pageSize) {
		super();
		this.emf = emf;
		this.pageSize = pageSize;
	}

	/**
	 * Gets the emf.
	 * 
	 * @return the emf
	 */
	protected EntityManagerFactory getEmf() {
		return this.emf;
	}

	/**
	 * Gets the page size.
	 * 
	 * @return the page size
	 */
	public int getPageSize() {
		return this.pageSize;
	}

	/**
	 * Gets the current page.
	 * 
	 * @return the current page
	 */
	public int getCurrentPage() {
		return this.currentPage;
	}

	/**
	 * Gets the num pages.
	 * 
	 * @return the num pages
	 */
	public abstract int getNumPages();

	/**
	 * Retrieve a page of Employee instances.
	 * 
	 * @param page
	 *            the page
	 * @return the list< t>
	 */
	public abstract List<T> get(int page);

	/**
	 * Size.
	 * 
	 * @return the int
	 */
	public abstract int size();

	/**
	 * Checks for next.
	 * 
	 * @return true, if checks for next
	 */
	public boolean hasNext() {
		return getCurrentPage() < getNumPages();
	}

	/**
	 * Next.
	 * 
	 * @return the list< t>
	 */
	public List<T> next() {
		if (!hasNext()) {
			throw new IllegalStateException("Next page not available");
		}
		return get(++this.currentPage);
	}

	/**
	 * Checks for previous.
	 * 
	 * @return true, if checks for previous
	 */
	public boolean hasPrevious() {
		return getCurrentPage() > 1;
	}

	/**
	 * Previous.
	 * 
	 * @return the list< t>
	 */
	public List<T> previous() {
		if (!hasPrevious()) {
			throw new IllegalStateException("Previous page not available");
		}
		return get(--this.currentPage);
	}

	/**
	 * The Enum Type.
	 */
	public enum Type {

		/** The none. */
		NONE,
		/** The page. */
		PAGE,
		/** The page in. */
		PAGE_IN
	}
}
