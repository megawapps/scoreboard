/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.tools.paging;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;

import com.megawapps.scoreboard.entities.Score;

/**
 * The Class GameFirstMaxPaging.
 * 
 * @author Giwi Softwares
 */
public class ScoreFirstMaxPaging extends EntityPaging<Score> {

	/** The size. */
	private int size = -1;

	/** The criteria. */
	private final CriteriaQuery<Score> criteria;

	/** The count criteria. */
	private final CriteriaQuery<Number> countCriteria;

	/**
	 * The Constructor.
	 * 
	 * @param emf
	 *            the emf
	 * @param criteria
	 *            the criteria
	 * @param countCriteria
	 *            the count criteria
	 * @param pageSize
	 *            the page size
	 */
	public ScoreFirstMaxPaging(final EntityManagerFactory emf, final CriteriaQuery<Score> criteria, final CriteriaQuery<Number> countCriteria, final int pageSize) {
		super(emf, pageSize);
		this.criteria = criteria;
		this.countCriteria = countCriteria;
	}

	/**
	 * Gets the criteria.
	 * 
	 * @return the criteria
	 */
	public CriteriaQuery<Score> getCriteria() {
		return criteria;
	}

	/**
	 * Gets the count criteria.
	 * 
	 * @return the count criteria
	 */
	public CriteriaQuery<Number> getCountCriteria() {
		return countCriteria;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.megawapps.scoreboard.tools.paging.EntityPaging#getNumPages()
	 */
	@Override
	public int getNumPages() {
		return size() / getPageSize() + (size() % getPageSize() > 0 ? 1 : 0);
	}

	/**
	 * Retrieve a page of Employee instances.
	 * 
	 * @param page
	 *            the page
	 * @return the list< game>
	 */
	@Override
	public List<Score> get(final int page) {
		final EntityManager em = getEmf().createEntityManager();

		try {
			final TypedQuery<Score> empsQuery = em.createQuery(getCriteria());
			final int first = (page - 1) * getPageSize();
			empsQuery.setFirstResult(first);
			empsQuery.setMaxResults(getPageSize());
			currentPage = page;
			return empsQuery.getResultList();
		} finally {
			em.close();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.megawapps.scoreboard.tools.paging.EntityPaging#size()
	 */
	@Override
	public int size() {
		if (size < 0) {
			final EntityManager em = getEmf().createEntityManager();

			try {
				final TypedQuery<Number> countQuery = em.createQuery(getCountCriteria());
				size = countQuery.getSingleResult().intValue();
			} finally {
				em.close();
			}
		}
		return size;
	}

}
