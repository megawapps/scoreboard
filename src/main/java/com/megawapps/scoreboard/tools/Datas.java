/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.tools;

import java.util.Map;

/**
 * The Class Datas.
 * 
 * @author Giwi Softwares
 */
public class Datas {

	/** The datas. */
	private Map<String, String> datas;

	/**
	 * Instantiates a new datas.
	 */
	public Datas() {

	}

	/**
	 * Instantiates a new datas.
	 * 
	 * @param datas
	 *            the datas
	 */
	public Datas(final Map<String, String> datas) {
		this.datas = datas;
	}

	/**
	 * Gets the datas.
	 * 
	 * @return the datas
	 */
	public Map<String, String> getDatas() {
		return datas;
	}

	/**
	 * Sets the datas.
	 * 
	 * @param datas
	 *            the datas
	 */
	public void setDatas(final Map<String, String> datas) {
		this.datas = datas;
	}
}
