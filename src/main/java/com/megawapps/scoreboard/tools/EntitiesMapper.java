/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.tools;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;

import com.megawapps.scoreboard.dao.DozerHelper;
import com.megawapps.scoreboard.dao.GameManagerDao;
import com.megawapps.scoreboard.dao.MailDao;
import com.megawapps.scoreboard.dao.MissionManagerDao;
import com.megawapps.scoreboard.dao.NewsManagerDao;
import com.megawapps.scoreboard.dao.PasswordEncryptionService;
import com.megawapps.scoreboard.dao.RuleManagerDao;
import com.megawapps.scoreboard.dao.ScoreManagerDao;
import com.megawapps.scoreboard.dao.ShopItemManagerDao;
import com.megawapps.scoreboard.dao.UserManagerDao;
import com.megawapps.scoreboard.entities.Game;
import com.megawapps.scoreboard.entities.Mission;
import com.megawapps.scoreboard.entities.Newsfeed;
import com.megawapps.scoreboard.entities.Rule;
import com.megawapps.scoreboard.entities.Score;
import com.megawapps.scoreboard.entities.Storeitem;
import com.megawapps.scoreboard.entities.User;

/**
 * The Class EntitiesMapper.
 * 
 * @author Giwi Softwares
 */
public class EntitiesMapper {
	/** The instance. */
	private static EntitiesMapper INSTANCE;

	/**
	 * Instantiates a new entities mapper.
	 */
	private EntitiesMapper() {
		// vide
	}

	/**
	 * Gets the single instance of EntitiesMapper.
	 * 
	 * @return single instance of EntitiesMapper
	 */
	public static EntitiesMapper getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new EntitiesMapper();
		}
		return INSTANCE;
	}

	/**
	 * Map game.
	 * 
	 * @param datas
	 *            the datas
	 * @return the game
	 */
	public Game mapGame(final Map<String, String> datas) {
		Game game = null;
		final GameManagerDao dao = GameManagerDao.getInstance();
		if (StringUtils.isNotBlank(datas.get("idgame"))) {
			// update
			game = dao.find(Long.valueOf(datas.get("idgame")), true);
			DozerHelper.getInstance().getMapper().map(new Datas(datas), game);
			dao.save(game);
		} else {
			game = DozerHelper.getInstance().getMapper().map(new Datas(datas), Game.class);
			game.setPubdate(new Date());
			game = dao.create(game);
		}
		return game;
	}

	/**
	 * Map user.
	 * 
	 * @param datas
	 *            the datas
	 * @return the user
	 * @throws NoSuchAlgorithmException
	 *             the no such algorithm exception
	 * @throws InvalidKeySpecException
	 *             the invalid key spec exception
	 */
	public User mapUser(final Map<String, String> datas) throws NoSuchAlgorithmException, InvalidKeySpecException {
		User user = null;
		final UserManagerDao dao = UserManagerDao.getInstance();
		if (StringUtils.isNotBlank(datas.get("iduser"))) {
			// update
			user = dao.find(Long.valueOf(datas.get("iduser")), true);
			final byte[] passwd = user.getPasswd();
			DozerHelper.getInstance().getMapper().map(new Datas(datas), user);
			if (StringUtils.isNotBlank(datas.get("passwd"))) {
				final byte[] salt = PasswordEncryptionService.getInstance().generateSalt();
				user.setSalt(salt);
				user.setPasswd(PasswordEncryptionService.getInstance().getEncryptedPassword(datas.get("passwd"), salt));
			} else {
				user.setPasswd(passwd);
			}
			dao.save(user);
		} else {
			final Datas d = new Datas(datas);
			user = DozerHelper.getInstance().getMapper().map(d, User.class);
			user.setCreadate(new Date());
			final byte[] salt = PasswordEncryptionService.getInstance().generateSalt();
			user.setSalt(salt);
			user.setPasswd(PasswordEncryptionService.getInstance().getEncryptedPassword(datas.get("passwd"), salt));
			final String activationcode = UUID.randomUUID().toString().replaceAll("-", ""); //$NON-NLS-1$ //$NON-NLS-2$
			user.setActivationcode(activationcode);
			user.setActive(false);
			user = dao.create(user);
			MailDao.getInstance().sentActivationMail(user, activationcode);
		}
		return user;
	}

	/**
	 * Map news.
	 * 
	 * @param datas
	 *            the datas
	 * @return the newsfeed
	 */
	public Newsfeed mapNews(final Map<String, String> datas) {
		Newsfeed newsfeed = null;
		final NewsManagerDao dao = NewsManagerDao.getInstance();
		if (StringUtils.isNotBlank(datas.get("idnews"))) {
			// update
			newsfeed = dao.find(Long.valueOf(datas.get("idnews")), true);
			DozerHelper.getInstance().getMapper().map(new Datas(datas), newsfeed);
			newsfeed.setUser(UserManagerDao.getInstance().find(Long.valueOf(datas.get("author")), false));
			dao.save(newsfeed);
		} else {
			newsfeed = DozerHelper.getInstance().getMapper().map(new Datas(datas), Newsfeed.class);
			newsfeed.setPubdate(new Date());
			newsfeed = dao.create(newsfeed);
			newsfeed.setUser(UserManagerDao.getInstance().find(Long.valueOf(datas.get("author")), false));
			newsfeed = dao.save(newsfeed);
		}
		return newsfeed;
	}

	/**
	 * Map score.
	 * 
	 * @param datas
	 *            the datas
	 * @return the score
	 */
	public Score mapScore(final Map<String, String> datas) {
		Score score = null;
		final ScoreManagerDao dao = ScoreManagerDao.getInstance();
		if (StringUtils.isNotBlank(datas.get("idscore"))) {
			// update
			score = dao.find(Long.valueOf(datas.get("idscore")), true);
			DozerHelper.getInstance().getMapper().map(new Datas(datas), score);
			score.setUser(UserManagerDao.getInstance().find(Long.valueOf(datas.get("userid")), false));
			score.setGame(GameManagerDao.getInstance().find(Long.valueOf(datas.get("gameid")), false));
			score.setScoredate(new Date());
			dao.save(score);
		} else {
			score = DozerHelper.getInstance().getMapper().map(new Datas(datas), Score.class);
			score.setScoredate(new Date());
			score = dao.create(score);
			score.setUser(UserManagerDao.getInstance().find(Long.valueOf(datas.get("userid")), false));
			score.setGame(GameManagerDao.getInstance().find(Long.valueOf(datas.get("gameid")), false));
			score = dao.save(score);
		}
		return score;
	}

	/**
	 * Map shop item.
	 * 
	 * @param datas
	 *            the datas
	 * @return the store item
	 */
	public Storeitem mapShopItem(final Map<String, String> datas) {
		Storeitem storeItem = null;
		final ShopItemManagerDao dao = ShopItemManagerDao.getInstance();
		if (StringUtils.isNotBlank(datas.get("idstoreitem"))) {
			// update
			storeItem = dao.find(Long.valueOf(datas.get("idstoreitem")), true);
			storeItem.setGame(GameManagerDao.getInstance().find(Long.valueOf(datas.get("idgame")), false));
			DozerHelper.getInstance().getMapper().map(new Datas(datas), storeItem);
			dao.save(storeItem);
		} else {
			storeItem = DozerHelper.getInstance().getMapper().map(new Datas(datas), Storeitem.class);
			storeItem.setGame(GameManagerDao.getInstance().find(Long.valueOf(datas.get("idgame")), false));
			storeItem = dao.create(storeItem);
		}
		return storeItem;
	}

	/**
	 * Map mission.
	 * 
	 * @param datas
	 *            the datas
	 * @return the mission
	 */
	public Mission mapMission(final Map<String, String> datas) {
		Mission mission = null;
		final MissionManagerDao dao = MissionManagerDao.getInstance();
		if (StringUtils.isNotBlank(datas.get("idmission"))) {
			// update
			mission = dao.find(Long.valueOf(datas.get("idmission")), true);
			DozerHelper.getInstance().getMapper().map(new Datas(datas), mission);
			dao.save(mission);
		} else {
			mission = DozerHelper.getInstance().getMapper().map(new Datas(datas), Mission.class);
			mission.setGame(GameManagerDao.getInstance().find(Long.valueOf(datas.get("idgame")), false));
			mission = dao.create(mission);
		}
		return dao.find(mission.getIdmission(), false);
	}

	/**
	 * Map rule.
	 * 
	 * @param datas
	 *            the datas
	 * @return the rule
	 */
	public Rule mapRule(final Map<String, String> datas) {
		Rule rule = null;
		final RuleManagerDao dao = RuleManagerDao.getInstance();
		if (StringUtils.isNotBlank(datas.get("idrule"))) {
			// update
			rule = dao.find(Long.valueOf(datas.get("idrule")), true);
			rule.setMission(MissionManagerDao.getInstance().find(rule.getMission().getIdmission(), false));
			DozerHelper.getInstance().getMapper().map(new Datas(datas), rule);
			dao.save(rule);
		} else {
			rule = DozerHelper.getInstance().getMapper().map(new Datas(datas), Rule.class);
			rule.setMission(MissionManagerDao.getInstance().find(rule.getMission().getIdmission(), false));
			rule = dao.create(rule);
		}
		return rule;
	}
}
