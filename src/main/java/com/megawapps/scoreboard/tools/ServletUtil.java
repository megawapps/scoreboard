/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.tools;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Mode;

/**
 * The Class ServletUtil.
 * 
 * @author Giwi Softwares
 */
public class ServletUtil {

	/** The instance. */
	private static ServletUtil INSTANCE;

	/**
	 * Instantiates a new servlet util.
	 */
	private ServletUtil() {
		// vide
	}

	/**
	 * Gets the single instance of ServletUtil.
	 * 
	 * @return single instance of ServletUtil
	 */
	public static ServletUtil getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ServletUtil();
		}
		return INSTANCE;
	}

	/**
	 * Parses the datas.
	 * 
	 * @param multiPart
	 *            the multi part
	 * @return the map
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public Map<String, String> parseDatas(final FormDataMultiPart multiPart) throws FileNotFoundException, IOException {
		final Map<String, String> datas = new HashMap<String, String>();

		final Iterator<String> iter = multiPart.getFields().keySet().iterator();
		while (iter.hasNext()) {
			final String key = iter.next();
			if ("avatar".equals(key) || "picture".equals(key)) {

				// traitement des images
				final String id = UUID.randomUUID().toString();
				final FormDataBodyPart f = multiPart.getField(key);
				final String fileName = id + ".png";
				final String dest = System.getProperty("user.dir") + "/scoreboardpics/" + fileName;
				final File tmpFile = new File(System.getProperty("java.io.tmpdir") + "/" + fileName);
				Streams.copy(f.getValueAs(InputStream.class), new FileOutputStream(tmpFile), true);
				if (tmpFile.length() > 0) {
					if ("avatar".equals(key)) {
						saveAndResizeImage(tmpFile, dest, 140, 140, true);
					} else {
						saveAndResizeImage(tmpFile, dest, 760, 330, false);
					}
					datas.put("picture", new String(Base64.encodeBase64(dest.getBytes())));
				}
				tmpFile.delete();

			} else {
				// traitement du reste
				final List<String> l = new ArrayList<String>();
				for (final FormDataBodyPart b : multiPart.getFields(key)) {
					l.add(b.getValue());
				}
				datas.put(key, StringUtils.join(l, ":"));
			}
		}
		return datas;
	}

	/**
	 * Save and resize image.
	 * 
	 * @param source
	 *            the source
	 * @param dest
	 *            the dest
	 * @param width
	 *            the width
	 * @param height
	 *            the height
	 * @param isSquare
	 *            the is square
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public void saveAndResizeImage(final File source, final String dest, final int width, final int height, final boolean isSquare) throws IOException {
		final BufferedImage originalImage = ImageIO.read(source);

		final BufferedImage targetImage;
		if (isSquare) {
			final BufferedImage tmpTargetImage;
			if (originalImage.getWidth() > originalImage.getHeight()) {
				tmpTargetImage = Scalr.crop(originalImage, originalImage.getHeight(), originalImage.getHeight());
			} else {
				tmpTargetImage = Scalr.crop(originalImage, originalImage.getWidth(), originalImage.getWidth());
			}
			targetImage = Scalr.resize(tmpTargetImage, Mode.FIT_TO_WIDTH, width, height);
		} else {
			targetImage = Scalr.resize(originalImage, Mode.FIT_TO_WIDTH, width, height);
		}
		final File fDest = new File(dest);
		if (!fDest.getParentFile().exists()) {
			fDest.getParentFile().mkdirs();
		}
		ImageIO.write(targetImage, "PNG", fDest);
	}

}
