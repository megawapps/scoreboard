/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.fileupload.FileUploadException;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;

import com.megawapps.scoreboard.dao.AuthManager;
import com.megawapps.scoreboard.dao.DozerHelper;
import com.megawapps.scoreboard.dao.ShopItemManagerDao;
import com.megawapps.scoreboard.dao.UserManagerDao;
import com.megawapps.scoreboard.entities.Shophistory;
import com.megawapps.scoreboard.entities.Storeitem;
import com.megawapps.scoreboard.rest.model.web.User;
import com.megawapps.scoreboard.tools.EntitiesMapper;
import com.megawapps.scoreboard.tools.ServletUtil;
import com.megawapps.scoreboard.tools.paging.EntityPaging;

/**
 * The Class GameManager.
 * 
 * @author Giwi Softwares
 */
@Path("/shop")
public class ShopManager {

	/** The request. */
	@Context
	private HttpServletRequest request;

	/**
	 * Gets the.
	 * 
	 * @param id
	 *            the id
	 * @return the response
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") final long id) {
		final ShopItemManagerDao dao = ShopItemManagerDao.getInstance();
		final Storeitem si = dao.find(id, false);
		return Response.ok(DozerHelper.getInstance().getMapper().map(si, com.megawapps.scoreboard.rest.model.web.StoreItem.class)).build();
	}

	/**
	 * Delete.
	 * 
	 * @param id
	 *            the id
	 * @return the response
	 */
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") final long id) {
		if (!AuthManager.getInstance().isAdmin((User) request.getSession().getAttribute("user"))) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		final ShopItemManagerDao dao = ShopItemManagerDao.getInstance();
		dao.delete(id);
		return Response.ok("ok").build();
	}

	/**
	 * Gets the active.
	 * 
	 * @return the active
	 */
	@GET
	@Path("/active")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getActives() {
		final List<com.megawapps.scoreboard.rest.model.web.StoreItem> listeOfItems = new ArrayList<com.megawapps.scoreboard.rest.model.web.StoreItem>();
		for (final Storeitem si : ShopItemManagerDao.getInstance().getListOfShopItems(true)) {
			listeOfItems.add(DozerHelper.getInstance().getMapper().map(si, com.megawapps.scoreboard.rest.model.web.StoreItem.class));
		}
		final GenericEntity<List<com.megawapps.scoreboard.rest.model.web.StoreItem>> entity = new GenericEntity<List<com.megawapps.scoreboard.rest.model.web.StoreItem>>(listeOfItems) {
		};
		return Response.ok(entity).build();
	}

	/**
	 * Gets the active.
	 * 
	 * @param idgames
	 *            the idgames
	 * @param page
	 *            the page
	 * @return the active
	 */
	@GET
	@Path("/active/{idgames}/{page}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getActivesFromGame(@PathParam("idgames") final long idgames, @PathParam("page") final int page) {
		final ShopItemManagerDao dao = ShopItemManagerDao.getInstance();
		final EntityPaging<Storeitem> paging = dao.getPaging(ShopItemManagerDao.getInstance().getCriteria(idgames));

		final List<com.megawapps.scoreboard.rest.model.web.StoreItem> listOfStoreItem = new ArrayList<com.megawapps.scoreboard.rest.model.web.StoreItem>();
		for (final Storeitem s : paging.get(page)) {
			listOfStoreItem.add(DozerHelper.getInstance().getMapper().map(s, com.megawapps.scoreboard.rest.model.web.StoreItem.class));
		}
		final GenericEntity<List<com.megawapps.scoreboard.rest.model.web.StoreItem>> entity = new GenericEntity<List<com.megawapps.scoreboard.rest.model.web.StoreItem>>(listOfStoreItem) {
		};
		return Response.ok(entity).build();
	}

	/**
	 * Gets the count.
	 * 
	 * @param idgames
	 *            the idgames
	 * @return the count
	 */
	@GET
	@Path("/pages/{idgames}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCount(@PathParam("idgames") final long idgames) {
		final ShopItemManagerDao dao = ShopItemManagerDao.getInstance();
		final EntityPaging<Storeitem> g = dao.getPaging(ShopItemManagerDao.getInstance().getCriteria(idgames));
		return Response.ok(g.getNumPages()).build();
	}

	/**
	 * Gets the.
	 * 
	 * @return the response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response get() {
		final List<com.megawapps.scoreboard.rest.model.web.StoreItem> listeOfItems = new ArrayList<com.megawapps.scoreboard.rest.model.web.StoreItem>();
		for (final Storeitem si : ShopItemManagerDao.getInstance().getListOfShopItems()) {
			listeOfItems.add(DozerHelper.getInstance().getMapper().map(si, com.megawapps.scoreboard.rest.model.web.StoreItem.class));
		}
		final GenericEntity<List<com.megawapps.scoreboard.rest.model.web.StoreItem>> entity = new GenericEntity<List<com.megawapps.scoreboard.rest.model.web.StoreItem>>(listeOfItems) {
		};
		return Response.ok(entity).build();
	}

	/**
	 * New item.
	 * 
	 * @param multiPart
	 *            the multi part
	 * @return the response
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FileUploadException
	 *             the file upload exception
	 */
	@POST
	@Consumes("multipart/form-data")
	@Produces(MediaType.APPLICATION_JSON)
	public Response newItem(final FormDataMultiPart multiPart) throws IOException, FileUploadException {
		if (!AuthManager.getInstance().isAdmin((User) request.getSession().getAttribute("user"))) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		final Storeitem si = EntitiesMapper.getInstance().mapShopItem(ServletUtil.getInstance().parseDatas(multiPart));
		return Response.ok(DozerHelper.getInstance().getMapper().map(si, com.megawapps.scoreboard.rest.model.web.StoreItem.class)).build();
	}

	/**
	 * Additem to cart.
	 * 
	 * @param idstoreItems
	 *            the idstore items
	 * @return the response
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/addItem/{idstoreItems}")
	public Response additemToCart(@PathParam("idstoreItems") final long idstoreItems) {
		if (null == request.getSession().getAttribute("user")) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		final com.megawapps.scoreboard.rest.model.web.StoreItem siw = DozerHelper.getInstance().getMapper()
				.map(ShopItemManagerDao.getInstance().find(idstoreItems, false), com.megawapps.scoreboard.rest.model.web.StoreItem.class);
		final User u = (User) request.getSession().getAttribute("user");
		u.addToCart(siw);
		final GenericEntity<List<com.megawapps.scoreboard.rest.model.web.StoreItem>> entity = new GenericEntity<List<com.megawapps.scoreboard.rest.model.web.StoreItem>>(u.getCart()) {
		};
		return Response.ok(entity).build();
	}

	/**
	 * Remitem from cart.
	 * 
	 * @param idstoreItems
	 *            the idstore items
	 * @return the response
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/removeitem/{idstoreItems}")
	public Response remitemFromCart(@PathParam("idstoreItems") final long idstoreItems) {
		if (null == request.getSession().getAttribute("user")) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		final User u = (User) request.getSession().getAttribute("user");
		u.remFromCart(idstoreItems);
		final GenericEntity<List<com.megawapps.scoreboard.rest.model.web.StoreItem>> entity = new GenericEntity<List<com.megawapps.scoreboard.rest.model.web.StoreItem>>(u.getCart()) {
		};
		return Response.ok(entity).build();
	}

	/**
	 * Validate cart.
	 * 
	 * @return the response
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/validatecart")
	public Response validateCart() {
		if (null == request.getSession().getAttribute("user")) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		final User u = (User) request.getSession().getAttribute("user");
		final com.megawapps.scoreboard.entities.User us = UserManagerDao.getInstance().find(u.getIduser(), false);

		for (final com.megawapps.scoreboard.rest.model.web.StoreItem siw : u.getCart()) {
			final Shophistory sh = new Shophistory();
			sh.setShopdate(new Date());
			sh.setStoreitem(ShopItemManagerDao.getInstance().find(siw.getIdstoreitem(), false));
			sh.setUser(us);
			ShopItemManagerDao.getInstance().getEntityManager().getTransaction().begin();
			ShopItemManagerDao.getInstance().getEntityManager().persist(sh);
			ShopItemManagerDao.getInstance().getEntityManager().flush();
			ShopItemManagerDao.getInstance().getEntityManager().getTransaction().commit();
		}
		u.setCart(new ArrayList<com.megawapps.scoreboard.rest.model.web.StoreItem>());
		final GenericEntity<List<com.megawapps.scoreboard.rest.model.web.StoreItem>> entity = new GenericEntity<List<com.megawapps.scoreboard.rest.model.web.StoreItem>>(u.getCart()) {
		};
		return Response.ok(entity).build();
	}

	/**
	 * Gets the cart.
	 * 
	 * @return the cart
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/cart")
	public Response getCart() {
		if (null == request.getSession().getAttribute("user")) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		final User u = (User) request.getSession().getAttribute("user");
		if (u.getCart() == null) {
			u.setCart(new ArrayList<com.megawapps.scoreboard.rest.model.web.StoreItem>());
		}
		final GenericEntity<List<com.megawapps.scoreboard.rest.model.web.StoreItem>> entity = new GenericEntity<List<com.megawapps.scoreboard.rest.model.web.StoreItem>>(u.getCart()) {
		};
		return Response.ok(entity).build();
	}
}
