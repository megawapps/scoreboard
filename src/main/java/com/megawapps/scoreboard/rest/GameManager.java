/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.fileupload.FileUploadException;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;

import com.megawapps.scoreboard.dao.AuthManager;
import com.megawapps.scoreboard.dao.DozerHelper;
import com.megawapps.scoreboard.dao.GameManagerDao;
import com.megawapps.scoreboard.entities.Game;
import com.megawapps.scoreboard.tools.EntitiesMapper;
import com.megawapps.scoreboard.tools.ServletUtil;

/**
 * The Class GameManager.
 * 
 * @author Giwi Softwares
 */
@Path("/game")
public class GameManager {

	/** The request. */
	@Context
	private HttpServletRequest request;

	/**
	 * Gets the.
	 * 
	 * @param id
	 *            the id
	 * @return the response
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") final long id) {
		final GameManagerDao dao = GameManagerDao.getInstance();
		final Game g = dao.find(id, false);
		return Response.ok(DozerHelper.getInstance().getMapper().map(g, com.megawapps.scoreboard.rest.model.web.Game.class)).build();
	}

	/**
	 * Delete.
	 * 
	 * @param id
	 *            the id
	 * @return the response
	 */
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") final long id) {
		if (!AuthManager.getInstance().isAdmin((com.megawapps.scoreboard.rest.model.web.User) request.getSession().getAttribute("user"))) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		GameManagerDao.getInstance().delete(id);
		return Response.ok("ok").build();
	}

	/**
	 * Gets the featured.
	 * 
	 * @param featured
	 *            the featured
	 * @return the featured
	 */
	@GET
	@Path("/featured")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getFeatured(@PathParam("featured") final boolean featured) {

		List<com.megawapps.scoreboard.rest.model.web.Game> listOfGames = new ArrayList<com.megawapps.scoreboard.rest.model.web.Game>();
		for (Game g : GameManagerDao.getInstance().getListOfGames(true)) {
			listOfGames.add(DozerHelper.getInstance().getMapper().map(g, com.megawapps.scoreboard.rest.model.web.Game.class));
		}
		final GenericEntity<List<com.megawapps.scoreboard.rest.model.web.Game>> entity = new GenericEntity<List<com.megawapps.scoreboard.rest.model.web.Game>>(listOfGames) {
		};

		return Response.ok(entity).build();
	}

	/**
	 * Gets the.
	 * 
	 * @return the response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response get() {
		List<com.megawapps.scoreboard.rest.model.web.Game> listOfGames = new ArrayList<com.megawapps.scoreboard.rest.model.web.Game>();
		for (Game g : GameManagerDao.getInstance().getListOfGames(false)) {
			listOfGames.add(DozerHelper.getInstance().getMapper().map(g, com.megawapps.scoreboard.rest.model.web.Game.class));
		}
		final GenericEntity<List<com.megawapps.scoreboard.rest.model.web.Game>> entity = new GenericEntity<List<com.megawapps.scoreboard.rest.model.web.Game>>(listOfGames) {
		};
		return Response.ok(entity).build();
	}

	/**
	 * New game.
	 * 
	 * @param multiPart
	 *            the multi part
	 * @return the response
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FileUploadException
	 *             the file upload exception
	 */
	@POST
	@Consumes("multipart/form-data")
	@Produces(MediaType.APPLICATION_JSON)
	public Response newGame(final FormDataMultiPart multiPart) throws IOException, FileUploadException {
		if (!AuthManager.getInstance().isAdmin((com.megawapps.scoreboard.rest.model.web.User) request.getSession().getAttribute("user"))) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		final Game g = EntitiesMapper.getInstance().mapGame(ServletUtil.getInstance().parseDatas(multiPart));
		return Response.ok(DozerHelper.getInstance().getMapper().map(g, com.megawapps.scoreboard.rest.model.web.Game.class)).build();
	}
}
