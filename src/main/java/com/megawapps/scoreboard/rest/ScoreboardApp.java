/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest;

import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.mvc.MvcFeature;
import org.glassfish.jersey.server.mvc.MvcProperties;

/**
 * The Class ScoreboardApp.
 */
public class ScoreboardApp extends ResourceConfig {

	/**
	 * Instantiates a new scoreboard app.
	 */
	public ScoreboardApp() {
		packages("com.megawapps.scoreboard.rest");
		property(MvcProperties.TEMPLATE_BASE_PATH, "");
		register(MultiPartFeature.class);
		register(MvcFeature.class);
		register(LoggingFilter.class);
		register(JacksonFeature.class);
	}
}
