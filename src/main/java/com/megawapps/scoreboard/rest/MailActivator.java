/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.megawapps.scoreboard.dao.UserManagerDao;
import com.megawapps.scoreboard.entities.User;

/**
 * The Class MailActivator.
 */
@Path("/mail")
public class MailActivator {

	/**
	 * Gets the.
	 * 
	 * @param id
	 *            the id
	 * @param activationcode
	 *            the activationcode
	 * @return the response
	 * @throws URISyntaxException
	 *             the uRI syntax exception
	 */
	@GET
	@Path("/{iduser}/{activationcode}")
	@Produces(MediaType.TEXT_HTML)
	public Response get(@PathParam("iduser") final long id, @PathParam("activationcode") final String activationcode) throws URISyntaxException {
		final UserManagerDao dao = UserManagerDao.getInstance();
		final User u = dao.find(id, false);
		if ((u != null) && u.getActivationcode().equals(activationcode)) {
			u.setActive(true);
			UserManagerDao.getInstance().save(u);
			return Response.seeOther(new URI("../index.jsp#/account/ok")).build();
		} else {
			return Response.seeOther(new URI("../index.jsp#/account/ko")).build();
		}

	}
}
