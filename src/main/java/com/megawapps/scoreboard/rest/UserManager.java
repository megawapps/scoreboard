/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;

import com.megawapps.scoreboard.dao.AuthManager;
import com.megawapps.scoreboard.dao.DozerHelper;
import com.megawapps.scoreboard.dao.UserManagerDao;
import com.megawapps.scoreboard.entities.User;
import com.megawapps.scoreboard.rest.model.ingame.UserInfo;
import com.megawapps.scoreboard.rest.model.web.Mission;
import com.megawapps.scoreboard.rest.model.web.Shophistory;
import com.megawapps.scoreboard.tools.EntitiesMapper;
import com.megawapps.scoreboard.tools.ServletUtil;
import com.megawapps.scoreboard.tools.criteria.UserCriteria;

/**
 * The Class UserManager.
 * 
 * @author Giwi Softwares
 */
@Path("/users")
public class UserManager {

	/** The Constant VALID_EMAIL_ADDRESS_REGEX. */
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	/** The request. */
	@Context
	private HttpServletRequest request;

	/**
	 * Validate.
	 * 
	 * @param emailStr
	 *            the email str
	 * @return true, if successful
	 */
	private boolean validate(final String emailStr) {
		final Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		return matcher.find();
	}

	/**
	 * Gets the current user.
	 * 
	 * @return the current user
	 */
	@GET
	@Path("/current")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCurrentUser() {
		final com.megawapps.scoreboard.rest.model.web.User u = (com.megawapps.scoreboard.rest.model.web.User) request.getSession().getAttribute("user");
		final UserInfo ui = new UserInfo();
		if (u != null) {
			DozerHelper.getInstance().getMapper().map(UserManagerDao.getInstance().refresh(UserManagerDao.getInstance().find(u.getIduser(), false)), ui);
		} else {
			final Map<String, String> errorMap = new HashMap<String, String>();
			errorMap.put("error", "notlogged");
			ui.setErrorMap(errorMap);
		}
		return Response.ok(ui).build();
	}

	/**
	 * Email uniqueness test.
	 * 
	 * @param email
	 *            the email
	 * @return the response
	 */
	@GET
	@Path("/validate/{email}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response testEmail(@PathParam("email") final String email) {
		final Map<String, String> errorMap = new HashMap<String, String>();
		if (!validate(email.replaceAll("\\[at\\]", "@"))) {
			errorMap.put("error", "format");
			return Response.ok(errorMap).build();
		}
		if (StringUtils.isBlank(email)) {
			errorMap.put("error", "required");
			return Response.ok(errorMap).build();
		}
		final UserCriteria criteria = new UserCriteria();
		criteria.setEmail(email.replaceAll("\\[at\\]", "@"));
		final List<User> lu = UserManagerDao.getInstance().getUsers(criteria);
		if (lu.size() > 0) {
			errorMap.put("error", "exist");
			return Response.ok(errorMap).build();
		}
		errorMap.put("ok", "ok");
		return Response.ok(errorMap).build();
	}

	/**
	 * Gets the.
	 * 
	 * @param id
	 *            the id
	 * @return the response
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") final long id) {
		final UserManagerDao dao = UserManagerDao.getInstance();
		final User u = dao.find(id, false);
		final com.megawapps.scoreboard.rest.model.web.User us = DozerHelper.getInstance().getMapper().map(u, com.megawapps.scoreboard.rest.model.web.User.class);
		us.setMission(new ArrayList<Mission>());
		us.setShophistories(new ArrayList<Shophistory>());
		return Response.ok(us).build();
	}

	/**
	 * Delete.
	 * 
	 * @param id
	 *            the id
	 * @return the response
	 */
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") final long id) {
		if (!AuthManager.getInstance().isAdmin((com.megawapps.scoreboard.rest.model.web.User) request.getSession().getAttribute("user"))) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		UserManagerDao.getInstance().delete(id);
		return Response.ok("ok").build();
	}

	/**
	 * Gets the.
	 * 
	 * @return the response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response get() {
		final List<com.megawapps.scoreboard.rest.model.web.User> listOfUsers = new ArrayList<com.megawapps.scoreboard.rest.model.web.User>();
		for (final User u : UserManagerDao.getInstance().getListOfUsers()) {
			final com.megawapps.scoreboard.rest.model.web.User us = DozerHelper.getInstance().getMapper().map(u, com.megawapps.scoreboard.rest.model.web.User.class);
			us.setMission(new ArrayList<Mission>());
			us.setShophistories(new ArrayList<Shophistory>());
			listOfUsers.add(us);
		}
		final GenericEntity<List<com.megawapps.scoreboard.rest.model.web.User>> entity = new GenericEntity<List<com.megawapps.scoreboard.rest.model.web.User>>(listOfUsers) {
		};
		return Response.ok(entity).build();
	}

	/**
	 * New user.
	 * 
	 * @param multiPart
	 *            the multi part
	 * @return the response
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FileUploadException
	 *             the file upload exception
	 * @throws NoSuchAlgorithmException
	 *             the no such algorithm exception
	 * @throws InvalidKeySpecException
	 *             the invalid key spec exception
	 */
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response newUser(final FormDataMultiPart multiPart) throws IOException, FileUploadException, NoSuchAlgorithmException, InvalidKeySpecException {
		final User u = EntitiesMapper.getInstance().mapUser(ServletUtil.getInstance().parseDatas(multiPart));
		final com.megawapps.scoreboard.rest.model.web.User us = DozerHelper.getInstance().getMapper().map(u, com.megawapps.scoreboard.rest.model.web.User.class);
		us.setMission(new ArrayList<Mission>());
		us.setShophistories(new ArrayList<Shophistory>());
		return Response.ok(us).build();
	}

	/**
	 * New user.
	 * 
	 * @param login
	 *            the login
	 * @param passwd
	 *            the passwd
	 * @return the response
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FileUploadException
	 *             the file upload exception
	 * @throws NoSuchAlgorithmException
	 *             the no such algorithm exception
	 * @throws InvalidKeySpecException
	 *             the invalid key spec exception
	 */
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response newUserFromGames(@FormParam("login") final String login, @FormParam("passwd") final String passwd) throws IOException, FileUploadException, NoSuchAlgorithmException,
			InvalidKeySpecException {
		final Map<String, String> map = new HashMap<String, String>();
		map.put("email", login);
		map.put("passwd", passwd);
		final User u = EntitiesMapper.getInstance().mapUser(map);
		final com.megawapps.scoreboard.rest.model.web.User us = DozerHelper.getInstance().getMapper().map(u, com.megawapps.scoreboard.rest.model.web.User.class);
		us.setMission(new ArrayList<Mission>());
		us.setShophistories(new ArrayList<Shophistory>());
		return Response.ok(us).build();
	}
}
