/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.megawapps.scoreboard.dao.AdminDAO;

/**
 * The Class AdminDash.
 */
@Path("/admin")
public class AdminDash {

	/** The request. */
	@Context
	private HttpServletRequest request;

	/**
	 * Gets the.
	 * 
	 * @return the response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response get() {
		final Map<String, Object> response = new HashMap<String, Object>();
		response.put("nbusers", AdminDAO.getInstance().getUsersCount());
		response.put("ndshop", AdminDAO.getInstance().getShopCount());
		final GenericEntity<Map<String, Object>> entity = new GenericEntity<Map<String, Object>>(response) {
		};
		return Response.ok(entity).build();
	}
}
