/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.fileupload.FileUploadException;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;

import com.megawapps.scoreboard.dao.AuthManager;
import com.megawapps.scoreboard.dao.DozerHelper;
import com.megawapps.scoreboard.dao.NewsManagerDao;
import com.megawapps.scoreboard.entities.Newsfeed;
import com.megawapps.scoreboard.rest.model.web.Mission;
import com.megawapps.scoreboard.rest.model.web.Shophistory;
import com.megawapps.scoreboard.tools.EntitiesMapper;
import com.megawapps.scoreboard.tools.ServletUtil;
import com.megawapps.scoreboard.tools.criteria.NewsfeedCriteria;
import com.megawapps.scoreboard.tools.paging.EntityPaging;

/**
 * The Class NewsManager.
 */
@Path("/news")
public class NewsManager {

	/** The request. */
	@Context
	private HttpServletRequest request;

	/**
	 * Gets the.
	 * 
	 * @param id
	 *            the id
	 * @return the response
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") final long id) {
		return Response.ok(DozerHelper.getInstance().getMapper().map(NewsManagerDao.getInstance().find(id, false), com.megawapps.scoreboard.rest.model.web.Newsfeed.class)).build();
	}

	/**
	 * Delete.
	 * 
	 * @param id
	 *            the id
	 * @return the response
	 */
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") final long id) {
		if (!AuthManager.getInstance().isAdmin((com.megawapps.scoreboard.rest.model.web.User) request.getSession().getAttribute("user"))) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		NewsManagerDao.getInstance().delete(id);
		return Response.ok("ok").build();
	}

	/**
	 * Gets the by game.
	 * 
	 * @param page
	 *            the page
	 * @return the by game
	 */
	@GET
	@Path("/page/{page}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByGame(@PathParam("page") final int page) {
		final NewsManagerDao dao = NewsManagerDao.getInstance();
		final NewsfeedCriteria criteria = new NewsfeedCriteria(5);
		criteria.setPagingType(EntityPaging.Type.PAGE.name());
		final EntityPaging<Newsfeed> paging = dao.getPaging(criteria);

		final List<com.megawapps.scoreboard.rest.model.web.Newsfeed> listOfNews = new ArrayList<com.megawapps.scoreboard.rest.model.web.Newsfeed>();
		for (final Newsfeed n : paging.get(page)) {
			final com.megawapps.scoreboard.rest.model.web.Newsfeed news = DozerHelper.getInstance().getMapper().map(n, com.megawapps.scoreboard.rest.model.web.Newsfeed.class);
			news.getUser().setMission(new ArrayList<Mission>());
			news.getUser().setShophistories(new ArrayList<Shophistory>());
			listOfNews.add(news);
		}
		final GenericEntity<List<com.megawapps.scoreboard.rest.model.web.Newsfeed>> entity = new GenericEntity<List<com.megawapps.scoreboard.rest.model.web.Newsfeed>>(listOfNews) {
		};
		return Response.ok(entity).build();
	}

	/**
	 * Gets the count.
	 * 
	 * @return the count
	 */
	@GET
	@Path("/pages")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCount() {
		final NewsManagerDao dao = NewsManagerDao.getInstance();
		final NewsfeedCriteria criteria = new NewsfeedCriteria(5);
		criteria.setPagingType(EntityPaging.Type.PAGE.name());
		final EntityPaging<Newsfeed> paging = dao.getPaging(criteria);
		return Response.ok(paging.getNumPages()).build();
	}

	/**
	 * Gets the.
	 * 
	 * @return the response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response get() {
		final List<com.megawapps.scoreboard.rest.model.web.Newsfeed> listOfNews = new ArrayList<com.megawapps.scoreboard.rest.model.web.Newsfeed>();
		for (final Newsfeed n : NewsManagerDao.getInstance().getListOfNewsfeed()) {
			final com.megawapps.scoreboard.rest.model.web.Newsfeed news = DozerHelper.getInstance().getMapper().map(n, com.megawapps.scoreboard.rest.model.web.Newsfeed.class);
			news.getUser().setMission(new ArrayList<Mission>());
			news.getUser().setShophistories(new ArrayList<Shophistory>());
			listOfNews.add(news);
		}
		final GenericEntity<List<com.megawapps.scoreboard.rest.model.web.Newsfeed>> entity = new GenericEntity<List<com.megawapps.scoreboard.rest.model.web.Newsfeed>>(listOfNews) {
		};
		return Response.ok(entity).build();
	}

	/**
	 * New news.
	 * 
	 * @param multiPart
	 *            the multi part
	 * @return the response
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FileUploadException
	 *             the file upload exception
	 */
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response newNews(final FormDataMultiPart multiPart) throws IOException, FileUploadException {
		if (!AuthManager.getInstance().isAdmin((com.megawapps.scoreboard.rest.model.web.User) request.getSession().getAttribute("user"))) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		final Newsfeed n = EntitiesMapper.getInstance().mapNews(ServletUtil.getInstance().parseDatas(multiPart));
		return Response.ok(DozerHelper.getInstance().getMapper().map(n, com.megawapps.scoreboard.rest.model.web.Newsfeed.class)).build();
	}
}
