/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;

import com.megawapps.scoreboard.dao.DozerHelper;
import com.megawapps.scoreboard.dao.PasswordEncryptionService;
import com.megawapps.scoreboard.dao.UserManagerDao;
import com.megawapps.scoreboard.entities.User;
import com.megawapps.scoreboard.tools.ServletUtil;
import com.megawapps.scoreboard.tools.criteria.UserCriteria;

/**
 * The Class Login.
 */
@Path("/login")
public class Login {

	/** The request. */
	@Context
	private HttpServletRequest request;

	/**
	 * Logon.
	 * 
	 * @param multiPart
	 *            the multi part
	 * @return the response
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response logon(final FormDataMultiPart multiPart) throws FileNotFoundException, IOException {
		final Map<String, String> params = ServletUtil.getInstance().parseDatas(multiPart);
		final UserCriteria criteria = new UserCriteria();
		final String login = params.get("login");
		final String passwd = params.get("passwd");

		criteria.setEmail(login);
		final List<User> lu = UserManagerDao.getInstance().getUsers(criteria);
		final Map<String, String> errorMap = new HashMap<String, String>();
		if (lu.size() == 0 || StringUtils.isBlank(login) || StringUtils.isBlank(passwd)) {
			errorMap.put("error", "Bad login");
			return Response.ok(errorMap).build();
		}
		final User u = lu.get(0);
		try {
			if (PasswordEncryptionService.getInstance().authenticate(passwd, u.getPasswd(), u.getSalt())) {
				UserManagerDao.getInstance().requestToken(u);
				com.megawapps.scoreboard.rest.model.web.User us = DozerHelper.getInstance().getMapper().map(u, com.megawapps.scoreboard.rest.model.web.User.class);
				if (request != null) {
					request.getSession().setAttribute("user", us);
				}
				return Response.ok(DozerHelper.getInstance().getMapper().map(UserManagerDao.getInstance().refresh(u), com.megawapps.scoreboard.rest.model.web.User.class)).build();
			} else {
				errorMap.put("error", "Bad login");
				return Response.ok(errorMap).build();
			}
		} catch (final NoSuchAlgorithmException e) {
			errorMap.put("error", e.getMessage());
			return Response.ok(errorMap).build();
		} catch (final InvalidKeySpecException e) {
			errorMap.put("error", e.getMessage());
			return Response.ok(errorMap).build();
		}
	}

	/**
	 * Gets the featured.
	 * 
	 * @return the featured
	 * @throws URISyntaxException
	 *             the uRI syntax exception
	 */
	@GET
	@Path("/logoff")
	@Produces(MediaType.APPLICATION_JSON)
	public Response logoff() throws URISyntaxException {
		request.getSession().removeAttribute("user");
		request.getSession().invalidate();
		return Response.ok("{\"mess\":\"done\"}").build();
	}
}
