/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * The Class Translation.
 */
@Path("/translate")
public class Translation {
	/** The request. */
	@Context
	private HttpServletRequest request;

	/**
	 * Gets the.
	 * 
	 * @return the response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response get() {
		final Locale rLocale = request.getLocale();
		final ResourceBundle bundle = ResourceBundle.getBundle("com.megawapps.scoreboard.i18n.text", rLocale);
		final Enumeration<String> keys = bundle.getKeys();
		final Map<String, String> translation = new HashMap<String, String>();
		while (keys.hasMoreElements()) {
			final String key = keys.nextElement();
			translation.put(key, bundle.getString(key));
		}
		return Response.ok(translation).build();
	}
}
