/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.fileupload.FileUploadException;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;

import com.megawapps.scoreboard.dao.AuthManager;
import com.megawapps.scoreboard.dao.DozerHelper;
import com.megawapps.scoreboard.dao.MissionManagerDao;
import com.megawapps.scoreboard.entities.Mission;
import com.megawapps.scoreboard.tools.EntitiesMapper;
import com.megawapps.scoreboard.tools.ServletUtil;
import com.megawapps.scoreboard.tools.criteria.MissionCriteria;
import com.megawapps.scoreboard.tools.paging.EntityPaging;

/**
 * The Class MissionsManager.
 */
@Path("/mission")
public class MissionsManager {
	/** The request. */
	@Context
	private HttpServletRequest request;

	/**
	 * Gets the.
	 * 
	 * @param id
	 *            the id
	 * @return the response
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") final long id) {
		final Mission m = MissionManagerDao.getInstance().refresh(MissionManagerDao.getInstance().find(id, false));
		return Response.ok(DozerHelper.getInstance().getMapper().map(m, com.megawapps.scoreboard.rest.model.web.Mission.class)).build();
	}

	/**
	 * Delete.
	 * 
	 * @param id
	 *            the id
	 * @return the response
	 */
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") final long id) {
		if (!AuthManager.getInstance().isAdmin((com.megawapps.scoreboard.rest.model.web.User) request.getSession().getAttribute("user"))) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		MissionManagerDao.getInstance().delete(id);
		return Response.ok("ok").build();
	}

	/**
	 * Gets the by game.
	 * 
	 * @param page
	 *            the page
	 * @return the by game
	 */
	@GET
	@Path("/page/{page}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByGame(@PathParam("page") final int page) {
		final MissionManagerDao dao = MissionManagerDao.getInstance();
		final MissionCriteria criteria = new MissionCriteria(5);
		criteria.setPagingType(EntityPaging.Type.PAGE.name());
		final EntityPaging<Mission> paging = dao.getPaging(criteria);
		final List<com.megawapps.scoreboard.rest.model.web.Mission> listOfMissions = new ArrayList<com.megawapps.scoreboard.rest.model.web.Mission>();
		for (final Mission m : paging.get(page)) {
			listOfMissions.add(DozerHelper.getInstance().getMapper().map(m, com.megawapps.scoreboard.rest.model.web.Mission.class));
		}
		final GenericEntity<List<com.megawapps.scoreboard.rest.model.web.Mission>> entity = new GenericEntity<List<com.megawapps.scoreboard.rest.model.web.Mission>>(listOfMissions) {
		};
		return Response.ok(entity).build();
	}

	/**
	 * Gets the count.
	 * 
	 * @return the count
	 */
	@GET
	@Path("/pages")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCount() {
		final MissionManagerDao dao = MissionManagerDao.getInstance();
		final MissionCriteria criteria = new MissionCriteria(5);
		criteria.setPagingType(EntityPaging.Type.PAGE.name());
		final EntityPaging<Mission> paging = dao.getPaging(criteria);
		return Response.ok(paging.getNumPages()).build();
	}

	/**
	 * Gets the.
	 * 
	 * @return the response
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response get() {
		final List<com.megawapps.scoreboard.rest.model.web.Mission> listOfMissions = new ArrayList<com.megawapps.scoreboard.rest.model.web.Mission>();
		for (final Mission m : MissionManagerDao.getInstance().getListOfMission()) {
			listOfMissions.add(DozerHelper.getInstance().getMapper().map(m, com.megawapps.scoreboard.rest.model.web.Mission.class));
		}
		final GenericEntity<List<com.megawapps.scoreboard.rest.model.web.Mission>> entity = new GenericEntity<List<com.megawapps.scoreboard.rest.model.web.Mission>>(listOfMissions) {
		};
		return Response.ok(entity).build();
	}

	/**
	 * New news.
	 * 
	 * @param multiPart
	 *            the multi part
	 * @return the response
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FileUploadException
	 *             the file upload exception
	 */
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response newMission(final FormDataMultiPart multiPart) throws IOException, FileUploadException {
		if (!AuthManager.getInstance().isAdmin((com.megawapps.scoreboard.rest.model.web.User) request.getSession().getAttribute("user"))) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		final Mission m = EntitiesMapper.getInstance().mapMission(ServletUtil.getInstance().parseDatas(multiPart));
		return Response.ok(DozerHelper.getInstance().getMapper().map(m, com.megawapps.scoreboard.rest.model.web.Mission.class)).build();
	}
}
