/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.fileupload.FileUploadException;

import com.megawapps.scoreboard.dao.AuthManager;
import com.megawapps.scoreboard.dao.DozerHelper;
import com.megawapps.scoreboard.dao.ScoreManagerDao;
import com.megawapps.scoreboard.entities.Score;
import com.megawapps.scoreboard.rest.model.web.Mission;
import com.megawapps.scoreboard.rest.model.web.Shophistory;
import com.megawapps.scoreboard.tools.paging.EntityPaging;

/**
 * The Class ScoreManager.
 */
@Path("/score")
public class ScoreManager {

	/** The request. */
	@Context
	private HttpServletRequest request;

	/**
	 * Gets the.
	 * 
	 * @param id
	 *            the id
	 * @return the response
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") final long id) {
		final ScoreManagerDao dao = ScoreManagerDao.getInstance();
		final Score s = dao.find(id, false);
		return Response.ok(DozerHelper.getInstance().getMapper().map(s, com.megawapps.scoreboard.rest.model.web.Score.class)).build();
	}

	/**
	 * Gets the by game.
	 * 
	 * @param idgames
	 *            the idgames
	 * @param page
	 *            the page
	 * @return the by game
	 */
	@GET
	@Path("/game/{idgames}/{page}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByGame(@PathParam("idgames") final long idgames, @PathParam("page") final int page) {
		final ScoreManagerDao dao = ScoreManagerDao.getInstance();
		final EntityPaging<Score> paging = dao.getPaging(ScoreManagerDao.getInstance().getCriteria(idgames));

		final List<com.megawapps.scoreboard.rest.model.web.Score> listOfScores = new ArrayList<com.megawapps.scoreboard.rest.model.web.Score>();
		for (final Score s : paging.get(page)) {
			final com.megawapps.scoreboard.rest.model.web.Score score = DozerHelper.getInstance().getMapper().map(s, com.megawapps.scoreboard.rest.model.web.Score.class);
			score.getUser().setMission(new ArrayList<Mission>());
			score.getUser().setShophistories(new ArrayList<Shophistory>());
			listOfScores.add(score);
		}
		final GenericEntity<List<com.megawapps.scoreboard.rest.model.web.Score>> entity = new GenericEntity<List<com.megawapps.scoreboard.rest.model.web.Score>>(listOfScores) {
		};
		return Response.ok(entity).build();
	}

	/**
	 * Gets the count.
	 * 
	 * @param idgame
	 *            the idgame
	 * @return the count
	 */
	@GET
	@Path("/pages/{idgame}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCount(@PathParam("idgame") final long idgame) {
		final ScoreManagerDao dao = ScoreManagerDao.getInstance();
		final EntityPaging<Score> g = dao.getPaging(ScoreManagerDao.getInstance().getCriteria(idgame));
		return Response.ok(g.getNumPages()).build();
	}

	/**
	 * Delete.
	 * 
	 * @param id
	 *            the id
	 * @return the response
	 */
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") final long id) {
		if (!AuthManager.getInstance().isAdmin((com.megawapps.scoreboard.rest.model.web.User) request.getSession().getAttribute("user"))) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		ScoreManagerDao.getInstance().delete(id);
		return Response.ok("ok").build();
	}

	/**
	 * New score.
	 * 
	 * @param s
	 *            the s
	 * @return the response
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FileUploadException
	 *             the file upload exception
	 * @throws NoSuchAlgorithmException
	 *             the no such algorithm exception
	 * @throws InvalidKeySpecException
	 *             the invalid key spec exception
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response newScore(final com.megawapps.scoreboard.rest.model.web.Score s) throws IOException, FileUploadException, NoSuchAlgorithmException, InvalidKeySpecException {
		if (!AuthManager.getInstance().isValid(s.getUser())) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		return Response.ok(
				DozerHelper.getInstance().getMapper()
						.map(ScoreManagerDao.getInstance().create(DozerHelper.getInstance().getMapper().map(s, Score.class)), com.megawapps.scoreboard.rest.model.web.Score.class)).build();
	}
}
