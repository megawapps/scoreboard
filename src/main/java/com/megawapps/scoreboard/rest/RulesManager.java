/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.fileupload.FileUploadException;

import com.megawapps.scoreboard.dao.AuthManager;
import com.megawapps.scoreboard.dao.DozerHelper;
import com.megawapps.scoreboard.dao.MissionManagerDao;
import com.megawapps.scoreboard.dao.RuleManagerDao;
import com.megawapps.scoreboard.entities.Mission;
import com.megawapps.scoreboard.entities.Rule;

/**
 * The Class RulesManager.
 */
@Path("/rules")
public class RulesManager {
	/** The request. */
	@Context
	private HttpServletRequest request;

	/**
	 * Gets the.
	 * 
	 * @param id
	 *            the id
	 * @return the response
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") final long id) {
		final Rule r = RuleManagerDao.getInstance().find(id, false);
		return Response.ok(DozerHelper.getInstance().getMapper().map(r, com.megawapps.scoreboard.rest.model.web.Rule.class)).build();
	}

	/**
	 * Adds the rule.
	 * 
	 * @param r
	 *            the r
	 * @return the response
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FileUploadException
	 *             the file upload exception
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addRule(final com.megawapps.scoreboard.rest.model.web.Rule r) throws IOException, FileUploadException {
		if (!AuthManager.getInstance().isAdmin((com.megawapps.scoreboard.rest.model.web.User) request.getSession().getAttribute("user"))) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		final Mission mission = MissionManagerDao.getInstance().find(r.getIdmission(), false);
		final RuleManagerDao dao = RuleManagerDao.getInstance();
		Rule rule = DozerHelper.getInstance().getMapper().map(r, Rule.class);
		rule.setMission(mission);
		if (r.getIdrule() != null) {
			// update
			rule = dao.find(r.getIdrule(), true);
			DozerHelper.getInstance().getMapper().map(r, rule);
			dao.save(rule);
		} else {
			rule.setMission(MissionManagerDao.getInstance().find(rule.getMission().getIdmission(), false));
			rule = dao.create(rule);
		}
		return Response.ok(DozerHelper.getInstance().getMapper().map(rule, com.megawapps.scoreboard.rest.model.web.Rule.class)).build();
	}

	/**
	 * Delete.
	 * 
	 * @param id
	 *            the id
	 * @return the response
	 */
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") final long id) {
		if (!AuthManager.getInstance().isAdmin((com.megawapps.scoreboard.rest.model.web.User) request.getSession().getAttribute("user"))) {
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		RuleManagerDao.getInstance().delete(id);
		return Response.ok("ok").build();
	}
}
