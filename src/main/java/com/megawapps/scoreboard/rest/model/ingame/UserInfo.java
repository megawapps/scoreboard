/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest.model.ingame;

/**
 * The Class UserInfo.
 * 
 * @author xavier
 */
public class UserInfo extends MobileResponse {

	/** The active. */
	private boolean active;

	/** The avatar. */
	private String avatar;

	/** The email. */
	private String email;

	/** The pseudo. */
	private String pseudo;

	/** The passwd. */
	private byte[] passwd;

	/** The idusers. */
	private Long iduser;

	/** The name. */
	private String name;

	/** The forname. */
	private String forname;

	/** The admin. */
	private boolean admin;
	
	/** The coins. */
	private long coins;
	
	/** The score. */
	private long score;

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Gets the forname.
	 * 
	 * @return the forname
	 */
	public String getForname() {
		return forname;
	}

	/**
	 * Sets the forname.
	 * 
	 * @param forname
	 *            the forname to set
	 */
	public void setForname(final String forname) {
		this.forname = forname;
	}

	/**
	 * Checks if is active.
	 * 
	 * @return true, if is active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 * 
	 * @param active
	 *            the new active
	 */
	public void setActive(final boolean active) {
		this.active = active;
	}

	/**
	 * Gets the avatar.
	 * 
	 * @return the avatar
	 */
	public String getAvatar() {
		return avatar;
	}

	/**
	 * Sets the avatar.
	 * 
	 * @param avatar
	 *            the new avatar
	 */
	public void setAvatar(final String avatar) {
		this.avatar = avatar;
	}

	/**
	 * Gets the email.
	 * 
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 * 
	 * @param email
	 *            the new email
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * Gets the pseudo.
	 * 
	 * @return the pseudo
	 */
	public String getPseudo() {
		return pseudo;
	}

	/**
	 * Sets the pseudo.
	 * 
	 * @param pseudo
	 *            the new pseudo
	 */
	public void setPseudo(final String pseudo) {
		this.pseudo = pseudo;
	}

	/**
	 * Gets the passwd.
	 * 
	 * @return the passwd
	 */
	public byte[] getPasswd() {
		return passwd;
	}

	/**
	 * Sets the passwd.
	 * 
	 * @param passwd
	 *            the new passwd
	 */
	public void setPasswd(final byte[] passwd) {
		this.passwd = passwd;
	}

	/**
	 * Gets the iduser.
	 * 
	 * @return the iduser
	 */
	public Long getIduser() {
		return iduser;
	}

	/**
	 * Sets the iduser.
	 * 
	 * @param iduser
	 *            the new iduser
	 */
	public void setIduser(final Long iduser) {
		this.iduser = iduser;
	}

	/**
	 * Checks if is admin.
	 * 
	 * @return the admin
	 */
	public boolean isAdmin() {
		return admin;
	}

	/**
	 * Sets the admin.
	 * 
	 * @param admin
	 *            the admin to set
	 */
	public void setAdmin(final boolean admin) {
		this.admin = admin;
	}

	/**
	 * Gets the coins.
	 * 
	 * @return the coins
	 */
	public long getCoins() {
		return coins;
	}

	/**
	 * Sets the coins.
	 * 
	 * @param coins
	 *            the coins to set
	 */
	public void setCoins(final long coins) {
		this.coins = coins;
	}

	/**
	 * Gets the score.
	 * 
	 * @return the score
	 */
	public long getScore() {
		return score;
	}

	/**
	 * Sets the score.
	 * 
	 * @param score
	 *            the score to set
	 */
	public void setScore(final long score) {
		this.score = score;
	}

}
