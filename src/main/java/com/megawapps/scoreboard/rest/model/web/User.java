/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest.model.web;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The Class User.
 */
public class User {

	/** The iduser. */
	private Long iduser;

	/** The activationcode. */
	private String activationcode;

	/** The active. */
	private boolean active;

	/** The address. */
	private String address;

	/** The admin. */
	private boolean admin;

	/** The author. */
	private boolean author;

	/** The avatar. */
	private String avatar;

	/** The birthdate. */
	private Date birthdate;

	/** The city. */
	private String city;

	/** The crea date. */
	private Date creadate;

	/** The email. */
	private String email;

	/** The forname. */
	private String forname;

	/** The gender. */
	private String gender;

	/** The name. */
	private String name;

	/** The passwd. */
	private byte[] passwd;

	/** The pseudo. */
	private String pseudo;

	/** The zipcode. */
	private String zipcode;

	/** The shophistories. */
	private List<Shophistory> shophistories;

	/** The tokens. */
	private List<Token> tokens;

	/** The missions. */
	private List<Mission> missions;

	/** The cart. */
	private List<StoreItem> cart;

	/**
	 * Adds the to cart.
	 * 
	 * @param si
	 *            the si
	 */
	public void addToCart(final StoreItem si) {
		if (cart == null) {
			cart = new ArrayList<StoreItem>();
		}
		cart.add(si);
	}

	/**
	 * Gets the iduser.
	 * 
	 * @return the iduser
	 */
	public Long getIduser() {
		return iduser;
	}

	/**
	 * Sets the iduser.
	 * 
	 * @param iduser
	 *            the iduser to set
	 */
	public void setIduser(final Long iduser) {
		this.iduser = iduser;
	}

	/**
	 * Gets the activationcode.
	 * 
	 * @return the activationcode
	 */
	public String getActivationcode() {
		return activationcode;
	}

	/**
	 * Sets the activationcode.
	 * 
	 * @param activationcode
	 *            the activationcode to set
	 */
	public void setActivationcode(final String activationcode) {
		this.activationcode = activationcode;
	}

	/**
	 * Checks if is active.
	 * 
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 * 
	 * @param active
	 *            the active to set
	 */
	public void setActive(final boolean active) {
		this.active = active;
	}

	/**
	 * Gets the address.
	 * 
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 * 
	 * @param address
	 *            the address to set
	 */
	public void setAddress(final String address) {
		this.address = address;
	}

	/**
	 * Checks if is admin.
	 * 
	 * @return the admin
	 */
	public boolean isAdmin() {
		return admin;
	}

	/**
	 * Sets the admin.
	 * 
	 * @param admin
	 *            the admin to set
	 */
	public void setAdmin(final boolean admin) {
		this.admin = admin;
	}

	/**
	 * Checks if is author.
	 * 
	 * @return the author
	 */
	public boolean isAuthor() {
		return author;
	}

	/**
	 * Sets the author.
	 * 
	 * @param author
	 *            the author to set
	 */
	public void setAuthor(final boolean author) {
		this.author = author;
	}

	/**
	 * Gets the avatar.
	 * 
	 * @return the avatar
	 */
	public String getAvatar() {
		return avatar;
	}

	/**
	 * Sets the avatar.
	 * 
	 * @param avatar
	 *            the avatar to set
	 */
	public void setAvatar(final String avatar) {
		this.avatar = avatar;
	}

	/**
	 * Gets the birthdate.
	 * 
	 * @return the birthdate
	 */
	public Date getBirthdate() {
		return birthdate;
	}

	/**
	 * Sets the birthdate.
	 * 
	 * @param birthdate
	 *            the birthdate to set
	 */
	public void setBirthdate(final Date birthdate) {
		this.birthdate = birthdate;
	}

	/**
	 * Gets the city.
	 * 
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 * 
	 * @param city
	 *            the city to set
	 */
	public void setCity(final String city) {
		this.city = city;
	}

	/**
	 * Gets the crea date.
	 * 
	 * @return the creadate
	 */
	public Date getCreadate() {
		return creadate;
	}

	/**
	 * Sets the crea date.
	 * 
	 * @param creadate
	 *            the creadate to set
	 */
	public void setCreadate(final Date creadate) {
		this.creadate = creadate;
	}

	/**
	 * Gets the email.
	 * 
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 * 
	 * @param email
	 *            the email to set
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	/**
	 * Gets the forname.
	 * 
	 * @return the forname
	 */
	public String getForname() {
		return forname;
	}

	/**
	 * Sets the forname.
	 * 
	 * @param forname
	 *            the forname to set
	 */
	public void setForname(final String forname) {
		this.forname = forname;
	}

	/**
	 * Gets the gender.
	 * 
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * Sets the gender.
	 * 
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(final String gender) {
		this.gender = gender;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Gets the passwd.
	 * 
	 * @return the passwd
	 */
	public byte[] getPasswd() {
		return passwd;
	}

	/**
	 * Sets the passwd.
	 * 
	 * @param passwd
	 *            the passwd to set
	 */
	public void setPasswd(final byte[] passwd) {
		this.passwd = passwd;
	}

	/**
	 * Gets the pseudo.
	 * 
	 * @return the pseudo
	 */
	public String getPseudo() {
		return pseudo;
	}

	/**
	 * Sets the pseudo.
	 * 
	 * @param pseudo
	 *            the pseudo to set
	 */
	public void setPseudo(final String pseudo) {
		this.pseudo = pseudo;
	}

	/**
	 * Gets the zipcode.
	 * 
	 * @return the zipcode
	 */
	public String getZipcode() {
		return zipcode;
	}

	/**
	 * Sets the zipcode.
	 * 
	 * @param zipcode
	 *            the zipcode to set
	 */
	public void setZipcode(final String zipcode) {
		this.zipcode = zipcode;
	}

	/**
	 * Gets the shophistories.
	 * 
	 * @return the shophistories
	 */
	public List<Shophistory> getShophistories() {
		return shophistories;
	}

	/**
	 * Sets the shophistories.
	 * 
	 * @param shophistories
	 *            the shophistories to set
	 */
	public void setShophistories(final List<Shophistory> shophistories) {
		this.shophistories = shophistories;
	}

	/**
	 * Gets the tokens.
	 * 
	 * @return the tokens
	 */
	public List<Token> getTokens() {
		return tokens;
	}

	/**
	 * Sets the tokens.
	 * 
	 * @param tokens
	 *            the tokens to set
	 */
	public void setTokens(final List<Token> tokens) {
		this.tokens = tokens;
	}

	/**
	 * Gets the mission.
	 * 
	 * @return the mission
	 */
	public List<Mission> getMission() {
		return missions;
	}

	/**
	 * Sets the mission.
	 * 
	 * @param mission
	 *            the mission to set
	 */
	public void setMission(final List<Mission> mission) {
		missions = mission;
	}

	/**
	 * Gets the cart.
	 * 
	 * @return the cart
	 */
	public List<StoreItem> getCart() {
		return cart;
	}

	/**
	 * Sets the cart.
	 * 
	 * @param cart
	 *            the cart to set
	 */
	public void setCart(final List<StoreItem> cart) {
		this.cart = cart;
	}

	/**
	 * Rem from cart.
	 * 
	 * @param idstoreItems
	 *            the idstore items
	 */
	public void remFromCart(final long idstoreItems) {
		for (int i = 0; i < cart.size(); i++) {
			final StoreItem s = cart.get(i);
			if (s.getIdstoreitem() == idstoreItems) {
				cart.remove(i);
				break;
			}
		}

	}

}