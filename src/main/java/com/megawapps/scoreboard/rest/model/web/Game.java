/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest.model.web;

import java.util.Date;

/**
 * The Class Game.
 */
public class Game {
	
	/** The idgame. */
	private long idgame;
	
	/** The andmarketlink. */
	private String andmarketlink;
	
	/** The applestorelink. */
	private String applestorelink;
	
	/** The coinname. */
	private String coinname;
	
	/** The description. */
	private String description;
	
	/** The featured. */
	private boolean featured;
	
	/** The name. */
	private String name;
	
	/** The picture. */
	private String picture;
	
	/** The plateform. */
	private String plateform;
	
	/** The pubdate. */
	private Date pubdate;

	/**
	 * Gets the idgame.
	 * 
	 * @return the idgame
	 */
	public long getIdgame() {
		return idgame;
	}

	/**
	 * Sets the idgame.
	 * 
	 * @param idgame
	 *            the idgame to set
	 */
	public void setIdgame(final long idgame) {
		this.idgame = idgame;
	}

	/**
	 * Gets the andmarketlink.
	 * 
	 * @return the andmarketlink
	 */
	public String getAndmarketlink() {
		return andmarketlink;
	}

	/**
	 * Sets the andmarketlink.
	 * 
	 * @param andmarketlink
	 *            the andmarketlink to set
	 */
	public void setAndmarketlink(final String andmarketlink) {
		this.andmarketlink = andmarketlink;
	}

	/**
	 * Gets the applestorelink.
	 * 
	 * @return the applestorelink
	 */
	public String getApplestorelink() {
		return applestorelink;
	}

	/**
	 * Sets the applestorelink.
	 * 
	 * @param applestorelink
	 *            the applestorelink to set
	 */
	public void setApplestorelink(final String applestorelink) {
		this.applestorelink = applestorelink;
	}

	/**
	 * Gets the coinname.
	 * 
	 * @return the coinname
	 */
	public String getCoinname() {
		return coinname;
	}

	/**
	 * Sets the coinname.
	 * 
	 * @param coinname
	 *            the coinname to set
	 */
	public void setCoinname(final String coinname) {
		this.coinname = coinname;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the description to set
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Checks if is featured.
	 * 
	 * @return the featured
	 */
	public boolean isFeatured() {
		return featured;
	}

	/**
	 * Sets the featured.
	 * 
	 * @param featured
	 *            the featured to set
	 */
	public void setFeatured(final boolean featured) {
		this.featured = featured;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Gets the picture.
	 * 
	 * @return the picture
	 */
	public String getPicture() {
		return picture;
	}

	/**
	 * Sets the picture.
	 * 
	 * @param picture
	 *            the picture to set
	 */
	public void setPicture(final String picture) {
		this.picture = picture;
	}

	/**
	 * Gets the plateform.
	 * 
	 * @return the plateform
	 */
	public String getPlateform() {
		return plateform;
	}

	/**
	 * Sets the plateform.
	 * 
	 * @param plateform
	 *            the plateform to set
	 */
	public void setPlateform(final String plateform) {
		this.plateform = plateform;
	}

	/**
	 * Gets the pubdate.
	 * 
	 * @return the pubdate
	 */
	public Date getPubdate() {
		return pubdate;
	}

	/**
	 * Sets the pubdate.
	 * 
	 * @param pubdate
	 *            the pubdate to set
	 */
	public void setPubdate(final Date pubdate) {
		this.pubdate = pubdate;
	}

}
