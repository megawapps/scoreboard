/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest.model.web;

/**
 * The Class StoreItem.
 */
public class StoreItem {

	/** The idstore items. */
	private Long idstoreitem;

	/** The active. */
	private boolean active;

	/** The description. */
	private String description;

	/** The picture. */
	private String picture;

	/** The price. */
	private float price;

	/** The title. */
	private String title;

	/** The game. */
	private Game game;

	/**
	 * Gets the idstore items.
	 * 
	 * @return the idstoreitem
	 */
	public Long getIdstoreitem() {
		return idstoreitem;
	}

	/**
	 * Sets the idstore items.
	 * 
	 * @param idstoreitem
	 *            the idstoreitem to set
	 */
	public void setIdstoreitem(final Long idstoreitem) {
		this.idstoreitem = idstoreitem;
	}

	/**
	 * Checks if is active.
	 * 
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Sets the active.
	 * 
	 * @param active
	 *            the active to set
	 */
	public void setActive(final boolean active) {
		this.active = active;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the description to set
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Gets the picture.
	 * 
	 * @return the picture
	 */
	public String getPicture() {
		return picture;
	}

	/**
	 * Sets the picture.
	 * 
	 * @param picture
	 *            the picture to set
	 */
	public void setPicture(final String picture) {
		this.picture = picture;
	}

	/**
	 * Gets the price.
	 * 
	 * @return the price
	 */
	public float getPrice() {
		return price;
	}

	/**
	 * Sets the price.
	 * 
	 * @param price
	 *            the price to set
	 */
	public void setPrice(final float price) {
		this.price = price;
	}

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the title to set
	 */
	public void setTitle(final String title) {
		this.title = title;
	}

	/**
	 * Gets the game.
	 * 
	 * @return the game
	 */
	public Game getGame() {
		return game;
	}

	/**
	 * Sets the game.
	 * 
	 * @param game
	 *            the game to set
	 */
	public void setGame(final Game game) {
		this.game = game;
	}
}
