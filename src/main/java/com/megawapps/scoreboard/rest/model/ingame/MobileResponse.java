/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest.model.ingame;

import java.util.Map;

/**
 * The Class MobileResponse.
 * 
 * @author xavier
 */
public class MobileResponse {

	/** The error map. */
	private Map<String, String> errorMap;

	/**
	 * Gets the error map.
	 * 
	 * @return the errorMap
	 */
	public Map<String, String> getErrorMap() {
		return errorMap;
	}

	/**
	 * Sets the error map.
	 * 
	 * @param errorMap
	 *            the errorMap to set
	 */
	public void setErrorMap(final Map<String, String> errorMap) {
		this.errorMap = errorMap;
	}

}
