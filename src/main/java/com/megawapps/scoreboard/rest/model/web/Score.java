/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest.model.web;

import java.math.BigInteger;
import java.util.Date;

/**
 * The Class Score.
 */
public class Score {
	/** The id scores. */
	private Long idscore;
	/** The coins. */
	private BigInteger coins;
	/** The score. */
	private BigInteger score;
	/** The scrore date. */
	private Date scoredate;
	/** The game. */
	private Game game;
	/** The user. */
	private User user;

	/**
	 * Gets the id scores.
	 * 
	 * @return the idScores
	 */
	public Long getIdscore() {
		return idscore;
	}

	/**
	 * Sets the id scores.
	 * 
	 * @param idScores
	 *            the idScores to set
	 */
	public void setIdscores(final Long idScores) {
		idscore = idScores;
	}

	/**
	 * Gets the coins.
	 * 
	 * @return the coins
	 */
	public BigInteger getCoins() {
		return coins;
	}

	/**
	 * Sets the coins.
	 * 
	 * @param coins
	 *            the coins to set
	 */
	public void setCoins(final BigInteger coins) {
		this.coins = coins;
	}

	/**
	 * Gets the score.
	 * 
	 * @return the score
	 */
	public BigInteger getScore() {
		return score;
	}

	/**
	 * Sets the score.
	 * 
	 * @param score
	 *            the score to set
	 */
	public void setScore(final BigInteger score) {
		this.score = score;
	}

	/**
	 * Gets the scrore date.
	 * 
	 * @return the scoredate
	 */
	public Date getScoredate() {
		return scoredate;
	}

	/**
	 * Sets the scrore date.
	 * 
	 * @param scoreDate
	 *            the new scrore date
	 */
	public void setScoredate(final Date scoreDate) {
		scoredate = scoreDate;
	}

	/**
	 * Gets the game.
	 * 
	 * @return the game
	 */
	public Game getGame() {
		return game;
	}

	/**
	 * Sets the game.
	 * 
	 * @param game
	 *            the game to set
	 */
	public void setGame(final Game game) {
		this.game = game;
	}

	/**
	 * Gets the user.
	 * 
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 * 
	 * @param user
	 *            the user to set
	 */
	public void setUser(final User user) {
		this.user = user;
	}
}
