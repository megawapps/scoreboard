/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest.model.web;

/**
 * The Class Rule.
 */
public class Rule {

	/** The idrules. */
	private Long idrule;
	
	/** The description. */
	private String description;
	
	/** The idmission. */
	private long idmission;

	/**
	 * Gets the idrule.
	 * 
	 * @return the idrule
	 */
	public Long getIdrule() {
		return idrule;
	}

	/**
	 * Sets the idrules.
	 * 
	 * @param idrule
	 *            the new idrules
	 */
	public void setIdrule(final Long idrule) {
		this.idrule = idrule;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the description to set
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Gets the idmission.
	 * 
	 * @return the idmission
	 */
	public long getIdmission() {
		return idmission;
	}

	/**
	 * Sets the idmission.
	 * 
	 * @param idmission
	 *            the idmission to set
	 */
	public void setIdmission(final long idmission) {
		this.idmission = idmission;
	}

}
