/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest.model.web;

import java.util.Date;

/**
 * The Class Shophistory.
 */
public class Shophistory {

	/** The id shop history. */
	private Long idShopHistory;

	/** The shop date. */
	private Date shopDate;

	/** The store item. */
	private StoreItem storeItem;

	// /** The user. */
	// private User user;

	/**
	 * Gets the id shop history.
	 * 
	 * @return the idShopHistory
	 */
	public Long getIdShopHistory() {
		return idShopHistory;
	}

	/**
	 * Sets the id shop history.
	 * 
	 * @param idShopHistory
	 *            the idShopHistory to set
	 */
	public void setIdShopHistory(final Long idShopHistory) {
		this.idShopHistory = idShopHistory;
	}

	/**
	 * Gets the shop date.
	 * 
	 * @return the shopDate
	 */
	public Date getShopDate() {
		return shopDate;
	}

	/**
	 * Sets the shop date.
	 * 
	 * @param shopDate
	 *            the shopDate to set
	 */
	public void setShopDate(final Date shopDate) {
		this.shopDate = shopDate;
	}

	/**
	 * Gets the store item.
	 * 
	 * @return the storeItem
	 */
	public StoreItem getStoreItem() {
		return storeItem;
	}

	/**
	 * Sets the store item.
	 * 
	 * @param storeItem
	 *            the storeItem to set
	 */
	public void setStoreItem(final StoreItem storeItem) {
		this.storeItem = storeItem;
	}
	//
	// /**
	// * Gets the user.
	// *
	// * @return the user
	// */
	// public User getUser() {
	// return user;
	// }
	//
	// /**
	// * Sets the user.
	// *
	// * @param user
	// * the user to set
	// */
	// public void setUser(final User user) {
	// this.user = user;
	// }

}
