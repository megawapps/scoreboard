/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest.model.ingame;

import java.math.BigInteger;
import java.util.Date;

/**
 * The Class ScoreInfo.
 * 
 * @author xavier
 */
public class ScoreInfo {

	/** The id scores. */
	private Long idScores;

	/** The coins. */
	private BigInteger coins;

	/** The score. */
	private BigInteger score;

	/** The scrore date. */
	private Date scroreDate;

	/** The owner. */
	private UserInfo owner;

	/** The gamesave. */
	private String gamesave;

	/**
	 * Gets the coins.
	 * 
	 * @return the coins
	 */
	public BigInteger getCoins() {
		return coins;
	}

	/**
	 * Sets the coins.
	 * 
	 * @param coins
	 *            the new coins
	 */
	public void setCoins(final BigInteger coins) {
		this.coins = coins;
	}

	/**
	 * Gets the score.
	 * 
	 * @return the score
	 */
	public BigInteger getScore() {
		return score;
	}

	/**
	 * Sets the score.
	 * 
	 * @param score
	 *            the new score
	 */
	public void setScore(final BigInteger score) {
		this.score = score;
	}

	/**
	 * Gets the scrore date.
	 * 
	 * @return the scrore date
	 */
	public Date getScroreDate() {
		return scroreDate;
	}

	/**
	 * Sets the scrore date.
	 * 
	 * @param scroreDate
	 *            the new scrore date
	 */
	public void setScroreDate(final Date scroreDate) {
		this.scroreDate = scroreDate;
	}

	/**
	 * Gets the owner.
	 * 
	 * @return the owner
	 */
	public UserInfo getOwner() {
		return owner;
	}

	/**
	 * Sets the owner.
	 * 
	 * @param owner
	 *            the owner to set
	 */
	public void setOwner(final UserInfo owner) {
		this.owner = owner;
	}

	/**
	 * Gets the id scores.
	 * 
	 * @return the idScores
	 */
	public Long getIdScores() {
		return idScores;
	}

	/**
	 * Sets the id scores.
	 * 
	 * @param idScores
	 *            the idScores to set
	 */
	public void setIdScores(final Long idScores) {
		this.idScores = idScores;
	}

	/**
	 * Gets the gamesave.
	 * 
	 * @return the gamesave
	 */
	public String getGamesave() {
		return gamesave;
	}

	/**
	 * Sets the gamesave.
	 * 
	 * @param gamesave
	 *            the gamesave to set
	 */
	public void setGamesave(final String gamesave) {
		this.gamesave = gamesave;
	}

}
