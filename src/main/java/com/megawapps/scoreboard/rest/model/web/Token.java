/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest.model.web;

import java.util.Date;

/**
 * The Class Token.
 */
public class Token {

	/** The idtoken. */
	private Long idtoken;

	/** The end date. */
	private Date enddate;

	/** The value. */
	private String value;

	/**
	 * Gets the idtoken.
	 * 
	 * @return the idtoken
	 */
	public Long getIdtoken() {
		return idtoken;
	}

	/**
	 * Sets the idtoken.
	 * 
	 * @param idtoken
	 *            the idtoken to set
	 */
	public void setIdtoken(final Long idtoken) {
		this.idtoken = idtoken;
	}

	/**
	 * Gets the end date.
	 * 
	 * @return the enddate
	 */
	public Date getEnddate() {
		return enddate;
	}

	/**
	 * Sets the end date.
	 * 
	 * @param endDate
	 *            the new end date
	 */
	public void setEnddate(final Date endDate) {
		enddate = endDate;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets the value.
	 * 
	 * @param value
	 *            the value to set
	 */
	public void setValue(final String value) {
		this.value = value;
	}
}
