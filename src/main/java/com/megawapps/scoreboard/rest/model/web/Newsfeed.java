/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest.model.web;

import java.util.Date;

/**
 * The Class Newsfeed.
 */
public class Newsfeed {

	/** The idnews. */
	private Long idnews;

	/** The content. */
	private String content;

	/** The pub date. */
	private Date pubdate;

	/** The title. */
	private String title;

	/** The user. */
	private User user;

	/**
	 * Gets the idnews.
	 * 
	 * @return the idnews
	 */
	public Long getIdnews() {
		return idnews;
	}

	/**
	 * Sets the idnews.
	 * 
	 * @param idnews
	 *            the idnews to set
	 */
	public void setIdnews(final Long idnews) {
		this.idnews = idnews;
	}

	/**
	 * Gets the content.
	 * 
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Sets the content.
	 * 
	 * @param content
	 *            the content to set
	 */
	public void setContent(final String content) {
		this.content = content;
	}

	/**
	 * Gets the pub date.
	 * 
	 * @return the pubdate
	 */
	public Date getPubdate() {
		return pubdate;
	}

	/**
	 * Sets the pub date.
	 * 
	 * @param pubDate
	 *            the new pub date
	 */
	public void setPubdate(final Date pubDate) {
		pubdate = pubDate;
	}

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the title to set
	 */
	public void setTitle(final String title) {
		this.title = title;
	}

	/**
	 * Gets the user.
	 * 
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 * 
	 * @param user
	 *            the user to set
	 */
	public void setUser(final User user) {
		this.user = user;
	}
}
