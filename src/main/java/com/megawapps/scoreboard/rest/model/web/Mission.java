/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest.model.web;

import java.util.List;

/**
 * The Class Mission.
 */
public class Mission {

	/** The idmissions. */
	private Long idmission;

	/** The description. */
	private String description;

	/** The title. */
	private String title;

	/** The game. */
	private Game game;

	/** The rules. */
	private List<Rule> rules;

	/**
	 * Gets the idmission.
	 * 
	 * @return the idmission
	 */
	public Long getIdmission() {
		return idmission;
	}

	/**
	 * Sets the idmission.
	 * 
	 * @param idmission
	 *            the new idmissions
	 */
	public void setIdmission(final Long idmission) {
		this.idmission = idmission;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the description to set
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the title to set
	 */
	public void setTitle(final String title) {
		this.title = title;
	}

	/**
	 * Gets the game.
	 * 
	 * @return the game
	 */
	public Game getGame() {
		return game;
	}

	/**
	 * Sets the game.
	 * 
	 * @param game
	 *            the game to set
	 */
	public void setGame(final Game game) {
		this.game = game;
	}

	/**
	 * Gets the rules.
	 * 
	 * @return the rules
	 */
	public List<Rule> getRules() {
		return rules;
	}

	/**
	 * Sets the rules.
	 * 
	 * @param rules
	 *            the rules to set
	 */
	public void setRules(final List<Rule> rules) {
		this.rules = rules;
	}

}
