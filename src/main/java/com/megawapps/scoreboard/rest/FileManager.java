/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.lf5.util.StreamUtils;

/**
 * The Class FileManager.
 */
@Path("/file")
public class FileManager {

	/** The context. */
	@javax.ws.rs.core.Context
	ServletContext context;

	/**
	 * Gets the artifact binary content.
	 * 
	 * @param filenameHash
	 *            the filename hash
	 * @return the artifact binary content
	 */
	@GET
	@Path("/{filename}")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getArtifactBinaryContent(@PathParam("filename") final String filenameHash) {
		final String filename = new String(Base64.decodeBase64(filenameHash.getBytes()));

		final StreamingOutput stream = new StreamingOutput() {
			@Override
			public void write(final OutputStream output) throws IOException {
				try {
					final File file = new File(filename);
					if (file.exists()) {
						StreamUtils.copyThenClose(new FileInputStream(file), output);
					} else {
						StreamUtils.copyThenClose(new FileInputStream(new File(context.getRealPath("/imgs/no.jpg"))), output);
					}
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
		};

		return Response.ok(stream, "image/png") // TODO: set content-type of your file
				.header("content-disposition", "attachment; filename = " + filename).build();
	}
}
