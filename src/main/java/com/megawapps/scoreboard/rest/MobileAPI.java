/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.rest;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.lang.StringUtils;

import com.megawapps.scoreboard.dao.DozerHelper;
import com.megawapps.scoreboard.dao.GameManagerDao;
import com.megawapps.scoreboard.dao.MailDao;
import com.megawapps.scoreboard.dao.PasswordEncryptionService;
import com.megawapps.scoreboard.dao.ScoreManagerDao;
import com.megawapps.scoreboard.dao.UserManagerDao;
import com.megawapps.scoreboard.entities.Score;
import com.megawapps.scoreboard.entities.User;
import com.megawapps.scoreboard.rest.model.ingame.ScoreInfo;
import com.megawapps.scoreboard.rest.model.ingame.UserInfo;
import com.megawapps.scoreboard.tools.criteria.UserCriteria;
import com.megawapps.scoreboard.tools.paging.EntityPaging;

/**
 * The Class MobileAPI.
 * 
 * @author xavier
 */
@Path("/mobile")
public class MobileAPI {
	/** The request. */
	@Context
	private HttpServletRequest request;

	/**
	 * Test login.
	 * 
	 * @param login
	 *            the login
	 * @param passwd
	 *            the passwd
	 * @param idgame
	 *            the idgame
	 * @return the response
	 */
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response testLogin(@FormParam("login") final String login, @FormParam("passwd") final String passwd, @FormParam("idgame") final long idgame) {
		final UserInfo ui = new UserInfo();
		final UserCriteria criteria = new UserCriteria();
		criteria.setEmail(login);
		criteria.setPseudo(null);
		final List<User> lu = UserManagerDao.getInstance().getUsers(criteria);
		final Map<String, String> errorMap = new HashMap<String, String>();
		if ((lu.size() == 0) || StringUtils.isBlank(login) || StringUtils.isBlank(passwd)) {
			errorMap.put("error.label", "Unknown login");
			errorMap.put("error.code", "unknown.login");
		} else {
			final User u = lu.get(0);
			try {
				if (PasswordEncryptionService.getInstance().authenticate(passwd, u.getPasswd(), u.getSalt())) {
					DozerHelper.getInstance().getMapper().map(UserManagerDao.getInstance().refresh(u), ui);
					for (final Score s : u.getScores()) {
						if (s.getGame().getIdgame() == idgame) {
							final Score sRefreshed = ScoreManagerDao.getInstance().refresh(s);
							ui.setCoins(sRefreshed.getCoins());
							ui.setScore(sRefreshed.getScore());
						}
					}
				} else {
					errorMap.put("error.label", "Bad login");
					errorMap.put("error.code", "bad.login");
				}
			} catch (final NoSuchAlgorithmException e) {
				errorMap.put("error.label", e.getMessage());
				errorMap.put("error.code", "NoSuchAlgorithmException");
			} catch (final InvalidKeySpecException e) {
				errorMap.put("error.label", e.getMessage());
				errorMap.put("error.code", "InvalidKeySpecException");
			}
		}
		ui.setErrorMap(errorMap);
		return Response.ok(ui).build();
	}

	/**
	 * Register newuser.
	 * 
	 * @param login
	 *            the login
	 * @param passwd
	 *            the passwd
	 * @return the response
	 */
	@POST
	@Path("/register")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public Response registerNewuser(@FormParam("login") final String login, @FormParam("passwd") final String passwd) {
		final Map<String, String> errorMap = new HashMap<String, String>();
		final UserInfo ui = new UserInfo();
		// TODO : blinder l'enregistrement avec un test validité email
		if (UserManagerDao.getInstance().exists(login)) {
			errorMap.put("error.label", "Existing login");
			errorMap.put("error.code", "existing.login");
		} else {
			try {
				User u = new User();
				u.setEmail(login);
				u.setCreadate(new Date());
				byte[] salt;
				salt = PasswordEncryptionService.getInstance().generateSalt();
				u.setSalt(salt);
				u.setPasswd(PasswordEncryptionService.getInstance().getEncryptedPassword(passwd, salt));
				final String activationcode = UUID.randomUUID().toString().replaceAll("-", ""); //$NON-NLS-1$ //$NON-NLS-2$
				u.setActivationcode(activationcode);
				u.setActive(false);
				u = UserManagerDao.getInstance().create(u);
				MailDao.getInstance().sentActivationMail(u, activationcode);
				DozerHelper.getInstance().getMapper().map(UserManagerDao.getInstance().refresh(u), ui);
			} catch (final NoSuchAlgorithmException e) {
				errorMap.put("error.label", e.getMessage());
				errorMap.put("error.code", "NoSuchAlgorithmException");
			} catch (final InvalidKeySpecException e) {
				errorMap.put("error.label", e.getMessage());
				errorMap.put("error.code", "InvalidKeySpecException");
			}
		}
		ui.setErrorMap(errorMap);
		return Response.ok(ui).build();
	}

	/**
	 * New score.
	 * 
	 * @param si
	 *            the si
	 * @param gameid
	 *            the gameid
	 * @param userid
	 *            the userid
	 * @return the response
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 * @throws FileUploadException
	 *             the file upload exception
	 * @throws NoSuchAlgorithmException
	 *             the no such algorithm exception
	 * @throws InvalidKeySpecException
	 *             the invalid key spec exception
	 */
	@POST
	@Path("/score/{gameid}/{userid}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response newScore(final ScoreInfo si, @PathParam("gameid") final long gameid, @PathParam("userid") final long userid) throws IOException, FileUploadException, NoSuchAlgorithmException,
			InvalidKeySpecException {
		final Score s = DozerHelper.getInstance().getMapper().map(si, Score.class);
		s.setUser(UserManagerDao.getInstance().find(userid, false));
		s.setScoredate(new Date());
		s.setGame(GameManagerDao.getInstance().find(gameid, false));
		ScoreManagerDao.getInstance().create(s);
		return Response.ok(DozerHelper.getInstance().getMapper().map(s, ScoreInfo.class)).build();
	}

	/**
	 * Gets the best scores.
	 * 
	 * @param idgame
	 *            the idgames
	 * @return the best scores
	 */
	@GET
	@Path("/bestscores/{idgame}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getbestScores(@PathParam("idgame") final long idgame) {
		final ScoreManagerDao dao = ScoreManagerDao.getInstance();
		final List<ScoreInfo> bestScores = new ArrayList<ScoreInfo>();
		final EntityPaging<Score> paging = dao.getPaging(ScoreManagerDao.getInstance().getCriteria(idgame));
		for (final Score s : paging.get(1)) {
			bestScores.add(DozerHelper.getInstance().getMapper().map(s, ScoreInfo.class));
		}
		final GenericEntity<List<ScoreInfo>> entity = new GenericEntity<List<ScoreInfo>>(bestScores) {
		};
		return Response.ok(entity).build();
	}
}
