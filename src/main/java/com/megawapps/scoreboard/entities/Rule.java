/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the RULE database table.
 * 
 */
@Entity
@Table(name="RULE")
@NamedQuery(name="Rule.findAll", query="SELECT r FROM Rule r")
public class Rule implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The idrule. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IDRULE", unique=true, nullable=false)
	private long idrule;

	/** The description. */
	@Column(name="DESCRIPTION", length=255)
	private String description;

	//bi-directional many-to-one association to Mission
	/** The mission. */
	@ManyToOne
	@JoinColumn(name="MISSION_IDMISSION")
	private Mission mission;

	//bi-directional many-to-one association to Usershasmission
	/** The usershasmissions. */
	@OneToMany(mappedBy="rule", fetch=FetchType.EAGER)
	private List<Usershasmission> usershasmissions;

	/**
	 * Instantiates a new rule.
	 */
	public Rule() {
	}

	/**
	 * Gets the idrule.
	 * 
	 * @return the idrule
	 */
	public long getIdrule() {
		return this.idrule;
	}

	/**
	 * Sets the idrule.
	 * 
	 * @param idrule
	 *            the new idrule
	 */
	public void setIdrule(long idrule) {
		this.idrule = idrule;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the mission.
	 * 
	 * @return the mission
	 */
	public Mission getMission() {
		return this.mission;
	}

	/**
	 * Sets the mission.
	 * 
	 * @param mission
	 *            the new mission
	 */
	public void setMission(Mission mission) {
		this.mission = mission;
	}

	/**
	 * Gets the usershasmissions.
	 * 
	 * @return the usershasmissions
	 */
	public List<Usershasmission> getUsershasmissions() {
		return this.usershasmissions;
	}

	/**
	 * Sets the usershasmissions.
	 * 
	 * @param usershasmissions
	 *            the new usershasmissions
	 */
	public void setUsershasmissions(List<Usershasmission> usershasmissions) {
		this.usershasmissions = usershasmissions;
	}

	/**
	 * Adds the usershasmission.
	 * 
	 * @param usershasmission
	 *            the usershasmission
	 * @return the usershasmission
	 */
	public Usershasmission addUsershasmission(Usershasmission usershasmission) {
		getUsershasmissions().add(usershasmission);
		usershasmission.setRule(this);

		return usershasmission;
	}

	/**
	 * Removes the usershasmission.
	 * 
	 * @param usershasmission
	 *            the usershasmission
	 * @return the usershasmission
	 */
	public Usershasmission removeUsershasmission(Usershasmission usershasmission) {
		getUsershasmissions().remove(usershasmission);
		usershasmission.setRule(null);

		return usershasmission;
	}

}