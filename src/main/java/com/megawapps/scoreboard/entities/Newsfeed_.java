package com.megawapps.scoreboard.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-02-18T23:20:36.529+0100")
@StaticMetamodel(Newsfeed.class)
public class Newsfeed_ {
	public static volatile SingularAttribute<Newsfeed, Long> idnews;
	public static volatile SingularAttribute<Newsfeed, String> content;
	public static volatile SingularAttribute<Newsfeed, Date> pubdate;
	public static volatile SingularAttribute<Newsfeed, String> title;
	public static volatile SingularAttribute<Newsfeed, User> user;
}
