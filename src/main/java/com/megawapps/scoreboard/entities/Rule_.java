package com.megawapps.scoreboard.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-02-18T23:20:36.532+0100")
@StaticMetamodel(Rule.class)
public class Rule_ {
	public static volatile SingularAttribute<Rule, Long> idrule;
	public static volatile SingularAttribute<Rule, String> description;
	public static volatile SingularAttribute<Rule, Mission> mission;
	public static volatile ListAttribute<Rule, Usershasmission> usershasmissions;
}
