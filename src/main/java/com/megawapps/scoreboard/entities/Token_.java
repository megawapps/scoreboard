package com.megawapps.scoreboard.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-02-18T23:20:36.570+0100")
@StaticMetamodel(Token.class)
public class Token_ {
	public static volatile SingularAttribute<Token, Long> idtoken;
	public static volatile SingularAttribute<Token, Date> enddate;
	public static volatile SingularAttribute<Token, String> value;
	public static volatile SingularAttribute<Token, User> user;
}
