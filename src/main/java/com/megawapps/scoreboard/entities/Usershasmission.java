/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the USERSHASMISSION database table.
 * 
 */
@Entity
@Table(name = "USERSHASMISSION")
@NamedQuery(name = "Usershasmission.findAll", query = "SELECT u FROM Usershasmission u")
public class Usershasmission implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The idmissionuser. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "IDMISSIONUSER", unique = true, nullable = false)
	private long idmissionuser;

	/** The done. */
	@Column(name = "DONE")
	private boolean done;

	// bi-directional many-to-one association to Rule
	/** The rule. */
	@ManyToOne
	@JoinColumn(name = "RULE_IDRULE")
	private Rule rule;

	// bi-directional many-to-one association to User
	/** The user. */
	@ManyToOne
	@JoinColumn(name = "USER_IDUSER")
	private User user;

	/**
	 * Instantiates a new usershasmission.
	 */
	public Usershasmission() {
	}

	/**
	 * Gets the idmissionuser.
	 * 
	 * @return the idmissionuser
	 */
	public long getIdmissionuser() {
		return idmissionuser;
	}

	/**
	 * Sets the idmissionuser.
	 * 
	 * @param idmissionuser
	 *            the new idmissionuser
	 */
	public void setIdmissionuser(final long idmissionuser) {
		this.idmissionuser = idmissionuser;
	}

	/**
	 * Gets the done.
	 * 
	 * @return the done
	 */
	public boolean getDone() {
		return done;
	}

	/**
	 * Sets the done.
	 * 
	 * @param done
	 *            the new done
	 */
	public void setDone(final boolean done) {
		this.done = done;
	}

	/**
	 * Gets the rule.
	 * 
	 * @return the rule
	 */
	public Rule getRule() {
		return rule;
	}

	/**
	 * Sets the rule.
	 * 
	 * @param rule
	 *            the new rule
	 */
	public void setRule(final Rule rule) {
		this.rule = rule;
	}

	/**
	 * Gets the user.
	 * 
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 * 
	 * @param user
	 *            the new user
	 */
	public void setUser(final User user) {
		this.user = user;
	}

}