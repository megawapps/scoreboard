/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the NEWSFEED database table.
 * 
 */
@Entity
@Table(name="NEWSFEED")
@NamedQuery(name="Newsfeed.findAll", query="SELECT n FROM Newsfeed n")
public class Newsfeed implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The idnews. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IDNEWS", unique=true, nullable=false)
	private long idnews;

	/** The content. */
	@Column(name="CONTENT", length=255)
	private String content;

	/** The pubdate. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="PUBDATE")
	private Date pubdate;

	/** The title. */
	@Column(name="TITLE", length=45)
	private String title;

	//bi-directional many-to-one association to User
	/** The user. */
	@ManyToOne
	@JoinColumn(name="USER_IDUSER")
	private User user;

	/**
	 * Instantiates a new newsfeed.
	 */
	public Newsfeed() {
	}

	/**
	 * Gets the idnews.
	 * 
	 * @return the idnews
	 */
	public long getIdnews() {
		return this.idnews;
	}

	/**
	 * Sets the idnews.
	 * 
	 * @param idnews
	 *            the new idnews
	 */
	public void setIdnews(long idnews) {
		this.idnews = idnews;
	}

	/**
	 * Gets the content.
	 * 
	 * @return the content
	 */
	public String getContent() {
		return this.content;
	}

	/**
	 * Sets the content.
	 * 
	 * @param content
	 *            the new content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * Gets the pubdate.
	 * 
	 * @return the pubdate
	 */
	public Date getPubdate() {
		return this.pubdate;
	}

	/**
	 * Sets the pubdate.
	 * 
	 * @param pubdate
	 *            the new pubdate
	 */
	public void setPubdate(Date pubdate) {
		this.pubdate = pubdate;
	}

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the user.
	 * 
	 * @return the user
	 */
	public User getUser() {
		return this.user;
	}

	/**
	 * Sets the user.
	 * 
	 * @param user
	 *            the new user
	 */
	public void setUser(User user) {
		this.user = user;
	}

}