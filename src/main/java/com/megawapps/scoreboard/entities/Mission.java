/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the MISSION database table.
 * 
 */
@Entity
@Table(name="MISSION")
@NamedQuery(name="Mission.findAll", query="SELECT m FROM Mission m")
public class Mission implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The idmission. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IDMISSION", unique=true, nullable=false)
	private long idmission;

	/** The description. */
	@Column(name="DESCRIPTION", length=255)
	private String description;

	/** The title. */
	@Column(name="TITLE", length=254)
	private String title;

	//bi-directional many-to-one association to Game
	/** The game. */
	@ManyToOne(cascade={CascadeType.REFRESH})
	@JoinColumn(name="GAME_IDGAME")
	private Game game;

	//bi-directional many-to-one association to Rule
	/** The rules. */
	@OneToMany(mappedBy="mission", fetch=FetchType.EAGER)
	private List<Rule> rules;

	/**
	 * Instantiates a new mission.
	 */
	public Mission() {
	}

	/**
	 * Gets the idmission.
	 * 
	 * @return the idmission
	 */
	public long getIdmission() {
		return this.idmission;
	}

	/**
	 * Sets the idmission.
	 * 
	 * @param idmission
	 *            the new idmission
	 */
	public void setIdmission(long idmission) {
		this.idmission = idmission;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the game.
	 * 
	 * @return the game
	 */
	public Game getGame() {
		return this.game;
	}

	/**
	 * Sets the game.
	 * 
	 * @param game
	 *            the new game
	 */
	public void setGame(Game game) {
		this.game = game;
	}

	/**
	 * Gets the rules.
	 * 
	 * @return the rules
	 */
	public List<Rule> getRules() {
		return this.rules;
	}

	/**
	 * Sets the rules.
	 * 
	 * @param rules
	 *            the new rules
	 */
	public void setRules(List<Rule> rules) {
		this.rules = rules;
	}

	/**
	 * Adds the rule.
	 * 
	 * @param rule
	 *            the rule
	 * @return the rule
	 */
	public Rule addRule(Rule rule) {
		getRules().add(rule);
		rule.setMission(this);

		return rule;
	}

	/**
	 * Removes the rule.
	 * 
	 * @param rule
	 *            the rule
	 * @return the rule
	 */
	public Rule removeRule(Rule rule) {
		getRules().remove(rule);
		rule.setMission(null);

		return rule;
	}

}