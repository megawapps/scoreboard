package com.megawapps.scoreboard.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-02-18T23:20:36.490+0100")
@StaticMetamodel(Game.class)
public class Game_ {
	public static volatile SingularAttribute<Game, Long> idgame;
	public static volatile SingularAttribute<Game, String> andmarketlink;
	public static volatile SingularAttribute<Game, String> applestorelink;
	public static volatile SingularAttribute<Game, String> coinname;
	public static volatile SingularAttribute<Game, String> description;
	public static volatile SingularAttribute<Game, Boolean> featured;
	public static volatile SingularAttribute<Game, String> name;
	public static volatile SingularAttribute<Game, String> picture;
	public static volatile SingularAttribute<Game, String> plateform;
	public static volatile SingularAttribute<Game, Date> pubdate;
	public static volatile ListAttribute<Game, Mission> missions;
	public static volatile ListAttribute<Game, Score> scores;
	public static volatile ListAttribute<Game, Storeitem> storeitems;
}
