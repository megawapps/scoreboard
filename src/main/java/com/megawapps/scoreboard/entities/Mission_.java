package com.megawapps.scoreboard.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-02-18T23:20:36.513+0100")
@StaticMetamodel(Mission.class)
public class Mission_ {
	public static volatile SingularAttribute<Mission, Long> idmission;
	public static volatile SingularAttribute<Mission, String> description;
	public static volatile SingularAttribute<Mission, String> title;
	public static volatile SingularAttribute<Mission, Game> game;
	public static volatile ListAttribute<Mission, Rule> rules;
}
