/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the SCORE database table.
 * 
 */
@Entity
@Table(name="SCORE")
@NamedQuery(name="Score.findAll", query="SELECT s FROM Score s")
public class Score implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The idscore. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IDSCORE", unique=true, nullable=false)
	private long idscore;

	/** The coins. */
	@Column(name="COINS")
	private long coins;

	/** The gamesave. */
	@Lob
	@Column(name="GAMESAVE")
	private String gamesave;

	/** The score. */
	@Column(name="SCORE")
	private long score;

	/** The scoredate. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="SCOREDATE")
	private Date scoredate;

	//bi-directional many-to-one association to Game
	/** The game. */
	@ManyToOne
	@JoinColumn(name="GAME_IDGAME")
	private Game game;

	//bi-directional many-to-one association to User
	/** The user. */
	@ManyToOne
	@JoinColumn(name="USER_IDUSER")
	private User user;

	/**
	 * Instantiates a new score.
	 */
	public Score() {
	}

	/**
	 * Gets the idscore.
	 * 
	 * @return the idscore
	 */
	public long getIdscore() {
		return this.idscore;
	}

	/**
	 * Sets the idscore.
	 * 
	 * @param idscore
	 *            the new idscore
	 */
	public void setIdscore(long idscore) {
		this.idscore = idscore;
	}

	/**
	 * Gets the coins.
	 * 
	 * @return the coins
	 */
	public long getCoins() {
		return this.coins;
	}

	/**
	 * Sets the coins.
	 * 
	 * @param coins
	 *            the new coins
	 */
	public void setCoins(long coins) {
		this.coins = coins;
	}

	/**
	 * Gets the gamesave.
	 * 
	 * @return the gamesave
	 */
	public String getGamesave() {
		return this.gamesave;
	}

	/**
	 * Sets the gamesave.
	 * 
	 * @param gamesave
	 *            the new gamesave
	 */
	public void setGamesave(String gamesave) {
		this.gamesave = gamesave;
	}

	/**
	 * Gets the score.
	 * 
	 * @return the score
	 */
	public long getScore() {
		return this.score;
	}

	/**
	 * Sets the score.
	 * 
	 * @param score
	 *            the new score
	 */
	public void setScore(long score) {
		this.score = score;
	}

	/**
	 * Gets the scoredate.
	 * 
	 * @return the scoredate
	 */
	public Date getScoredate() {
		return this.scoredate;
	}

	/**
	 * Sets the scoredate.
	 * 
	 * @param scoredate
	 *            the new scoredate
	 */
	public void setScoredate(Date scoredate) {
		this.scoredate = scoredate;
	}

	/**
	 * Gets the game.
	 * 
	 * @return the game
	 */
	public Game getGame() {
		return this.game;
	}

	/**
	 * Sets the game.
	 * 
	 * @param game
	 *            the new game
	 */
	public void setGame(Game game) {
		this.game = game;
	}

	/**
	 * Gets the user.
	 * 
	 * @return the user
	 */
	public User getUser() {
		return this.user;
	}

	/**
	 * Sets the user.
	 * 
	 * @param user
	 *            the new user
	 */
	public void setUser(User user) {
		this.user = user;
	}

}