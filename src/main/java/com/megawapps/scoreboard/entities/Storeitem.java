/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the STOREITEM database table.
 * 
 */
@Entity
@Table(name="STOREITEM")
@NamedQuery(name="Storeitem.findAll", query="SELECT s FROM Storeitem s")
public class Storeitem implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The idstoreitem. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IDSTOREITEM", unique=true, nullable=false)
	private long idstoreitem;

	/** The active. */
	@Column(name="ACTIVE")
	private boolean active;

	/** The description. */
	@Column(name="DESCRIPTION", length=255)
	private String description;

	/** The picture. */
	@Column(name="PICTURE", length=254)
	private String picture;

	/** The price. */
	@Column(name="PRICE")
	private float price;

	/** The title. */
	@Column(name="TITLE", length=45)
	private String title;

	//bi-directional many-to-one association to Shophistory
	/** The shophistories. */
	@OneToMany(mappedBy="storeitem", fetch=FetchType.EAGER)
	private List<Shophistory> shophistories;

	//bi-directional many-to-one association to Game
	/** The game. */
	@ManyToOne
	@JoinColumn(name="GAME_IDGAME")
	private Game game;

	/**
	 * Instantiates a new storeitem.
	 */
	public Storeitem() {
	}

	/**
	 * Gets the idstoreitem.
	 * 
	 * @return the idstoreitem
	 */
	public long getIdstoreitem() {
		return this.idstoreitem;
	}

	/**
	 * Sets the idstoreitem.
	 * 
	 * @param idstoreitem
	 *            the new idstoreitem
	 */
	public void setIdstoreitem(long idstoreitem) {
		this.idstoreitem = idstoreitem;
	}

	/**
	 * Gets the active.
	 * 
	 * @return the active
	 */
	public boolean getActive() {
		return this.active;
	}

	/**
	 * Sets the active.
	 * 
	 * @param active
	 *            the new active
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the picture.
	 * 
	 * @return the picture
	 */
	public String getPicture() {
		return this.picture;
	}

	/**
	 * Sets the picture.
	 * 
	 * @param picture
	 *            the new picture
	 */
	public void setPicture(String picture) {
		this.picture = picture;
	}

	/**
	 * Gets the price.
	 * 
	 * @return the price
	 */
	public float getPrice() {
		return this.price;
	}

	/**
	 * Sets the price.
	 * 
	 * @param price
	 *            the new price
	 */
	public void setPrice(float price) {
		this.price = price;
	}

	/**
	 * Gets the title.
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the shophistories.
	 * 
	 * @return the shophistories
	 */
	public List<Shophistory> getShophistories() {
		return this.shophistories;
	}

	/**
	 * Sets the shophistories.
	 * 
	 * @param shophistories
	 *            the new shophistories
	 */
	public void setShophistories(List<Shophistory> shophistories) {
		this.shophistories = shophistories;
	}

	/**
	 * Adds the shophistory.
	 * 
	 * @param shophistory
	 *            the shophistory
	 * @return the shophistory
	 */
	public Shophistory addShophistory(Shophistory shophistory) {
		getShophistories().add(shophistory);
		shophistory.setStoreitem(this);

		return shophistory;
	}

	/**
	 * Removes the shophistory.
	 * 
	 * @param shophistory
	 *            the shophistory
	 * @return the shophistory
	 */
	public Shophistory removeShophistory(Shophistory shophistory) {
		getShophistories().remove(shophistory);
		shophistory.setStoreitem(null);

		return shophistory;
	}

	/**
	 * Gets the game.
	 * 
	 * @return the game
	 */
	public Game getGame() {
		return this.game;
	}

	/**
	 * Sets the game.
	 * 
	 * @param game
	 *            the new game
	 */
	public void setGame(Game game) {
		this.game = game;
	}

}