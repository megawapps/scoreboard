package com.megawapps.scoreboard.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-02-18T23:20:36.555+0100")
@StaticMetamodel(Shophistory.class)
public class Shophistory_ {
	public static volatile SingularAttribute<Shophistory, Long> idshophistory;
	public static volatile SingularAttribute<Shophistory, Date> shopdate;
	public static volatile SingularAttribute<Shophistory, Storeitem> storeitem;
	public static volatile SingularAttribute<Shophistory, User> user;
}
