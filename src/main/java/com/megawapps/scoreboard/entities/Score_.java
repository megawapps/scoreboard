package com.megawapps.scoreboard.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-02-18T23:20:36.537+0100")
@StaticMetamodel(Score.class)
public class Score_ {
	public static volatile SingularAttribute<Score, Long> idscore;
	public static volatile SingularAttribute<Score, Long> coins;
	public static volatile SingularAttribute<Score, String> gamesave;
	public static volatile SingularAttribute<Score, Long> score;
	public static volatile SingularAttribute<Score, Date> scoredate;
	public static volatile SingularAttribute<Score, Game> game;
	public static volatile SingularAttribute<Score, User> user;
}
