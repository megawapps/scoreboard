/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the SHOPHISTORY database table.
 * 
 */
@Entity
@Table(name="SHOPHISTORY")
@NamedQuery(name="Shophistory.findAll", query="SELECT s FROM Shophistory s")
public class Shophistory implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The idshophistory. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IDSHOPHISTORY", unique=true, nullable=false)
	private long idshophistory;

	/** The shopdate. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="SHOPDATE")
	private Date shopdate;

	//bi-directional many-to-one association to Storeitem
	/** The storeitem. */
	@ManyToOne
	@JoinColumn(name="STOREITEM_IDSTOREITEM")
	private Storeitem storeitem;

	//bi-directional many-to-one association to User
	/** The user. */
	@ManyToOne
	@JoinColumn(name="USER_IDUSER")
	private User user;

	/**
	 * Instantiates a new shophistory.
	 */
	public Shophistory() {
	}

	/**
	 * Gets the idshophistory.
	 * 
	 * @return the idshophistory
	 */
	public long getIdshophistory() {
		return this.idshophistory;
	}

	/**
	 * Sets the idshophistory.
	 * 
	 * @param idshophistory
	 *            the new idshophistory
	 */
	public void setIdshophistory(long idshophistory) {
		this.idshophistory = idshophistory;
	}

	/**
	 * Gets the shopdate.
	 * 
	 * @return the shopdate
	 */
	public Date getShopdate() {
		return this.shopdate;
	}

	/**
	 * Sets the shopdate.
	 * 
	 * @param shopdate
	 *            the new shopdate
	 */
	public void setShopdate(Date shopdate) {
		this.shopdate = shopdate;
	}

	/**
	 * Gets the storeitem.
	 * 
	 * @return the storeitem
	 */
	public Storeitem getStoreitem() {
		return this.storeitem;
	}

	/**
	 * Sets the storeitem.
	 * 
	 * @param storeitem
	 *            the new storeitem
	 */
	public void setStoreitem(Storeitem storeitem) {
		this.storeitem = storeitem;
	}

	/**
	 * Gets the user.
	 * 
	 * @return the user
	 */
	public User getUser() {
		return this.user;
	}

	/**
	 * Sets the user.
	 * 
	 * @param user
	 *            the new user
	 */
	public void setUser(User user) {
		this.user = user;
	}

}