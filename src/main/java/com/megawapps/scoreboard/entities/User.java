/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the USER database table.
 * 
 */
@Entity
@Table(name="USER")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The iduser. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IDUSER", unique=true, nullable=false)
	private long iduser;

	/** The activationcode. */
	@Column(name="ACTIVATIONCODE", length=254)
	private String activationcode;

	/** The active. */
	@Column(name="ACTIVE")
	private boolean active;

	/** The address. */
	@Column(name="ADDRESS", length=255)
	private String address;

	/** The admin. */
	@Column(name="ADMIN")
	private boolean admin;

	/** The author. */
	@Column(name="AUTHOR")
	private boolean author;

	/** The avatar. */
	@Column(name="AVATAR", length=254)
	private String avatar;

	/** The birthdate. */
	@Temporal(TemporalType.DATE)
	@Column(name="BIRTHDATE")
	private Date birthdate;

	/** The city. */
	@Column(name="CITY", length=45)
	private String city;

	/** The creadate. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREADATE")
	private Date creadate;

	/** The email. */
	@Column(name="EMAIL", length=45)
	private String email;

	/** The forname. */
	@Column(name="FORNAME", length=45)
	private String forname;

	/** The gender. */
	@Column(name="GENDER", length=1)
	private String gender;

	/** The name. */
	@Column(name="NAME", length=45)
	private String name;

	/** The passwd. */
	@Lob
	@Column(name="PASSWD")
	private byte[] passwd;

	/** The pseudo. */
	@Column(name="PSEUDO", length=45)
	private String pseudo;

	/** The salt. */
	@Lob
	@Column(name="SALT")
	private byte[] salt;

	/** The zipcode. */
	@Column(name="ZIPCODE", length=6)
	private String zipcode;

	//bi-directional many-to-one association to Newsfeed
	/** The newsfeeds. */
	@OneToMany(mappedBy="user", fetch=FetchType.EAGER)
	private List<Newsfeed> newsfeeds;

	//bi-directional many-to-one association to Score
	/** The scores. */
	@OneToMany(mappedBy="user", fetch=FetchType.EAGER)
	private List<Score> scores;

	//bi-directional many-to-one association to Shophistory
	/** The shophistories. */
	@OneToMany(mappedBy="user", fetch=FetchType.EAGER)
	private List<Shophistory> shophistories;

	//bi-directional many-to-one association to Token
	/** The tokens. */
	@OneToMany(mappedBy="user", fetch=FetchType.EAGER)
	private List<Token> tokens;

	//bi-directional many-to-one association to Usershasmission
	/** The usershasmissions. */
	@OneToMany(mappedBy="user", fetch=FetchType.EAGER)
	private List<Usershasmission> usershasmissions;

	/**
	 * Instantiates a new user.
	 */
	public User() {
	}

	/**
	 * Gets the iduser.
	 * 
	 * @return the iduser
	 */
	public long getIduser() {
		return this.iduser;
	}

	/**
	 * Sets the iduser.
	 * 
	 * @param iduser
	 *            the new iduser
	 */
	public void setIduser(long iduser) {
		this.iduser = iduser;
	}

	/**
	 * Gets the activationcode.
	 * 
	 * @return the activationcode
	 */
	public String getActivationcode() {
		return this.activationcode;
	}

	/**
	 * Sets the activationcode.
	 * 
	 * @param activationcode
	 *            the new activationcode
	 */
	public void setActivationcode(String activationcode) {
		this.activationcode = activationcode;
	}

	/**
	 * Gets the active.
	 * 
	 * @return the active
	 */
	public boolean getActive() {
		return this.active;
	}

	/**
	 * Sets the active.
	 * 
	 * @param active
	 *            the new active
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * Gets the address.
	 * 
	 * @return the address
	 */
	public String getAddress() {
		return this.address;
	}

	/**
	 * Sets the address.
	 * 
	 * @param address
	 *            the new address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the admin.
	 * 
	 * @return the admin
	 */
	public boolean getAdmin() {
		return this.admin;
	}

	/**
	 * Sets the admin.
	 * 
	 * @param admin
	 *            the new admin
	 */
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	/**
	 * Gets the author.
	 * 
	 * @return the author
	 */
	public boolean getAuthor() {
		return this.author;
	}

	/**
	 * Sets the author.
	 * 
	 * @param author
	 *            the new author
	 */
	public void setAuthor(boolean author) {
		this.author = author;
	}

	/**
	 * Gets the avatar.
	 * 
	 * @return the avatar
	 */
	public String getAvatar() {
		return this.avatar;
	}

	/**
	 * Sets the avatar.
	 * 
	 * @param avatar
	 *            the new avatar
	 */
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	/**
	 * Gets the birthdate.
	 * 
	 * @return the birthdate
	 */
	public Date getBirthdate() {
		return this.birthdate;
	}

	/**
	 * Sets the birthdate.
	 * 
	 * @param birthdate
	 *            the new birthdate
	 */
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	/**
	 * Gets the city.
	 * 
	 * @return the city
	 */
	public String getCity() {
		return this.city;
	}

	/**
	 * Sets the city.
	 * 
	 * @param city
	 *            the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the creadate.
	 * 
	 * @return the creadate
	 */
	public Date getCreadate() {
		return this.creadate;
	}

	/**
	 * Sets the creadate.
	 * 
	 * @param creadate
	 *            the new creadate
	 */
	public void setCreadate(Date creadate) {
		this.creadate = creadate;
	}

	/**
	 * Gets the email.
	 * 
	 * @return the email
	 */
	public String getEmail() {
		return this.email;
	}

	/**
	 * Sets the email.
	 * 
	 * @param email
	 *            the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the forname.
	 * 
	 * @return the forname
	 */
	public String getForname() {
		return this.forname;
	}

	/**
	 * Sets the forname.
	 * 
	 * @param forname
	 *            the new forname
	 */
	public void setForname(String forname) {
		this.forname = forname;
	}

	/**
	 * Gets the gender.
	 * 
	 * @return the gender
	 */
	public String getGender() {
		return this.gender;
	}

	/**
	 * Sets the gender.
	 * 
	 * @param gender
	 *            the new gender
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the passwd.
	 * 
	 * @return the passwd
	 */
	public byte[] getPasswd() {
		return this.passwd;
	}

	/**
	 * Sets the passwd.
	 * 
	 * @param passwd
	 *            the new passwd
	 */
	public void setPasswd(byte[] passwd) {
		this.passwd = passwd;
	}

	/**
	 * Gets the pseudo.
	 * 
	 * @return the pseudo
	 */
	public String getPseudo() {
		return this.pseudo;
	}

	/**
	 * Sets the pseudo.
	 * 
	 * @param pseudo
	 *            the new pseudo
	 */
	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	/**
	 * Gets the salt.
	 * 
	 * @return the salt
	 */
	public byte[] getSalt() {
		return this.salt;
	}

	/**
	 * Sets the salt.
	 * 
	 * @param salt
	 *            the new salt
	 */
	public void setSalt(byte[] salt) {
		this.salt = salt;
	}

	/**
	 * Gets the zipcode.
	 * 
	 * @return the zipcode
	 */
	public String getZipcode() {
		return this.zipcode;
	}

	/**
	 * Sets the zipcode.
	 * 
	 * @param zipcode
	 *            the new zipcode
	 */
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	/**
	 * Gets the newsfeeds.
	 * 
	 * @return the newsfeeds
	 */
	public List<Newsfeed> getNewsfeeds() {
		return this.newsfeeds;
	}

	/**
	 * Sets the newsfeeds.
	 * 
	 * @param newsfeeds
	 *            the new newsfeeds
	 */
	public void setNewsfeeds(List<Newsfeed> newsfeeds) {
		this.newsfeeds = newsfeeds;
	}

	/**
	 * Adds the newsfeed.
	 * 
	 * @param newsfeed
	 *            the newsfeed
	 * @return the newsfeed
	 */
	public Newsfeed addNewsfeed(Newsfeed newsfeed) {
		getNewsfeeds().add(newsfeed);
		newsfeed.setUser(this);

		return newsfeed;
	}

	/**
	 * Removes the newsfeed.
	 * 
	 * @param newsfeed
	 *            the newsfeed
	 * @return the newsfeed
	 */
	public Newsfeed removeNewsfeed(Newsfeed newsfeed) {
		getNewsfeeds().remove(newsfeed);
		newsfeed.setUser(null);

		return newsfeed;
	}

	/**
	 * Gets the scores.
	 * 
	 * @return the scores
	 */
	public List<Score> getScores() {
		return this.scores;
	}

	/**
	 * Sets the scores.
	 * 
	 * @param scores
	 *            the new scores
	 */
	public void setScores(List<Score> scores) {
		this.scores = scores;
	}

	/**
	 * Adds the score.
	 * 
	 * @param score
	 *            the score
	 * @return the score
	 */
	public Score addScore(Score score) {
		getScores().add(score);
		score.setUser(this);

		return score;
	}

	/**
	 * Removes the score.
	 * 
	 * @param score
	 *            the score
	 * @return the score
	 */
	public Score removeScore(Score score) {
		getScores().remove(score);
		score.setUser(null);

		return score;
	}

	/**
	 * Gets the shophistories.
	 * 
	 * @return the shophistories
	 */
	public List<Shophistory> getShophistories() {
		return this.shophistories;
	}

	/**
	 * Sets the shophistories.
	 * 
	 * @param shophistories
	 *            the new shophistories
	 */
	public void setShophistories(List<Shophistory> shophistories) {
		this.shophistories = shophistories;
	}

	/**
	 * Adds the shophistory.
	 * 
	 * @param shophistory
	 *            the shophistory
	 * @return the shophistory
	 */
	public Shophistory addShophistory(Shophistory shophistory) {
		getShophistories().add(shophistory);
		shophistory.setUser(this);

		return shophistory;
	}

	/**
	 * Removes the shophistory.
	 * 
	 * @param shophistory
	 *            the shophistory
	 * @return the shophistory
	 */
	public Shophistory removeShophistory(Shophistory shophistory) {
		getShophistories().remove(shophistory);
		shophistory.setUser(null);

		return shophistory;
	}

	/**
	 * Gets the tokens.
	 * 
	 * @return the tokens
	 */
	public List<Token> getTokens() {
		return this.tokens;
	}

	/**
	 * Sets the tokens.
	 * 
	 * @param tokens
	 *            the new tokens
	 */
	public void setTokens(List<Token> tokens) {
		this.tokens = tokens;
	}

	/**
	 * Adds the token.
	 * 
	 * @param token
	 *            the token
	 * @return the token
	 */
	public Token addToken(Token token) {
		getTokens().add(token);
		token.setUser(this);

		return token;
	}

	/**
	 * Removes the token.
	 * 
	 * @param token
	 *            the token
	 * @return the token
	 */
	public Token removeToken(Token token) {
		getTokens().remove(token);
		token.setUser(null);

		return token;
	}

	/**
	 * Gets the usershasmissions.
	 * 
	 * @return the usershasmissions
	 */
	public List<Usershasmission> getUsershasmissions() {
		return this.usershasmissions;
	}

	/**
	 * Sets the usershasmissions.
	 * 
	 * @param usershasmissions
	 *            the new usershasmissions
	 */
	public void setUsershasmissions(List<Usershasmission> usershasmissions) {
		this.usershasmissions = usershasmissions;
	}

	/**
	 * Adds the usershasmission.
	 * 
	 * @param usershasmission
	 *            the usershasmission
	 * @return the usershasmission
	 */
	public Usershasmission addUsershasmission(Usershasmission usershasmission) {
		getUsershasmissions().add(usershasmission);
		usershasmission.setUser(this);

		return usershasmission;
	}

	/**
	 * Removes the usershasmission.
	 * 
	 * @param usershasmission
	 *            the usershasmission
	 * @return the usershasmission
	 */
	public Usershasmission removeUsershasmission(Usershasmission usershasmission) {
		getUsershasmissions().remove(usershasmission);
		usershasmission.setUser(null);

		return usershasmission;
	}

}