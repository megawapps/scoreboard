package com.megawapps.scoreboard.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-02-18T23:20:36.600+0100")
@StaticMetamodel(User.class)
public class User_ {
	public static volatile SingularAttribute<User, Long> iduser;
	public static volatile SingularAttribute<User, String> activationcode;
	public static volatile SingularAttribute<User, Boolean> active;
	public static volatile SingularAttribute<User, String> address;
	public static volatile SingularAttribute<User, Boolean> admin;
	public static volatile SingularAttribute<User, Boolean> author;
	public static volatile SingularAttribute<User, String> avatar;
	public static volatile SingularAttribute<User, Date> birthdate;
	public static volatile SingularAttribute<User, String> city;
	public static volatile SingularAttribute<User, Date> creadate;
	public static volatile SingularAttribute<User, String> email;
	public static volatile SingularAttribute<User, String> forname;
	public static volatile SingularAttribute<User, String> gender;
	public static volatile SingularAttribute<User, String> name;
	public static volatile SingularAttribute<User, byte[]> passwd;
	public static volatile SingularAttribute<User, String> pseudo;
	public static volatile SingularAttribute<User, byte[]> salt;
	public static volatile SingularAttribute<User, String> zipcode;
	public static volatile ListAttribute<User, Newsfeed> newsfeeds;
	public static volatile ListAttribute<User, Score> scores;
	public static volatile ListAttribute<User, Shophistory> shophistories;
	public static volatile ListAttribute<User, Token> tokens;
	public static volatile ListAttribute<User, Usershasmission> usershasmissions;
}
