package com.megawapps.scoreboard.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-02-18T23:20:36.604+0100")
@StaticMetamodel(Usershasmission.class)
public class Usershasmission_ {
	public static volatile SingularAttribute<Usershasmission, Long> idmissionuser;
	public static volatile SingularAttribute<Usershasmission, Boolean> done;
	public static volatile SingularAttribute<Usershasmission, Rule> rule;
	public static volatile SingularAttribute<Usershasmission, User> user;
}
