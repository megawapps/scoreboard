/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the TOKEN database table.
 * 
 */
@Entity
@Table(name="TOKEN")
@NamedQuery(name="Token.findAll", query="SELECT t FROM Token t")
public class Token implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The idtoken. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IDTOKEN", unique=true, nullable=false)
	private long idtoken;

	/** The enddate. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ENDDATE")
	private Date enddate;

	/** The value. */
	@Column(name="VALUE", length=254)
	private String value;

	//bi-directional many-to-one association to User
	/** The user. */
	@ManyToOne
	@JoinColumn(name="USER_IDUSER")
	private User user;

	/**
	 * Instantiates a new token.
	 */
	public Token() {
	}

	/**
	 * Gets the idtoken.
	 * 
	 * @return the idtoken
	 */
	public long getIdtoken() {
		return this.idtoken;
	}

	/**
	 * Sets the idtoken.
	 * 
	 * @param idtoken
	 *            the new idtoken
	 */
	public void setIdtoken(long idtoken) {
		this.idtoken = idtoken;
	}

	/**
	 * Gets the enddate.
	 * 
	 * @return the enddate
	 */
	public Date getEnddate() {
		return this.enddate;
	}

	/**
	 * Sets the enddate.
	 * 
	 * @param enddate
	 *            the new enddate
	 */
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	/**
	 * Gets the value.
	 * 
	 * @return the value
	 */
	public String getValue() {
		return this.value;
	}

	/**
	 * Sets the value.
	 * 
	 * @param value
	 *            the new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Gets the user.
	 * 
	 * @return the user
	 */
	public User getUser() {
		return this.user;
	}

	/**
	 * Sets the user.
	 * 
	 * @param user
	 *            the new user
	 */
	public void setUser(User user) {
		this.user = user;
	}

}