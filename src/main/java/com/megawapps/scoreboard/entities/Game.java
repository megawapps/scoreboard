/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the GAME database table.
 * 
 */
@Entity
@Table(name="GAME")
@NamedQuery(name="Game.findAll", query="SELECT g FROM Game g")
public class Game implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The idgame. */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="IDGAME", unique=true, nullable=false)
	private long idgame;

	/** The andmarketlink. */
	@Column(name="ANDMARKETLINK", length=254)
	private String andmarketlink;

	/** The applestorelink. */
	@Column(name="APPLESTORELINK", length=254)
	private String applestorelink;

	/** The coinname. */
	@Column(name="COINNAME", length=45)
	private String coinname;

	/** The description. */
	@Lob
	@Column(name="DESCRIPTION")
	private String description;

	/** The featured. */
	@Column(name="FEATURED")
	private boolean featured;

	/** The name. */
	@Column(name="NAME", length=45)
	private String name;

	/** The picture. */
	@Column(name="PICTURE", length=254)
	private String picture;

	/** The plateform. */
	@Column(name="PLATEFORM", length=45)
	private String plateform;

	/** The pubdate. */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="PUBDATE")
	private Date pubdate;

	//bi-directional many-to-one association to Mission
	/** The missions. */
	@OneToMany(mappedBy="game", cascade={CascadeType.REFRESH}, fetch=FetchType.EAGER)
	private List<Mission> missions;

	//bi-directional many-to-one association to Score
	/** The scores. */
	@OneToMany(mappedBy="game", fetch=FetchType.EAGER)
	private List<Score> scores;

	//bi-directional many-to-one association to Storeitem
	/** The storeitems. */
	@OneToMany(mappedBy="game", fetch=FetchType.EAGER)
	private List<Storeitem> storeitems;

	/**
	 * Instantiates a new game.
	 */
	public Game() {
	}

	/**
	 * Gets the idgame.
	 * 
	 * @return the idgame
	 */
	public long getIdgame() {
		return this.idgame;
	}

	/**
	 * Sets the idgame.
	 * 
	 * @param idgame
	 *            the new idgame
	 */
	public void setIdgame(long idgame) {
		this.idgame = idgame;
	}

	/**
	 * Gets the andmarketlink.
	 * 
	 * @return the andmarketlink
	 */
	public String getAndmarketlink() {
		return this.andmarketlink;
	}

	/**
	 * Sets the andmarketlink.
	 * 
	 * @param andmarketlink
	 *            the new andmarketlink
	 */
	public void setAndmarketlink(String andmarketlink) {
		this.andmarketlink = andmarketlink;
	}

	/**
	 * Gets the applestorelink.
	 * 
	 * @return the applestorelink
	 */
	public String getApplestorelink() {
		return this.applestorelink;
	}

	/**
	 * Sets the applestorelink.
	 * 
	 * @param applestorelink
	 *            the new applestorelink
	 */
	public void setApplestorelink(String applestorelink) {
		this.applestorelink = applestorelink;
	}

	/**
	 * Gets the coinname.
	 * 
	 * @return the coinname
	 */
	public String getCoinname() {
		return this.coinname;
	}

	/**
	 * Sets the coinname.
	 * 
	 * @param coinname
	 *            the new coinname
	 */
	public void setCoinname(String coinname) {
		this.coinname = coinname;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the featured.
	 * 
	 * @return the featured
	 */
	public boolean getFeatured() {
		return this.featured;
	}

	/**
	 * Sets the featured.
	 * 
	 * @param featured
	 *            the new featured
	 */
	public void setFeatured(boolean featured) {
		this.featured = featured;
	}

	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the picture.
	 * 
	 * @return the picture
	 */
	public String getPicture() {
		return this.picture;
	}

	/**
	 * Sets the picture.
	 * 
	 * @param picture
	 *            the new picture
	 */
	public void setPicture(String picture) {
		this.picture = picture;
	}

	/**
	 * Gets the plateform.
	 * 
	 * @return the plateform
	 */
	public String getPlateform() {
		return this.plateform;
	}

	/**
	 * Sets the plateform.
	 * 
	 * @param plateform
	 *            the new plateform
	 */
	public void setPlateform(String plateform) {
		this.plateform = plateform;
	}

	/**
	 * Gets the pubdate.
	 * 
	 * @return the pubdate
	 */
	public Date getPubdate() {
		return this.pubdate;
	}

	/**
	 * Sets the pubdate.
	 * 
	 * @param pubdate
	 *            the new pubdate
	 */
	public void setPubdate(Date pubdate) {
		this.pubdate = pubdate;
	}

	/**
	 * Gets the missions.
	 * 
	 * @return the missions
	 */
	public List<Mission> getMissions() {
		return this.missions;
	}

	/**
	 * Sets the missions.
	 * 
	 * @param missions
	 *            the new missions
	 */
	public void setMissions(List<Mission> missions) {
		this.missions = missions;
	}

	/**
	 * Adds the mission.
	 * 
	 * @param mission
	 *            the mission
	 * @return the mission
	 */
	public Mission addMission(Mission mission) {
		getMissions().add(mission);
		mission.setGame(this);

		return mission;
	}

	/**
	 * Removes the mission.
	 * 
	 * @param mission
	 *            the mission
	 * @return the mission
	 */
	public Mission removeMission(Mission mission) {
		getMissions().remove(mission);
		mission.setGame(null);

		return mission;
	}

	/**
	 * Gets the scores.
	 * 
	 * @return the scores
	 */
	public List<Score> getScores() {
		return this.scores;
	}

	/**
	 * Sets the scores.
	 * 
	 * @param scores
	 *            the new scores
	 */
	public void setScores(List<Score> scores) {
		this.scores = scores;
	}

	/**
	 * Adds the score.
	 * 
	 * @param score
	 *            the score
	 * @return the score
	 */
	public Score addScore(Score score) {
		getScores().add(score);
		score.setGame(this);

		return score;
	}

	/**
	 * Removes the score.
	 * 
	 * @param score
	 *            the score
	 * @return the score
	 */
	public Score removeScore(Score score) {
		getScores().remove(score);
		score.setGame(null);

		return score;
	}

	/**
	 * Gets the storeitems.
	 * 
	 * @return the storeitems
	 */
	public List<Storeitem> getStoreitems() {
		return this.storeitems;
	}

	/**
	 * Sets the storeitems.
	 * 
	 * @param storeitems
	 *            the new storeitems
	 */
	public void setStoreitems(List<Storeitem> storeitems) {
		this.storeitems = storeitems;
	}

	/**
	 * Adds the storeitem.
	 * 
	 * @param storeitem
	 *            the storeitem
	 * @return the storeitem
	 */
	public Storeitem addStoreitem(Storeitem storeitem) {
		getStoreitems().add(storeitem);
		storeitem.setGame(this);

		return storeitem;
	}

	/**
	 * Removes the storeitem.
	 * 
	 * @param storeitem
	 *            the storeitem
	 * @return the storeitem
	 */
	public Storeitem removeStoreitem(Storeitem storeitem) {
		getStoreitems().remove(storeitem);
		storeitem.setGame(null);

		return storeitem;
	}

}