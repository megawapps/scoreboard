package com.megawapps.scoreboard.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2014-02-18T23:20:36.563+0100")
@StaticMetamodel(Storeitem.class)
public class Storeitem_ {
	public static volatile SingularAttribute<Storeitem, Long> idstoreitem;
	public static volatile SingularAttribute<Storeitem, Boolean> active;
	public static volatile SingularAttribute<Storeitem, String> description;
	public static volatile SingularAttribute<Storeitem, String> picture;
	public static volatile SingularAttribute<Storeitem, Float> price;
	public static volatile SingularAttribute<Storeitem, String> title;
	public static volatile ListAttribute<Storeitem, Shophistory> shophistories;
	public static volatile SingularAttribute<Storeitem, Game> game;
}
