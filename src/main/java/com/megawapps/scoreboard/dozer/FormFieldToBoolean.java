/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.dozer;

import org.dozer.ConfigurableCustomConverter;

/**
 * The Class FormFieldToBoolean.
 * 
 * @author Giwi Softwares
 */
public class FormFieldToBoolean implements ConfigurableCustomConverter {

	/** The key. */
	private String key;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.dozer.CustomConverter#convert(java.lang.Object, java.lang.Object, java.lang.Class, java.lang.Class)
	 */
	@Override
	public Object convert(final Object destinationFieldValue, final Object sourceFieldValue, final Class destinationClass, final Class sourceClass) {
		final String datas = (String) sourceFieldValue;
		if (datas != null) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.dozer.ConfigurableCustomConverter#setParameter(java.lang.String)
	 */
	@Override
	public void setParameter(final String key) {
		this.key = key;
	}
}
