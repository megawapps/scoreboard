/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.megawapps.scoreboard.dao.DozerHelper;

/**
 * Application Lifecycle Listener implementation class Startup.
 */
@WebListener
public class Startup implements ServletContextListener {

	/**
	 * Context initialized.
	 * 
	 * @param arg0
	 *            the arg0
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	@Override
	public void contextInitialized(final ServletContextEvent arg0) {
		DozerHelper.getInstance();
	}

	/**
	 * Context destroyed.
	 * 
	 * @param arg0
	 *            the arg0
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(final ServletContextEvent arg0) {
	}

}
