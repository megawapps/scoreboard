/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.dao;

import java.util.Date;

import com.megawapps.scoreboard.entities.Token;
import com.megawapps.scoreboard.rest.model.web.User;

/**
 * The Class AuthManager.
 */
public class AuthManager {

	/** The instance. */
	private static AuthManager INSTANCE;

	/**
	 * Instantiates a new auth manager.
	 */
	private AuthManager() {
		//
	}

	/**
	 * Gets the single instance of AuthManager.
	 * 
	 * @return single instance of AuthManager
	 */
	public static AuthManager getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new AuthManager();
		}
		return INSTANCE;
	}

	/**
	 * Checks if is admin.
	 * 
	 * @param u
	 *            the u
	 * @return true, if is admin
	 */
	public boolean isAdmin(final User u) {
		return isLogged(u) && u.isAdmin();
	}

	/**
	 * Checks if is logged.
	 * 
	 * @param u
	 *            the u
	 * @return true, if is logged
	 */
	private boolean isLogged(final User u) {
		if (u != null) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if is valid.
	 * 
	 * @param user
	 *            the user
	 * @return true, if is valid
	 */
	public boolean isValid(final User user) {
		for (final Token t : UserManagerDao.getInstance().find(user.getIduser(), false).getTokens()) {
			if (t.getEnddate().after(new Date())) {
				return true;
			}
		}
		return false;
	}

}
