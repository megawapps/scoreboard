/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.OptimisticLockException;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import com.megawapps.scoreboard.entities.Rule;

/**
 * The Class RuleManagerDao.
 * 
 * @author Xavier.Marin
 */
public class RuleManagerDao {
	/** The entity manager. */
	private EntityManager entityManager;
	/** The instance. */
	private static RuleManagerDao INSTANCE;

	/**
	 * The Constructor.
	 */
	private RuleManagerDao() {
		final EntityManagerFactory factory = Persistence.createEntityManagerFactory("scoreboard");
		entityManager = factory.createEntityManager();
	}

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static RuleManagerDao getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new RuleManagerDao();
		}
		return INSTANCE;
	}

	/**
	 * Gets the entity manager.
	 * 
	 * @return the entity manager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Sets the entity manager.
	 * 
	 * @param entityManager
	 *            the entity manager
	 */
	@PersistenceContext(unitName = "scoreboard")
	public void setEntityManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Find.
	 * 
	 * @param id
	 *            the id
	 * @param complete
	 *            the complete
	 * @return the rule
	 */
	public Rule find(final Long id, final boolean complete) {
		final Rule rule = getEntityManager().find(Rule.class, id);
		if (complete && (rule != null)) {
			rule.getMission();
		}
		return refresh(rule);
	}

	/**
	 * Save.
	 * 
	 * @param myRule
	 *            the my rule
	 * @return the rule
	 */
	public Rule save(final Rule myRule) {
		Rule rule = null;
		try {
			getEntityManager().getTransaction().begin();
			rule = getEntityManager().merge(myRule);
			if (rule != null) {
				getEntityManager().flush();
			}
			getEntityManager().getTransaction().commit();
			rule = refresh(myRule);
		} catch (final OptimisticLockException ole) {
			return null;
		}
		return rule;
	}

	/**
	 * Creates the.
	 * 
	 * @param myRule
	 *            the my rule
	 * @return the rule
	 */
	public Rule create(final Rule myRule) {
		Rule rule = null;
		try {
			getEntityManager().getTransaction().begin();
			getEntityManager().persist(myRule);
			getEntityManager().flush();
			getEntityManager().getTransaction().commit();
			rule = refresh(myRule);
		} catch (final OptimisticLockException ole) {
			return null;
		}
		return rule;
	}

	/**
	 * Delete.
	 * 
	 * @param theRule
	 *            the the rule
	 * @return the rule
	 */
	public Rule delete(final Rule theRule) {
		try {
			getEntityManager().getTransaction().begin();
			final Rule rule = getEntityManager().find(Rule.class, theRule.getIdrule());
			getEntityManager().remove(rule);
			getEntityManager().flush();
			getEntityManager().getTransaction().commit();
		} catch (final OptimisticLockException ole) {
			return theRule;
		}
		return null;
	}

	/**
	 * Delete.
	 * 
	 * @param idRule
	 *            the id rule
	 * @return the idRule
	 */
	public Long delete(final Long idRule) {
		if (!getEntityManager().getTransaction().isActive()) {
			getEntityManager().getTransaction().begin();
		}
		final Rule rule = getEntityManager().find(Rule.class, idRule);
		if ((rule != null) && getEntityManager().contains(rule)) {
			getEntityManager().remove(rule);
		}
		getEntityManager().flush();
		getEntityManager().getTransaction().commit();
		return null;
	}

	/**
	 * Refresh.
	 * 
	 * @param myRule
	 *            the mission
	 * @return the rule
	 */
	public Rule refresh(final Rule myRule) {
		final Rule rule = getEntityManager().find(Rule.class, myRule.getIdrule());
		getEntityManager().refresh(rule);
		rule.getMission();
		return rule;
	}
}
