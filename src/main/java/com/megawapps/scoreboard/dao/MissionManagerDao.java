/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.OptimisticLockException;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import com.megawapps.scoreboard.entities.Mission;
import com.megawapps.scoreboard.tools.criteria.MissionCriteria;
import com.megawapps.scoreboard.tools.paging.EntityPaging;

/**
 * The Class NewsManagerDao.
 */
public class MissionManagerDao {

	/** The entity manager. */
	private EntityManager entityManager;
	/** The instance. */
	private static MissionManagerDao INSTANCE;

	/**
	 * The Constructor.
	 */
	private MissionManagerDao() {
		final EntityManagerFactory factory = Persistence.createEntityManagerFactory("scoreboard");
		entityManager = factory.createEntityManager();
	}

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static MissionManagerDao getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new MissionManagerDao();
		}
		return INSTANCE;
	}

	/**
	 * Gets the list of mission.
	 * 
	 * @return the list of mission
	 */
	public List<Mission> getListOfMission() {
		final MissionCriteria criteria = new MissionCriteria(10);
		criteria.setPageSize(5);
		criteria.setPagingType(EntityPaging.Type.PAGE.name());
		return getMissions(criteria);
	}

	/**
	 * Gets the entity manager.
	 * 
	 * @return the entity manager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Sets the entity manager.
	 * 
	 * @param entityManager
	 *            the entity manager
	 */
	@PersistenceContext(unitName = "scoreboard")
	public void setEntityManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Find.
	 * 
	 * @param id
	 *            the id
	 * @param complete
	 *            the complete
	 * @return the mission
	 */
	public Mission find(final Long id, final boolean complete) {
		final Mission mission = getEntityManager().find(Mission.class, id);
		if (complete && (mission != null)) {
			mission.getGame();
			mission.getRules();
		}

		return refresh(mission);
	}

	/**
	 * Save.
	 * 
	 * @param myMission
	 *            the my mission
	 * @return the mission
	 */
	public Mission save(final Mission myMission) {
		Mission mission = null;
		try {
			getEntityManager().getTransaction().begin();
			mission = getEntityManager().merge(myMission);
			if (mission != null) {
				getEntityManager().flush();
			}
			getEntityManager().getTransaction().commit();
			mission = refresh(myMission);
		} catch (final OptimisticLockException ole) {
			return null;
		}
		return mission;
	}

	/**
	 * Creates the.
	 * 
	 * @param myMission
	 *            the my mission
	 * @return the mission
	 */
	public Mission create(final Mission myMission) {
		Mission mission = null;
		try {
			getEntityManager().getTransaction().begin();
			getEntityManager().persist(myMission);
			getEntityManager().flush();
			getEntityManager().getTransaction().commit();
			mission = refresh(myMission);
		} catch (final OptimisticLockException ole) {
			return null;
		}
		return mission;
	}

	/**
	 * Delete.
	 * 
	 * @param myMission
	 *            the mission
	 * @return the mission
	 */
	public Mission delete(final Mission myMission) {
		try {
			getEntityManager().getTransaction().begin();
			final Mission mission = getEntityManager().find(Mission.class, myMission.getIdmission());
			getEntityManager().remove(mission);
			getEntityManager().flush();
			getEntityManager().getTransaction().commit();
		} catch (final OptimisticLockException ole) {
			return myMission;
		}
		return null;
	}

	/**
	 * Delete.
	 * 
	 * @param idMission
	 *            the id mission
	 * @return the idMission
	 */
	public Long delete(final Long idMission) {
		try {
			getEntityManager().getTransaction().begin();
			final Mission mission = getEntityManager().find(Mission.class, idMission);
			getEntityManager().remove(mission);
			getEntityManager().flush();
			getEntityManager().getTransaction().commit();
		} catch (final OptimisticLockException ole) {
			return idMission;
		}
		return null;
	}

	/**
	 * Refresh.
	 * 
	 * @param myMission
	 *            the mission
	 * @return the mission
	 */
	public Mission refresh(final Mission myMission) {
		final Mission mission = getEntityManager().find(Mission.class, myMission.getIdmission());
		getEntityManager().refresh(mission);
		mission.getGame();
		mission.getRules();
		return mission;
	}

	/**
	 * Gets the paging.
	 * 
	 * @param criteria
	 *            the criteria
	 * @return the paging
	 */
	public EntityPaging<Mission> getPaging(final MissionCriteria criteria) {
		return criteria.getPaging(getEntityManager().getEntityManagerFactory());
	}

	/**
	 * Gets the mission.
	 * 
	 * @param criteria
	 *            the criteria
	 * @return the mission
	 */
	public List<Mission> getMissions(final MissionCriteria criteria) {
		final CriteriaQuery<Mission> cq = criteria.createQuery(getEntityManager());
		return getEntityManager().createQuery(cq).getResultList();
	}
}
