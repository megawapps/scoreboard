/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import com.megawapps.scoreboard.entities.Shophistory;
import com.megawapps.scoreboard.entities.User;

/**
 * The Class AdminDAO.
 */
public class AdminDAO {

	/** The entity manager. */
	private EntityManager entityManager;

	/** The instance. */
	private static AdminDAO INSTANCE;

	/**
	 * The Constructor.
	 */
	private AdminDAO() {
		final EntityManagerFactory factory = Persistence.createEntityManagerFactory("scoreboard");
		entityManager = factory.createEntityManager();
	}

	/**
	 * Gets the single instance of AdminDAO.
	 * 
	 * @return single instance of AdminDAO
	 */
	public static AdminDAO getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new AdminDAO();
		}
		return INSTANCE;
	}

	/**
	 * Gets the entity manager.
	 * 
	 * @return the entity manager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Sets the entity manager.
	 * 
	 * @param entityManager
	 *            the entity manager
	 */
	@PersistenceContext(unitName = "scoreboard")
	public void setEntityManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Gets the users count.
	 * 
	 * @return the users count
	 */
	public long getUsersCount() {
		final CriteriaBuilder qb = entityManager.getCriteriaBuilder();
		final CriteriaQuery<Long> cq = qb.createQuery(Long.class);
		cq.select(qb.count(cq.from(User.class)));
		return entityManager.createQuery(cq).getSingleResult();
	}

	/**
	 * Gets the shop count.
	 * 
	 * @return the shop count
	 */
	public long getShopCount() {
		final CriteriaBuilder qb = entityManager.getCriteriaBuilder();
		final CriteriaQuery<Long> cq = qb.createQuery(Long.class);
		cq.select(qb.count(cq.from(Shophistory.class)));
		return entityManager.createQuery(cq).getSingleResult();
	}
}
