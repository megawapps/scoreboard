/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Session;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import com.megawapps.scoreboard.entities.User;
import com.megawapps.scoreboard.tools.Messages;
import com.megawapps.scoreboard.tools.Params;

/**
 * The Class MailDao.
 */
public class MailDao {

	/** The instance. */
	private static MailDao INSTANCE = null;

	/** The Constant FROM. */
	private static final String FROM = Messages.getString("mail.from"); //$NON-NLS-1$

	/** The session. */
	private final Session session;

	/**
	 * Instantiates a new mail dao.
	 */
	private MailDao() {
		final Properties props = new Properties();
		props.put("mail.smtp.host", Params.getString("mail.smtp.host")); //$NON-NLS-1$
		props.put("mail.smtp.port", Params.getString("mail.smtp.port"));//$NON-NLS-1$
		props.put("mail.smtp.auth", "true");
		session = Session.getDefaultInstance(props);
	}

	/**
	 * Gets the single instance of MailDao.
	 * 
	 * @return single instance of MailDao
	 */
	public static MailDao getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new MailDao();
		}
		return INSTANCE;
	}

	/**
	 * Send mail.
	 * 
	 * @param dests
	 *            the dests
	 * @param body
	 *            the body
	 * @param subject
	 *            the subject
	 * @return true, if successful
	 */
	public boolean sendMail(final List<String> dests, final String body, final String subject) {
		try {
			final HtmlEmail email = new HtmlEmail();
			email.setHostName(Params.getString("mail.smtp.host"));
			email.setSmtpPort(Integer.valueOf(Params.getString("mail.smtp.port")));
			email.setAuthentication(Params.getString("mail.smtp.user"), Params.getString("mail.smtp.passwd"));
			email.setSSLOnConnect(false);
			email.setFrom(Params.getString("mail.from"));
			email.setSubject(subject);
			email.setHtmlMsg(body);
			for (final String to : dests) {
				email.addTo(to);
			}
			email.send();
			return true;
		} catch (final EmailException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Sent activation mail.
	 * 
	 * @param u
	 *            the u
	 * @param activationcode
	 *            the activationcode
	 * @return true, if successful
	 */
	public boolean sentActivationMail(final User u, final String activationcode) {
		final List<String> dests = new ArrayList<String>();
		dests.add(u.getEmail());
		return sendMail(dests, generateActivationBody(activationcode, u.getIduser()), Messages.getString("mail.account.validation.subject")); //$NON-NLS-1$
	}

	/**
	 * Generate activation body.
	 * 
	 * @param activationcode
	 *            the activationcode
	 * @param userId
	 *            the user id
	 * @return the string
	 */
	private String generateActivationBody(final String activationcode, final long userId) {
		final StringBuilder sb = new StringBuilder();
		sb.append(Messages.getString("mail.account.validation.title")); //$NON-NLS-1$
		sb.append(Messages.getString("mail.account.validation.line.1")); //$NON-NLS-1$
		sb.append(Messages.getString("mail.account.validation.line.2")); //$NON-NLS-1$
		sb.append(Messages.getString("mail.site.url")); //$NON-NLS-1$
		sb.append(Messages.getString("mail.site.url.api")); //$NON-NLS-1$
		sb.append(userId).append("/").append(activationcode);
		sb.append(Messages.getString("mail.account.validation.line.3"));
		sb.append(Params.getString("mail.site.url"));
		sb.append(Params.getString("mail.site.url.api"));
		sb.append(userId).append("/").append(activationcode);
		sb.append(Messages.getString("mail.account.validation.line.4")); //$NON-NLS-1$
		sb.append(Messages.getString("mail.account.validation.line.5")); //$NON-NLS-1$
		sb.append(Params.getString("mail.site.url")); //$NON-NLS-1$
		sb.append(Messages.getString("mail.account.validation.line.6"));
		sb.append(Messages.getString("mail.account.validation.line.7"));
		sb.append(Messages.getString("mail.account.validation.line.8")); //$NON-NLS-1$ 
		return sb.toString();
	}
}
