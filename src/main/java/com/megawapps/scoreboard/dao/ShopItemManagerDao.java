/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import com.megawapps.scoreboard.entities.Game;
import com.megawapps.scoreboard.entities.Storeitem;
import com.megawapps.scoreboard.tools.criteria.ShopCriteria;
import com.megawapps.scoreboard.tools.paging.EntityPaging;

/**
 * The Class ShopItemManagerDao.
 * 
 * @author Giwi Softwares
 */
public class ShopItemManagerDao {

	/** The entity manager. */
	private EntityManager entityManager;

	/** The instance. */
	private static ShopItemManagerDao INSTANCE;

	/**
	 * The Constructor.
	 */
	private ShopItemManagerDao() {
		final EntityManagerFactory factory = Persistence.createEntityManagerFactory("scoreboard");
		entityManager = factory.createEntityManager();
	}

	/**
	 * Gets the list of shop items.
	 * 
	 * @param active
	 *            the active
	 * @return the list of shop items
	 */
	public List<Storeitem> getListOfShopItems(final boolean active) {
		final ShopCriteria criteria = new ShopCriteria(10);
		criteria.setTitle(null);
		criteria.setPageSize(5);
		criteria.setPagingType(EntityPaging.Type.PAGE.name());
		criteria.setActive(active);
		return getShopItems(criteria);
	}

	/**
	 * Gets the list of shop items.
	 * 
	 * @return the list of shop items
	 */
	public List<Storeitem> getListOfShopItems() {
		final ShopCriteria criteria = new ShopCriteria(10);
		criteria.setTitle(null);
		criteria.setPageSize(5);
		criteria.setPagingType(EntityPaging.Type.PAGE.name());
		return getShopItems(criteria);
	}

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static ShopItemManagerDao getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ShopItemManagerDao();
		}
		return INSTANCE;
	}

	/**
	 * Gets the entity manager.
	 * 
	 * @return the entity manager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Sets the entity manager.
	 * 
	 * @param entityManager
	 *            the entity manager
	 */
	@PersistenceContext(unitName = "scoreboard")
	public void setEntityManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Gets the criteria.
	 * 
	 * @param idgames
	 *            the idgames
	 * @return the criteria
	 */
	public ShopCriteria getCriteria(final long idgames) {
		final ShopCriteria scriteria = new ShopCriteria(10);
		scriteria.setGameid(idgames);
		scriteria.setPagingType(EntityPaging.Type.PAGE.name());
		return scriteria;
	}

	/**
	 * Find.
	 * 
	 * @param id
	 *            the id
	 * @param complete
	 *            the complete
	 * @return the StoreItem
	 */
	public Storeitem find(final Long id, final boolean complete) {
		final Storeitem item = getEntityManager().find(Storeitem.class, id);
		if (complete && (item != null)) {
			item.getGame();
		}
		return refresh(item);
	}

	/**
	 * Save.
	 * 
	 * @param storeItem
	 *            the my item
	 * @return the item
	 */
	public Storeitem save(final Storeitem storeItem) {
		Storeitem item = null;
		getEntityManager().getTransaction().begin();
		item = getEntityManager().merge(storeItem);
		if (item != null) {
			getEntityManager().flush();
			getEntityManager().refresh(item);
		}
		getEntityManager().getTransaction().commit();
		return item;
	}

	/**
	 * Creates the.
	 * 
	 * @param myItem
	 *            the my item
	 * @return the item
	 */
	public Storeitem create(final Storeitem myItem) {
		Storeitem item = null;
		getEntityManager().getTransaction().begin();
		getEntityManager().persist(myItem);
		getEntityManager().flush();
		getEntityManager().getTransaction().commit();
		item = refresh(myItem);
		return item;
	}

	/**
	 * Delete.
	 * 
	 * @param item
	 *            the item
	 * @return the item
	 */
	public Storeitem delete(final Storeitem item) {
		getEntityManager().getTransaction().begin();
		final Game emp = getEntityManager().find(Game.class, item.getIdstoreitem());
		getEntityManager().remove(emp);
		getEntityManager().flush();
		getEntityManager().getTransaction().commit();
		return null;
	}

	/**
	 * Delete.
	 * 
	 * @param idstoreItems
	 *            the idstoreItems
	 * @return the long
	 */
	public Long delete(final Long idstoreItems) {
		getEntityManager().getTransaction().begin();
		final Storeitem item = getEntityManager().find(Storeitem.class, idstoreItems);
		getEntityManager().remove(item);
		getEntityManager().flush();
		getEntityManager().getTransaction().commit();
		return null;
	}

	/**
	 * Refresh.
	 * 
	 * @param myItem
	 *            the item
	 * @return the item
	 */
	public Storeitem refresh(final Storeitem myItem) {
		final Storeitem item = getEntityManager().find(Storeitem.class, myItem.getIdstoreitem());
		getEntityManager().refresh(item);
		return item;
	}

	/**
	 * Gets the paging.
	 * 
	 * @param criteria
	 *            the criteria
	 * @return the paging
	 */
	public EntityPaging<Storeitem> getPaging(final ShopCriteria criteria) {
		return criteria.getPaging(getEntityManager().getEntityManagerFactory());
	}

	/**
	 * Gets the games.
	 * 
	 * @param criteria
	 *            the criteria
	 * @return the shop items
	 */
	public List<Storeitem> getShopItems(final ShopCriteria criteria) {
		final CriteriaQuery<Storeitem> cq = criteria.createQuery(getEntityManager());
		return getEntityManager().createQuery(cq).getResultList();
	}
}
