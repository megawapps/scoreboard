/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.dao;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;

/**
 * The Class DozerHelper.
 * 
 * @author Giwi Softwares
 */
public class DozerHelper {

	/** The instance. */
	private static DozerHelper INSTANCE;

	/** The mapper. */
	private DozerBeanMapper mapper;

	/**
	 * Instantiates a new dozer helper.
	 */
	private DozerHelper() {
		final List<String> myMappingFiles = new ArrayList<String>();
		myMappingFiles.add("mapping.xml");
		myMappingFiles.add("mobilemapping.xml");
		myMappingFiles.add("adminMapping.xml");
		mapper = new DozerBeanMapper();
		mapper.setMappingFiles(myMappingFiles);
	}

	/**
	 * Gets the single instance of DozerHelper.
	 * 
	 * @return single instance of DozerHelper
	 */
	public static DozerHelper getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new DozerHelper();
		}
		return INSTANCE;
	}

	/**
	 * Gets the mapper.
	 * 
	 * @return the mapper
	 */
	public DozerBeanMapper getMapper() {
		return mapper;
	}

	/**
	 * Sets the mapper.
	 * 
	 * @param mapper
	 *            the new mapper
	 */
	public void setMapper(final DozerBeanMapper mapper) {
		this.mapper = mapper;
	}
}
