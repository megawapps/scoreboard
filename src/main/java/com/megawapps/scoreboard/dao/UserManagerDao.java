/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import com.megawapps.scoreboard.entities.Token;
import com.megawapps.scoreboard.entities.User;
import com.megawapps.scoreboard.tools.criteria.UserCriteria;
import com.megawapps.scoreboard.tools.paging.EntityPaging;

/**
 * The Class UserManagerDao.
 * 
 * @author Giwi Softwares
 */
public class UserManagerDao {

	/** The entity manager. */
	private EntityManager entityManager;
	/** The instance. */
	private static UserManagerDao INSTANCE;

	/**
	 * The Constructor.
	 */
	private UserManagerDao() {
		final EntityManagerFactory factory = Persistence.createEntityManagerFactory("scoreboard");
		entityManager = factory.createEntityManager();
	}

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static UserManagerDao getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new UserManagerDao();
		}
		return INSTANCE;
	}

	/**
	 * Gets the entity manager.
	 * 
	 * @return the entity manager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Sets the entity manager.
	 * 
	 * @param entityManager
	 *            the entity manager
	 */
	@PersistenceContext(unitName = "scoreboard")
	public void setEntityManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Gets the list of users.
	 * 
	 * @return the list of users
	 */
	public List<User> getListOfUsers() {
		final UserCriteria criteria = new UserCriteria(10);
		criteria.setPseudo(null);
		criteria.setPageSize(5);
		criteria.setPagingType(EntityPaging.Type.PAGE.name());
		return getUsers(criteria);
	}

	/**
	 * Find.
	 * 
	 * @param id
	 *            the id
	 * @param complete
	 *            the complete
	 * @return the user
	 */
	public User find(final Long id, final boolean complete) {
		final User user = getEntityManager().find(User.class, id);
		if (complete) {
			user.getShophistories();
			user.getShophistories();
		}
		user.getTokens();
		return refresh(user);
	}

	/**
	 * Save.
	 * 
	 * @param myUser
	 *            the my user
	 * @return the user
	 */
	public User save(final User myUser) {
		User user = null;
		getEntityManager().getTransaction().begin();
		user = getEntityManager().merge(myUser);
		if (user != null) {
			getEntityManager().flush();
		}
		getEntityManager().getTransaction().commit();
		user = refresh(myUser);
		return user;
	}

	/**
	 * Creates the.
	 * 
	 * @param myUser
	 *            the my user
	 * @return the user
	 */
	public User create(final User myUser) {
		User user = null;
		getEntityManager().getTransaction().begin();
		getEntityManager().persist(myUser);
		getEntityManager().flush();
		getEntityManager().getTransaction().commit();
		user = refresh(myUser);
		return user;
	}

	/**
	 * Delete.
	 * 
	 * @param user
	 *            the user
	 * @return the user
	 */
	public User delete(final User user) {
		getEntityManager().getTransaction().begin();
		final User emp = getEntityManager().find(User.class, user.getIduser());
		getEntityManager().remove(emp);
		getEntityManager().flush();
		getEntityManager().getTransaction().commit();
		return null;
	}

	/**
	 * Clean old tokens.
	 * 
	 * @param u
	 *            the u
	 */
	public void cleanOldTokens(final User u) {
		getEntityManager().getTransaction().begin();
		if (u.getTokens() == null) {
			u.setTokens(new ArrayList<Token>());
		}
		for (final Token t : u.getTokens()) {
			final Calendar cal = Calendar.getInstance();
			cal.setTime(t.getEnddate());
			if (cal.before(new Date())) {
				getEntityManager().remove(t);
			}
		}
		getEntityManager().flush();
		getEntityManager().getTransaction().commit();
	}

	/**
	 * Delete.
	 * 
	 * @param idusers
	 *            the idusers
	 * @return the long
	 */
	public Long delete(final Long idusers) {
		if (!getEntityManager().getTransaction().isActive()) {
			getEntityManager().getTransaction().begin();
		}
		final User u = getEntityManager().find(User.class, idusers);
		if ((u != null) && getEntityManager().contains(u)) {
			getEntityManager().remove(u);
		}
		getEntityManager().flush();
		getEntityManager().getTransaction().commit();
		return null;
	}

	/**
	 * Refresh.
	 * 
	 * @param user
	 *            the user
	 * @return the user
	 */
	public User refresh(final User user) {
		final User emp = getEntityManager().find(User.class, user.getIduser());
		getEntityManager().refresh(emp);
		return emp;
	}

	/**
	 * Gets the paging.
	 * 
	 * @param criteria
	 *            the criteria
	 * @return the paging
	 */
	public EntityPaging<User> getPaging(final UserCriteria criteria) {
		return criteria.getPaging(getEntityManager().getEntityManagerFactory());
	}

	/**
	 * Gets the users.
	 * 
	 * @param criteria
	 *            the criteria
	 * @return the users
	 */
	public List<User> getUsers(final UserCriteria criteria) {
		final CriteriaQuery<User> cq = criteria.createQuery(getEntityManager());
		return getEntityManager().createQuery(cq).getResultList();
	}

	/**
	 * Request token.
	 * 
	 * @param u
	 *            the u
	 */
	public void requestToken(final User u) {
		cleanOldTokens(u);
		if (u.getTokens().isEmpty()) {
			final Token t = new Token();
			final String id = UUID.randomUUID().toString();
			final Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.HOUR, 24);
			t.setValue(id);
			t.setEnddate(cal.getTime());
			t.setUser(u);
			u.getTokens().add(t);
			save(u);
		}
	}

	/**
	 * Exists.
	 * 
	 * @param email
	 *            the email
	 * @return true, if successful
	 */
	public boolean exists(final String email) {
		final UserCriteria criteria = new UserCriteria();
		criteria.setEmail(email);
		final List<User> lu = UserManagerDao.getInstance().getUsers(criteria);
		return lu.size() > 0;
	}
}
