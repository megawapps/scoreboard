/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.OptimisticLockException;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import com.megawapps.scoreboard.entities.Game;
import com.megawapps.scoreboard.entities.Score;
import com.megawapps.scoreboard.entities.User;
import com.megawapps.scoreboard.tools.criteria.ScoreCriteria;
import com.megawapps.scoreboard.tools.paging.EntityPaging;

/**
 * The Class ScoreManagerDao.
 */
public class ScoreManagerDao {

	/** The entity manager. */
	private EntityManager entityManager;

	/** The instance. */
	private static ScoreManagerDao INSTANCE;

	/**
	 * Instantiates a new score manager dao.
	 */
	private ScoreManagerDao() {
		final EntityManagerFactory factory = Persistence.createEntityManagerFactory("scoreboard");
		entityManager = factory.createEntityManager();
	}

	/**
	 * Gets the single instance of ScoreManagerDao.
	 * 
	 * @return single instance of ScoreManagerDao
	 */
	public static ScoreManagerDao getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ScoreManagerDao();
		}
		return INSTANCE;
	}

	/**
	 * Gets the entity manager.
	 * 
	 * @return the entity manager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Sets the entity manager.
	 * 
	 * @param entityManager
	 *            the new entity manager
	 */
	@PersistenceContext(unitName = "scoreboard")
	public void setEntityManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Gets the criteria.
	 * 
	 * @param idgames
	 *            the idgames
	 * @return the criteria
	 */
	public ScoreCriteria getCriteria(final long idgames) {
		final ScoreCriteria scriteria = new ScoreCriteria(10);
		scriteria.setGameid(idgames);
		scriteria.setPagingType(EntityPaging.Type.PAGE.name());
		return scriteria;
	}

	/**
	 * Find.
	 * 
	 * @param id
	 *            the id
	 * @param complete
	 *            the complete
	 * @return the score
	 */
	public Score find(final Long id, final boolean complete) {
		final Score score = getEntityManager().find(Score.class, id);
		if (complete && (score != null)) {
			score.getUser();
			score.getGame();
		}
		return refresh(score);
	}

	/**
	 * Save.
	 * 
	 * @param myScore
	 *            the my score
	 * @return the score
	 */
	public Score save(final Score myScore) {
		Score score = null;
		try {
			getEntityManager().getTransaction().begin();
			score = getEntityManager().merge(myScore);
			if (score != null) {
				getEntityManager().flush();
			}
			getEntityManager().getTransaction().commit();
		} catch (final OptimisticLockException ole) {
			return null;
		}

		return score;
	}

	/**
	 * Creates the.
	 * 
	 * @param myScore
	 *            the my score
	 * @return the score
	 */
	public Score create(final Score myScore) {
		final Score score = null;

		final Game g = GameManagerDao.getInstance().find(myScore.getGame().getIdgame(), false);
		final User u = UserManagerDao.getInstance().find(myScore.getUser().getIduser(), false);
		final ScoreCriteria sc = new ScoreCriteria();
		sc.setGameid(myScore.getGame().getIdgame());
		sc.setUserid(myScore.getUser().getIduser());
		final List<Score> listOfScores = getScores(sc);
		getEntityManager().getTransaction().begin();
		if (listOfScores.isEmpty()) {
			getEntityManager().persist(myScore);
			myScore.setGame(g);
			myScore.setUser(u);
		}
		for (final Score s : listOfScores) {
			s.setCoins(myScore.getCoins());
			if (myScore.getScore() > s.getScore()) {
				s.setScore(myScore.getScore());
				s.setScoredate(myScore.getScoredate());
			}
			getEntityManager().refresh(s);
		}

		getEntityManager().flush();
		getEntityManager().getTransaction().commit();

		return score;
	}

	/**
	 * Delete.
	 * 
	 * @param score
	 *            the score
	 * @return the score
	 */
	public Score delete(final Score score) {
		try {
			getEntityManager().getTransaction().begin();
			final Score sco = getEntityManager().find(Score.class, score.getIdscore());
			getEntityManager().remove(sco);
			getEntityManager().flush();
			getEntityManager().getTransaction().commit();
		} catch (final OptimisticLockException ole) {
			return score;
		}
		return null;
	}

	/**
	 * Delete.
	 * 
	 * @param idScore
	 *            the id score
	 * @return the long
	 */
	public Long delete(final Long idScore) {
		try {
			getEntityManager().getTransaction().begin();
			final Score sco = getEntityManager().find(Score.class, idScore);
			getEntityManager().remove(sco);
			getEntityManager().flush();
			getEntityManager().getTransaction().commit();
		} catch (final OptimisticLockException ole) {
			return idScore;
		}
		return null;
	}

	/**
	 * Refresh.
	 * 
	 * @param score
	 *            the score
	 * @return the score
	 */
	public Score refresh(final Score score) {
		final Score sco = getEntityManager().find(Score.class, score.getIdscore());
		getEntityManager().refresh(sco);
		sco.getUser();
		sco.getGame();
		return sco;
	}

	/**
	 * Gets the scores.
	 * 
	 * @param criteria
	 *            the criteria
	 * @return the scores
	 */
	public List<Score> getScores(final ScoreCriteria criteria) {
		final CriteriaQuery<Score> cq = criteria.createQuery(getEntityManager());
		return getEntityManager().createQuery(cq).getResultList();
	}

	/**
	 * Gets the paging.
	 * 
	 * @param criteria
	 *            the criteria
	 * @return the paging
	 */
	public EntityPaging<Score> getPaging(final ScoreCriteria criteria) {
		return criteria.getPaging(getEntityManager().getEntityManagerFactory());
	}

}