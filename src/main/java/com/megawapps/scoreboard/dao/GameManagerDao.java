/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.OptimisticLockException;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import com.megawapps.scoreboard.entities.Game;
import com.megawapps.scoreboard.tools.criteria.GameCriteria;
import com.megawapps.scoreboard.tools.paging.EntityPaging;

/**
 * The Class GameManagerDao.
 * 
 * @author Giwi Softwares
 */
public class GameManagerDao {

	/** The entity manager. */
	private EntityManager entityManager;

	/** The instance. */
	private static GameManagerDao INSTANCE;

	/**
	 * The Constructor.
	 */
	private GameManagerDao() {
		final EntityManagerFactory factory = Persistence.createEntityManagerFactory("scoreboard");
		entityManager = factory.createEntityManager();
	}

	/**
	 * Gets the list of games.
	 * 
	 * @param featured
	 *            the featured
	 * @return the list of games
	 */
	public List<Game> getListOfGames(final boolean featured) {
		final GameCriteria criteria = new GameCriteria(10);
		criteria.setName(null);
		criteria.setPageSize(5);
		criteria.setPagingType(EntityPaging.Type.PAGE.name());
		criteria.setFeatured(featured);
		return getGames(criteria);
	}

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static GameManagerDao getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new GameManagerDao();
		}
		return INSTANCE;
	}

	/**
	 * Gets the entity manager.
	 * 
	 * @return the entity manager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Sets the entity manager.
	 * 
	 * @param entityManager
	 *            the entity manager
	 */
	@PersistenceContext(unitName = "scoreboard")
	public void setEntityManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Find.
	 * 
	 * @param id
	 *            the id
	 * @param complete
	 *            the complete
	 * @return the game
	 */
	public Game find(final Long id, final boolean complete) {
		final Game game = getEntityManager().find(Game.class, id);
		if (complete && (game != null)) {
			game.getStoreitems();
		}
		return refresh(game);
	}

	/**
	 * Save.
	 * 
	 * @param myGame
	 *            the my game
	 * @return the game
	 */
	public Game save(final Game myGame) {
		Game game = null;
		try {
			getEntityManager().getTransaction().begin();
			game = getEntityManager().merge(myGame);
			if (game != null) {
				// getEntityManager().lock(game,
				// LockModeType.OPTIMISTIC_FORCE_INCREMENT);
				getEntityManager().flush();
				getEntityManager().refresh(game);
			}
			getEntityManager().getTransaction().commit();
		} catch (final OptimisticLockException ole) {
			return null;
		}
		return game;
	}

	/**
	 * Creates the.
	 * 
	 * @param myGame
	 *            the my game
	 * @return the game
	 */
	public Game create(final Game myGame) {
		Game game = null;
		try {
			getEntityManager().getTransaction().begin();
			getEntityManager().persist(myGame);
			getEntityManager().flush();
			getEntityManager().getTransaction().commit();
			game = refresh(myGame);
		} catch (final OptimisticLockException ole) {
			return null;
		}
		return game;
	}

	/**
	 * Delete.
	 * 
	 * @param game
	 *            the game
	 * @return the game
	 */
	public Game delete(final Game game) {
		try {
			getEntityManager().getTransaction().begin();
			final Game emp = getEntityManager().find(Game.class, game.getIdgame());
			getEntityManager().remove(emp);
			getEntityManager().flush();
			getEntityManager().getTransaction().commit();
		} catch (final OptimisticLockException ole) {
			return game;
		}
		return null;
	}

	/**
	 * Delete.
	 * 
	 * @param idgames
	 *            the idgames
	 * @return the long
	 */
	public Long delete(final Long idgames) {
		try {
			getEntityManager().getTransaction().begin();
			final Game game = getEntityManager().find(Game.class, idgames);
			getEntityManager().remove(game);
			getEntityManager().flush();
			getEntityManager().getTransaction().commit();

		} catch (final OptimisticLockException ole) {
			return idgames;
		}
		return null;
	}

	/**
	 * Refresh.
	 * 
	 * @param game
	 *            the game
	 * @return the game
	 */
	public Game refresh(final Game game) {
		final Game emp = getEntityManager().find(Game.class, game.getIdgame());
		getEntityManager().refresh(emp);
		game.getStoreitems();
		return emp;
	}

	/**
	 * Gets the paging.
	 * 
	 * @param criteria
	 *            the criteria
	 * @return the paging
	 */
	public EntityPaging<Game> getPaging(final GameCriteria criteria) {
		return criteria.getPaging(getEntityManager().getEntityManagerFactory());
	}

	/**
	 * Gets the games.
	 * 
	 * @param criteria
	 *            the criteria
	 * @return the games
	 */
	public List<Game> getGames(final GameCriteria criteria) {
		final CriteriaQuery<Game> cq = criteria.createQuery(getEntityManager());
		return getEntityManager().createQuery(cq).getResultList();
	}
}
