/*
 * Copyright Giwi Softwares 2013
 */
package com.megawapps.scoreboard.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.OptimisticLockException;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;

import com.megawapps.scoreboard.entities.Newsfeed;
import com.megawapps.scoreboard.tools.criteria.NewsfeedCriteria;
import com.megawapps.scoreboard.tools.paging.EntityPaging;

/**
 * The Class NewsManagerDao.
 */
public class NewsManagerDao {

	/** The entity manager. */
	private EntityManager entityManager;
	/** The instance. */
	private static NewsManagerDao INSTANCE;

	/**
	 * The Constructor.
	 */
	private NewsManagerDao() {
		final EntityManagerFactory factory = Persistence.createEntityManagerFactory("scoreboard");
		entityManager = factory.createEntityManager();
	}

	/**
	 * Gets the instance.
	 * 
	 * @return the instance
	 */
	public static NewsManagerDao getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new NewsManagerDao();
		}
		return INSTANCE;
	}

	/**
	 * Gets the list of newsfeed.
	 * 
	 * @return the list of newsfeed
	 */
	public List<Newsfeed> getListOfNewsfeed() {
		final NewsfeedCriteria criteria = new NewsfeedCriteria(10);
		criteria.setPageSize(5);
		criteria.setPagingType(EntityPaging.Type.PAGE.name());
		return getNews(criteria);
	}

	/**
	 * Gets the entity manager.
	 * 
	 * @return the entity manager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Sets the entity manager.
	 * 
	 * @param entityManager
	 *            the entity manager
	 */
	@PersistenceContext(unitName = "scoreboard")
	public void setEntityManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/**
	 * Find.
	 * 
	 * @param id
	 *            the id
	 * @param complete
	 *            the complete
	 * @return the newsfeed
	 */
	public Newsfeed find(final Long id, final boolean complete) {
		final Newsfeed news = getEntityManager().find(Newsfeed.class, id);
		if (complete && (news != null)) {
			news.getUser();
		}
		return refresh(news);
	}

	/**
	 * Save.
	 * 
	 * @param myNews
	 *            the my news
	 * @return the newsfeed
	 */
	public Newsfeed save(final Newsfeed myNews) {
		Newsfeed news = null;
		getEntityManager().getTransaction().begin();
		news = getEntityManager().merge(myNews);
		if (news != null) {
			getEntityManager().flush();
		}
		getEntityManager().getTransaction().commit();
		return news;
	}

	/**
	 * Creates the.
	 * 
	 * @param myNews
	 *            the my news
	 * @return the newsfeed
	 */
	public Newsfeed create(final Newsfeed myNews) {
		Newsfeed news = null;
		try {
			getEntityManager().getTransaction().begin();
			getEntityManager().persist(myNews);
			getEntityManager().flush();
			getEntityManager().getTransaction().commit();
			news = refresh(myNews);
		} catch (final OptimisticLockException ole) {
			return null;
		}
		return news;
	}

	/**
	 * Delete.
	 * 
	 * @param news
	 *            the news
	 * @return the newsfeed
	 */
	public Newsfeed delete(final Newsfeed news) {
		try {
			getEntityManager().getTransaction().begin();
			final Newsfeed emp = getEntityManager().find(Newsfeed.class, news.getIdnews());
			getEntityManager().remove(emp);
			getEntityManager().flush();
			getEntityManager().getTransaction().commit();
		} catch (final OptimisticLockException ole) {
			return news;
		}
		return null;
	}

	/**
	 * Delete.
	 * 
	 * @param idNews
	 *            the id news
	 * @return the long
	 */
	public Long delete(final Long idNews) {
		if (!getEntityManager().getTransaction().isActive()) {
			getEntityManager().getTransaction().begin();
		}
		final Newsfeed news = getEntityManager().find(Newsfeed.class, idNews);
		if ((news != null) && getEntityManager().contains(news)) {
			getEntityManager().remove(news);
		}
		getEntityManager().flush();
		getEntityManager().getTransaction().commit();
		return null;
	}

	/**
	 * Refresh.
	 * 
	 * @param news
	 *            the news
	 * @return the newsfeed
	 */
	public Newsfeed refresh(final Newsfeed news) {
		final Newsfeed emp = getEntityManager().find(Newsfeed.class, news.getIdnews());
		getEntityManager().refresh(emp);
		emp.getUser();
		return emp;
	}

	/**
	 * Gets the paging.
	 * 
	 * @param criteria
	 *            the criteria
	 * @return the paging
	 */
	public EntityPaging<Newsfeed> getPaging(final NewsfeedCriteria criteria) {
		return criteria.getPaging(getEntityManager().getEntityManagerFactory());
	}

	/**
	 * Gets the news.
	 * 
	 * @param criteria
	 *            the criteria
	 * @return the news
	 */
	public List<Newsfeed> getNews(final NewsfeedCriteria criteria) {
		final CriteriaQuery<Newsfeed> cq = criteria.createQuery(getEntityManager());
		return getEntityManager().createQuery(cq).getResultList();
	}
}
