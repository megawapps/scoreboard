<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html ng-app="megawapps" lang="fr">
<head>
<jsp:include page="tiles/libs.jsp" />

<script type="text/javascript" src="js/ngapps/public/megawapps.js"></script>
<script type="text/javascript" src="js/ngapps/public/scoreboard.js"></script>
<script type="text/javascript" src="js/ngapps/public/carousel.js"></script>
<script type="text/javascript" src="js/ngapps/public/newsfeed.js"></script>
<script type="text/javascript" src="js/ngapps/public/profil.js"></script>
<script type="text/javascript" src="js/ngapps/public/signup.js"></script>
</head>
<body>
	<jsp:include page="tiles/header.jsp">
		<jsp:param name="curPage" value="index" />
	</jsp:include>
	<div id="wrap">
		<div class="container">
			<div class="clearfix"></div>
			<div class="row">
				<div class="col-md-8">
					<div class="jumbotron">
						<div class="container">
							<img src="imgs/404.gif" />
							<h1>404 : page not found</h1>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div sb-left-menu></div>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="tiles/footer.jsp" />
</body>
</html>