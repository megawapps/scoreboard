<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="adminArea">
<head>
<jsp:include page="tiles/libs.jsp" />
<script type="text/javascript" src="js/ngapps/admin/admin.js"></script>
<script type="text/javascript" src="js/ngapps/admin/news.js"></script>
<script type="text/javascript" src="js/ngapps/admin/games.js"></script>
<script type="text/javascript" src="js/ngapps/admin/users.js"></script>
<script type="text/javascript" src="js/ngapps/admin/shop.js"></script>
<script type="text/javascript" src="js/ngapps/admin/missions.js"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script> -->

</head>
<body>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="modal-title"></h4>
				</div>
				<div class="modal-body" id="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-primary" id="modal-ok-btn">Ok</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<div class="modal fade" id="myDetailModal" tabindex="-1" role="dialog" aria-labelledby="myDetailModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="Detailmodal-title"></h4>
				</div>
				<div class="modal-body" id="Detailmodal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->


	<jsp:include page="tiles/header.jsp">
		<jsp:param name="curPage" value="admin" />
	</jsp:include>
	<div id="wrap">
		<div class="container">
			<div class="clearfix"></div>
			<c:if test="${sessionScope.user == null || sessionScope.user.admin != true}">
				<div class="jumbotron">
					<div class="container">
						<h1>
							<img src="imgs/denied.png" alt="..."> Hey, you're not allowed to be here!
						</h1>
						<p>Maybe you're not an admin, or you've not log in yet.</p>
					</div>
				</div>
			</c:if>
			<c:if test="${sessionScope.user != null && sessionScope.user.admin == true}">
				<div class="row">
					<div class="col-md-2">
						<div class="panel panel-default">
							<!-- Default panel contents -->
							<div class="panel-heading">
								<a href="#"><span class="glyphicon glyphicon-wrench"></span> Administration</a>
							</div>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="#/news"><span class="glyphicon glyphicon-comment"></span> Les news</a></li>
								<li><a href="#/games"><span class="glyphicon glyphicon-tower"></span> Les jeux</a></li>
								<li><a href="#/users"><span class="glyphicon glyphicon-user"></span> Les gamers</a></li>
								<li><a href="#/shop"><span class="glyphicon glyphicon-gift"></span> Le shop</a></li>
								<li><a href="#/shopcarts"><span class="glyphicon glyphicon-shopping-cart"></span> Les commandes</a></li>
								<li><a href="#/missions"><span class="glyphicon glyphicon-tasks"></span> Les missions</a></li>
								<li><a href="#/mailing"><span class="glyphicon glyphicon-bullhorn"></span> Mailing</a></li>
								<li><a href="#/misc"><span class="glyphicon glyphicon-cog"></span> Divers</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-10">
						<div ng-view></div>
					</div>
				</div>
			</c:if>
		</div>
	</div>
	<jsp:include page="tiles/footer.jsp" />
</body>
</html>