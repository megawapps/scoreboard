<!doctype html>
<html ng-app="megawapps"  >
<head>
<jsp:include page="tiles/libs.jsp" />

<script type="text/javascript" src="js/ngapps/public/megawapps.js"></script>
<script type="text/javascript" src="js/ngapps/public/scoreboard.js"></script>
<script type="text/javascript" src="js/ngapps/public/carousel.js"></script>
<script type="text/javascript" src="js/ngapps/public/newsfeed.js"></script>
<script type="text/javascript" src="js/ngapps/public/profil.js"></script>
<script type="text/javascript" src="js/ngapps/public/signup.js"></script>
<script type="text/javascript" src="js/ngapps/public/shop.js"></script>
</head>
<body>
	<jsp:include page="tiles/header.jsp">
		<jsp:param name="curPage" value="index" />
	</jsp:include>
	<div id="wrap">
		<ng-view></ng-view>
	</div>
	<jsp:include page="tiles/footer.jsp" />
</body>
</html>