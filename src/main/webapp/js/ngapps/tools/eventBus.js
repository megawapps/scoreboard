angular.module('eventbus', [])

.factory('eventbus', function($rootScope) {
	sharedService = {};

	sharedService.message = '';
	sharedService.data = {};

	sharedService.prepForBroadcast = function(msg, data) {
		this.message = msg;
		this.data = data;
		this.broadcastItem();
	};

	sharedService.broadcastItem = function() {
		$rootScope.$broadcast('eventbus');
	};

	return sharedService;
});
