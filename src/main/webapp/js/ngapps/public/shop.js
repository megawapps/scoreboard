angular.module('sbShop', [])

.controller('ShopCtrl', function($scope, $http, eventbus, $location) {
	eventbus.prepForBroadcast("menuPage", "shop");
	$http.get('api/users/current').success(function(data) {
		if (!data.iduser) {
			$location.path('#/');
		}
	}).error(function(error) {
		console.error(error);
	});
	$http.get('api/game').success(function(data) {
		$scope.games = data;
		if ($scope.games.length > 0) {
			$scope.idgame = $scope.games[0].idgame;
		}
	}).error(function(error) {
		console.error(error);
	});
})

.controller('ShopingCartCtrl', function($scope, $http, eventbus, $location, $translate) {
	$scope.cart = new Array();
	$scope.total = 0;
	$scope.loaded = false;
	eventbus.prepForBroadcast("menuPage", "shopingcart");
	$http.get('api/users/current').success(function(data) {
		if (!data.iduser) {
			$location.path('#/');
		} else {
			$http.get('api/shop/cart').success(function(data) {
				$scope.compute(data);
				$scope.loaded = true;
			}).error(function(error) {
				console.error(error);
				$scope.loaded = true;
			});
		}
	}).error(function(error) {
		console.error(error);
		$scope.loaded = true;
	});

	$scope.compute = function(data) {
		$scope.cart = new Array();
		$scope.total = 0;
		for (var j = 0; j < data.length; j++) {
			var s = data[j];
			var found = false;
			for (var i = 0; i < $scope.cart.length; i++) {
				if ($scope.cart[i].idstoreitem == s.idstoreitem) {
					$scope.cart[i].count += 1;
					$scope.cart[i].total = $scope.cart[i].price * $scope.cart[i].count;
					found = true;
					break;
				}
			}
			if (!found) {
				s.count = 1;
				s.total = s.price;
				$scope.cart.push(s);
			}
		}
		for (var i = 0; i < $scope.cart.length; i++) {
			$scope.total += $scope.cart[i].total;
		}
	};

	$scope.remove = function(id, title) {
		$scope.loaded = false;
		$http.post('api/shop/removeitem/' + id).success(function(data) {
			toastr.success(title + " " + $translate('shop.rem.succes'));
			$scope.compute(data);
			$scope.loaded = true;
		}).error(function(error) {
			console.error(error);
			$scope.loaded = true;
		});
	};

	$scope.validate = function() {
		$scope.loaded = false;
		$http.post('api/shop/validatecart').success(function(data) {
			toastr.success($translate('shop.val.succes'));
			$scope.compute(data);
			$scope.loaded = true;
		}).error(function(error) {
			console.error(error);
			$scope.loaded = true;
		});
	};
})

.directive("sbGameShop", function($translate) {
	return {
		scope : {
			idgame : '@',
			name : '@'
		},
		controller : function($http, $scope) {
			$scope.range = [];
			$scope.items = null;
			$scope.page = 1;
			$scope.pagecount = 0;
			$http.get('api/shop/pages/' + $scope.idgame).success(function(data) {
				$scope.pagecount = data;
				$scope.range = new Array(eval(data));
			}).error(function(error) {
				console.error(error);
			});

			$http.get('api/shop/active/' + $scope.idgame + '/' + $scope.page).success(function(data) {
				$scope.items = data;
			}).error(function(error) {
				console.error(error);
			});

			$scope.jump = function(page) {
				$scope.page = page;
				$http.get('api/shop/active/' + $scope.idgame + '/' + page).success(function(data) {
					$scope.items = data;
				}).error(function(error) {
					console.error(error);
				});
			};

			$scope.addItemToCart = function(idstoreitem, title) {
				$http.post('api/shop/addItem/' + idstoreitem).success(function(data) {
					$scope.cart = data;
					toastr.success(title + " " + $translate('shop.add.succes'));
				}).error(function(error) {
					console.error(error);
				});
			};
		},
		templateUrl : 'templates/public/sbGameShop.html'
	};
})

//
;

