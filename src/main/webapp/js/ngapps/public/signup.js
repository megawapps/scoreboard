angular.module('sbUserSignup', [])

.controller('SignupCtrl', function($scope, $http, eventbus) {
	eventbus.prepForBroadcast("menuPage", "index");
	$scope.error = {};
	$scope.done = "";
	angular.element('#birthdate').datepicker();
	angular.element('#birthdate').blur(function() {
		angular.element('#birthdate').datepicker('hide');
	});
	$scope.isDone = false;
	$scope.signup = {};
	$scope.signup.name = "";

	$scope.startUploading = function() {
		$scope.done = "";
		$scope.isDone = true;
	};

	$scope.complete = function(content) {
		if ($scope.isDone && $scope.signup_form.$valid) {
			$scope.done = "true";
		}
	};
})
//
;