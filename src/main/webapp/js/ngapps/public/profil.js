angular.module('sbUserProfil', [])

.controller('ProfilCtrl', function($scope, $http, eventbus, $location, $translate) {
	eventbus.prepForBroadcast("menuPage", "profil");
	$scope.error = {};
	angular.element('#birthdate').datepicker();
	$http.get('api/users/current').success(function(data) {
		if (data.iduser) {
			$http.get('api/users/' + data.iduser).success(function(data) {
				$scope.user = data;
			}).error(function(error) {
				console.error(error);
			});
		} else {
			$location.path('#/');
		}
	}).error(function(error) {
		console.error(error);
	});

	$scope.isDone = false;
	$scope.startUploading = function() {
		$scope.isDone = true;
	};

	$scope.complete = function(content) {
		if ($scope.isDone) {
			toastr.success($translate('profile.success'));
			$http.get('api/users/current').success(function(data) {
				if (data.iduser) {
					$http.get('api/users/' + data.iduser).success(function(data) {
						$scope.user = data;

					}).error(function(error) {
						console.error(error);
					});
				}
			}).error(function(error) {
				console.error(error);
			});
		}
	};
})
//
;