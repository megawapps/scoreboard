angular.module('sbNewsfeed', [])

.directive('sbNewsfeed', function($http) {
	return {
		scope : true,
		templateUrl : 'templates/public/newsfeed.html',
		controller : function($scope, $http) {
			$scope.range = [];
			$scope.news = null;
			$scope.page = 1;
			$scope.pagecount = 0;

			$http.get('api/news/pages/').success(function(data) {
				$scope.pagecount = eval(data);
				$scope.range = new Array(eval(data));
			}).error(function(error) {
				console.error(error);
			});

			$http.get('api/news/page/' + $scope.page).success(function(data) {
				$scope.news = data;
			}).error(function(error) {
				console.error(error);
			});

			$scope.jump = function(page) {
				$scope.page = page;
				$http.get('api/news/page/' + page).success(function(data) {
					$scope.news = data;
				}).error(function(error) {
					console.error(error);
				});
			};
		}
	};
})
//
;