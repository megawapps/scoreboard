angular.module('sbLogin', [ 'pascalprecht.translate' ])

.directive('sbLoginForm', function($http, eventbus, $translate) {
	return {
		scope : {
			pageName : '@'
		},
		templateUrl : 'templates/public/login.html',
		controller : function($scope, $http, $translate) {
			$scope.user = null;
			$http.get('api/users/current').success(function(data) {
				if (data.iduser) {
					$scope.user = data;
					if (!$scope.user.active) {
						toastr.warning($translate("header.inactive"));
					}
				}
			}).error(function(error) {
				console.error(error);
			});

			$scope.logoff = function() {
				$http.get('api/login/logoff').success(function(data) {
					$scope.user = null;
				}).error(function(error) {
					console.error(error);
				});
			};

			$scope.$on('eventbus', function() {
				if ("menuPage" == eventbus.message) {
					$scope.pageName = eventbus.data;
				}
			});
		},

		link : function(scope, element, attrs) {
			scope.isDone = false;

			scope.startUploading = function() {
				scope.isDone = true;
			};

			scope.uploadComplete = function(content) {
				$("#login_email").tooltip('hide');
				if (scope.isDone) {
					$http.get('api/users/current').success(function(data) {
						if (content.error) {
							toastr.error(content.error);
						} else {
							if (data.errorMap && data.errorMap.error) {
								toastr.error($translate("header." + data.errorMap.error));
							} else {
								scope.user = data;
								if (!scope.user.active) {
									toastr.warning($translate("header.inactive"));
								}
							}
						}
					}).error(function(error) {
						console.error(error);
					});
				}
			};
		}
	};
})
//
;