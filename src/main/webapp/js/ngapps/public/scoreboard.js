angular.module('sbScoreBoard', [])

.controller('ScoreboardCtrl', function($scope, $http, eventbus) {
	eventbus.prepForBroadcast("menuPage", "scores");
	$http.get('api/game').success(function(data) {
		$scope.games = data;
	}).error(function(error) {
		console.error(error);
	});
})

.directive("sbGameDetail", function() {
	return {
		scope : {
			idgame : '@',
			name : '@'
		},
		controller : function($http, $scope) {
			$scope.range = [];
			$scope.scores = null;
			$scope.page = 1;
			$scope.pagecount = 0;
			$http.get('api/score/pages/' + $scope.idgame).success(function(data) {
				$scope.pagecount = data;
			}).error(function(error) {
				console.error(error);
			});
			$http.get('api/score/game/' + $scope.idgame + '/' + $scope.page).success(function(data) {
				$scope.scores = data;
			}).error(function(error) {
				console.error(error);
			});
		},
		templateUrl : 'templates/public/sbGameDetail.html'
	};
})

// 
;