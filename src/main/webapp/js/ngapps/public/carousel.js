angular.module('sbCarousel', [])

.directive("sbCarousel", function($http) {
	return {
		scope : true,
		controller : function($scope, $http) {
			$http.get('api/game/featured').success(function(data) {
				$scope.games = data;
			}).error(function(error) {
				console.error(error);
			});

			$scope.ensureP = function(txt) {
				return $(txt).text();
			};
		},
		templateUrl : 'templates/public/carousel.html'
	};
});