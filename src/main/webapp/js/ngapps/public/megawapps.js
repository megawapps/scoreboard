angular.module('megawapps',
		[ 'ngRoute', 'eventbus', 'ngUpload', 'ngSanitize', 'pascalprecht.translate', 'utilities', 'sbCarousel', 'sbLogin', 'sbNewsfeed', 'sbScoreBoard', 'sbUserProfil', 'sbUserSignup', 'sbShop' ])

.config([ '$translateProvider', function($translateProvider) {
	$translateProvider.useUrlLoader('api/translate');
	$translateProvider.preferredLanguage('en');
} ])

.config(function($routeProvider) {
	$routeProvider

	.when('/', {
		controller : 'MainPageCtrl',
		templateUrl : 'templates/public/main.html'
	})

	.when('/scores', {
		controller : 'ScoreboardCtrl',
		templateUrl : 'templates/public/scoreboard.html'
	})

	.when('/about', {
		controller : 'AboutCtrl',
		templateUrl : 'templates/public/about.html'
	})

	.when('/profil', {
		controller : 'ProfilCtrl',
		templateUrl : 'templates/public/profil.html'
	})

	.when('/game/:idgame', {
		controller : 'GamePageCtrl',
		templateUrl : 'templates/public/gameSheet.html'
	})

	.when('/account/ok', {
		controller : 'AccountOKCtrl',
		templateUrl : 'templates/public/accountOk.html'
	})

	.when('/account/ko', {
		controller : 'AccountKOCtrl',
		templateUrl : 'templates/public/accountKo.html'
	})

	.when('/signup', {
		controller : 'SignupCtrl',
		templateUrl : 'templates/public/signup.html'
	})

	.when('/shop', {
		controller : 'ShopCtrl',
		templateUrl : 'templates/public/shop.html'
	})

	.when('/shopingcart', {
		controller : 'ShopingCartCtrl',
		templateUrl : 'templates/public/shopingcart.html'
	})

	.when('/game/:idgame', {
		controller : 'GamePageCtrl',
		templateUrl : 'templates/public/gameSheet.html'
	})
	// Default
	.otherwise({
		redirectTo : '/'
	});
})

.controller('MainPageCtrl', function($scope, $http, eventbus) {
	eventbus.prepForBroadcast("menuPage", "index");
})

.controller('AccountOKCtrl', function($scope, $http, eventbus) {
	eventbus.prepForBroadcast("menuPage", "index");
})

.controller('AccountKOCtrl', function($scope, $http, eventbus) {
	eventbus.prepForBroadcast("menuPage", "index");
})

.controller('AboutCtrl', function($scope, $http, eventbus) {
	eventbus.prepForBroadcast("menuPage", "about");
})

.controller('GamePageCtrl', function($scope, $http, $routeParams, eventbus) {
	eventbus.prepForBroadcast("menuPage", "gameInfo");
	$scope.idgame = $routeParams.idgame;
	$http.get('api/game/' + $scope.idgame).success(function(data) {
		$scope.curgame = data;
		console.error(data);
	}).error(function(error) {
		console.error(error);
	});

	$scope.ensureP = function(txt) {
		return $(txt).text();
	};
})

.directive("sbLeftMenu", function() {
	return {
		scope : {
			idgame : '@'
		},
		controller : function($http, $scope) {
			$http.get('api/game').success(function(data) {
				$scope.games = data;
			}).error(function(error) {
				console.error(error);
			});
		},
		templateUrl : 'templates/public/leftMenu.html'
	};
})
//
;