angular.module('usersAdmin', [ 'utilities' ])

.controller('UserListCtrl', function($scope, $http, $location) {

	$http.get('api/users').success(function(data) {
		$scope.users = data;
	}).error(function(error) {
		console.error(error);
	});

	$scope.del = function(iduser, username) {
		modalConfirm("Delete game", "About to delete " + username + ".<br />Are you sure?", function() {
			$http({
				method : 'DELETE',
				url : 'api/users/' + iduser
			}).success(function(data) {
				if ("ok" == data) {
					toastr.success(username + " successfully deleted!");
					$location.path('/users');
				} else {
					showError(data);
				}
			});
		});
	};

})

.controller('NewUserCtrl', function($scope, $http, $location) {
	$scope.isDone = false;
	$scope.curuser = {};
	$scope.curuser.name = "Anonyme";
	$http.get('api/users/current').success(function(data) {
		$scope.user = data;
	}).error(function(error) {
		console.error(error);
	});
	$scope.startUploading = function() {
		$scope.isDone = true;
	};

	$scope.complete = function(content) {
		if ($scope.isDone) {
			toastr.success($scope.user.name + " successfully created!");
			$location.path('/users');
		}
	};
})

.controller('EditUserCtrl', function($scope, $http, $routeParams, $location) {
	var iduser = $routeParams.iduser;
	$scope.user = {};
	$scope.isDone = false;

	$http.get('api/users/current').success(function(data) {
		$scope.user = data;
	}).error(function(error) {
		console.error(error);
	});
	$scope.startUploading = function() {
		$scope.isDone = true;
	};

	$scope.complete = function(content) {
		if ($scope.isDone) {
			toastr.success($scope.user.name + " successfully updated!");
			$location.path('/users');
		}
	};

	$http.get('api/users/' + iduser).success(function(data) {
		$scope.curuser = data;
		if ($scope.curuser.admin) {
			$scope.curuser.admin = true;
		} else {
			$scope.curuser.admin = false;
		}
		if ($scope.curuser.author) {
			$scope.curuser.author = true;
		} else {
			$scope.curuser.author = false;
		}
	}).error(function(error) {
		console.error(error);
	});
});