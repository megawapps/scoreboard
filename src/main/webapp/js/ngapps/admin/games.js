angular.module('gamesAdmin', [ 'utilities' ])

.controller('GamesListCtrl', function($scope, $http, $location) {

	$http.get('api/game').success(function(data) {
		$scope.games = data;
	}).error(function(error) {
		console.error(error);
	});

	$scope.del = function(idgame, gameName) {
		modalConfirm("Delete game", "About to delete " + gameName + ".<br />Are you sure?", function() {
			$http({
				method : 'DELETE',
				url : 'api/game/' + idgame
			}).success(function(data) {
				if ("ok" == data) {
					toastr.success(gameName + " successfully deleted!");
					$location.path('/games');
				} else {
					showError(data);
				}
			});
		});
	};

	$scope.ensureP = function(txt) {
		return $(txt).text();
	};
})

.controller('NewGamesCtrl', function($scope, $http, $location) {
	$scope.isDone = false;
	$scope.curgame = {};
	$scope.curgame.name = "Sans titre";
	$http.get('api/users/current').success(function(data) {
		$scope.user = data;
	}).error(function(error) {
		console.error(error);
	});
	$scope.startUploading = function() {
		toastr.success($scope.curgame.name + " successfully created!");
		$scope.isDone = true;
	};

	$scope.complete = function(content) {
		if ($scope.isDone) {
			$location.path('/games');
		}
	};
})

.controller('EditGamesCtrl', function($scope, $http, $routeParams, $location) {
	var idgame = $routeParams.idgame;
	$scope.user = {};
	$scope.isDone = false;

	$http.get('api/users/current').success(function(data) {
		$scope.user = data;
	}).error(function(error) {
		console.error(error);
	});
	$scope.startUploading = function() {
		$scope.isDone = true;
	};

	$scope.complete = function(content) {
		if ($scope.isDone) {
			toastr.success($scope.curgame.name + " successfully updated!");
			$location.path('/games');
		}
	};

	$http.get('api/game/' + idgame).success(function(data) {
		$scope.curgame = data;
		if ($scope.curgame.featured) {
			$scope.curgame.featured = true;
		} else {
			$scope.curgame.featured = false;
		}
		$scope.curgame.pf = {};
		var ptab = $scope.curgame.plateform.split(":");
		for ( var i = 0; i < ptab.length; i++) {
			if ("Android" == ptab[i]) {
				$scope.curgame.pf.android = true;
			}
			if ("IPhone" == ptab[i]) {
				$scope.curgame.pf.iphone = true;
			}
			if ("Web" == ptab[i]) {
				$scope.curgame.pf.web = true;
			}
		}
	}).error(function(error) {
		console.error(error);
	});
});
