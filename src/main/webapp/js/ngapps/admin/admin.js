angular.module('adminArea',
		[ 'ngRoute', 'eventbus', 'ngUpload', 'ngSanitize', 'utilities', 'sbLogin', 'pascalprecht.translate', 'newsAdmin', 'gamesAdmin', 'usersAdmin', 'shopAdmin', 'missionsAdmin' ])

.config([ '$translateProvider', function($translateProvider) {
	$translateProvider.useUrlLoader('api/translate');
	$translateProvider.preferredLanguage('en');
} ])

.config(function($routeProvider) {
	$routeProvider.when('/', {
		controller : 'AdminMainCtrl',
		templateUrl : 'templates/admin/welcome.html'
	})
	// News routing
	.when('/news', {
		controller : 'NewsListCtrl',
		templateUrl : 'templates/admin/newslist.html'
	}).when('/news/new', {
		controller : 'NewNewsCtrl',
		templateUrl : 'templates/admin/newnews.html'
	}).when('/news/edit/:idnews', {
		controller : 'EditNewsCtrl',
		templateUrl : 'templates/admin/newnews.html'
	})
	// games routing
	.when('/games', {
		controller : 'GamesListCtrl',
		templateUrl : 'templates/admin/gamelist.html'
	}).when('/games/new', {
		controller : 'NewGamesCtrl',
		templateUrl : 'templates/admin/newgame.html'
	}).when('/games/edit/:idgame', {
		controller : 'EditGamesCtrl',
		templateUrl : 'templates/admin/newgame.html'
	})
	// Users routing
	.when('/users', {
		controller : 'UserListCtrl',
		templateUrl : 'templates/admin/userslist.html'
	}).when('/users/new', {
		controller : 'NewUserCtrl',
		templateUrl : 'templates/admin/newuser.html'
	}).when('/users/edit/:iduser', {
		controller : 'EditUserCtrl',
		templateUrl : 'templates/admin/newuser.html'
	})
	// Shop routing
	.when('/shop', {
		controller : 'ShopListCtrl',
		templateUrl : 'templates/admin/shoplist.html'
	}).when('/shop/new', {
		controller : 'NewShopCtrl',
		templateUrl : 'templates/admin/newshop.html'
	}).when('/shop/edit/:id', {
		controller : 'EditShopCtrl',
		templateUrl : 'templates/admin/newshop.html'
	})
	// Missions routing
	.when('/missions', {
		controller : 'MissionListCtrl',
		templateUrl : 'templates/admin/missionlist.html'
	}).when('/missions/new', {
		controller : 'NewMissionCtrl',
		templateUrl : 'templates/admin/newmission.html'
	}).when('/missions/edit/:idmission', {
		controller : 'EditMissionCtrl',
		templateUrl : 'templates/admin/newmission.html'
	})

	// Mailing routing
	.when('/mailing', {
		controller : 'MailingCtrl',
		templateUrl : 'templates/admin/maillng.html'
	})
	// Misc routing
	.when('/misc', {
		controller : 'MiscCtrl',
		templateUrl : 'templates/admin/misc.html'
	})
	// Default
	.otherwise({
		redirectTo : '/'
	});
})

.controller('AdminMainCtrl', function($scope, $http) {
	$http.get('api/admin').success(function(data) {
		$scope.data = data;
	}).error(function(error) {
		console.error(error);
	});
})

.controller('MailingCtrl', function($scope, $http) {
})

.controller('MiscCtrl', function($scope, $http) {
})

//
;

