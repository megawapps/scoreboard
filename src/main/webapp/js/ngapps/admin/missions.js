angular.module('missionsAdmin', [])

.controller('MissionListCtrl', function($scope, $http, $location) {
	$scope.page = 1;
	$http.get('api/mission').success(function(data) {
		$scope.missions = data;
	}).error(function(error) {
		console.error(error);
	});

	$scope.del = function(idmission, title) {
		modalConfirm("Delete Mission", "About to delete " + title + ".<br />Are you sure?", function() {
			$http({
				method : 'DELETE',
				url : 'api/mission/' + idmission
			}).success(function(data) {
				if ("ok" == data) {
					toastr.success(title + " successfully deleted!");
					$location.path('/missions');
				} else {
					showError(data);
				}
			});
		});
	};

	$scope.ensureP = function(txt) {
		return $(txt).text();
	};

})

.controller('NewMissionCtrl', function($scope, $http, $location) {
	$scope.isDone = false;
	$scope.curmission = {
		description : '',
		title : "Sans titre",
		idmission : '',
		game : {
			idgame : ''
		}
	};
	$http.get('api/game').success(function(data) {
		$scope.games = data;
		console.error(data);
	}).error(function(error) {
		console.error(error);
	});
	$scope.startUploading = function() {

		$scope.isDone = true;
	};

	$scope.complete = function(content) {
		if ($scope.isDone) {
			toastr.success($scope.curmission.title + " successfully created!");
			$location.path('/missions');
		}
	};
})

.controller('EditMissionCtrl', function($scope, $http, $routeParams, $location) {
	$scope.isDone = false;
	$scope.rule = {
		idrule : '',
		description : "",
		idmission : $routeParams.idmission
	};
	$http.get('api/mission/' + $scope.rule.idmission).success(function(data) {
		$scope.curmission = data;
	}).error(function(error) {
		console.error(error);
	});
	$http.get('api/game').success(function(data) {
		$scope.games = data;
	}).error(function(error) {
		console.error(error);
	});

	$scope.startUploading = function() {
		$scope.isDone = true;
	};

	$scope.complete = function(content) {
		if ($scope.isDone) {
			toastr.success($scope.curmission.title + " successfully updated!");
			$location.path('/missions');
		}
	};

	$scope.addNewRule = function() {
		$http.post('api/rules', $scope.rule).success(function(data) {
			toastr.success("Successfully created!");
			$http.get('api/mission/' + $scope.rule.idmission).success(function(data) {
				$scope.curmission = data;
				$scope.rule = {
					idrule : "",
					description : "",
					idmission : $routeParams.idmission
				};
			}).error(function(error) {
				console.error(error);
			});
		}).error(function(error) {
			console.error(error);
		});
	};

	$scope.addNewRule = function() {
		$http.post('api/rules', $scope.rule).success(function(data) {
			if ($scope.rule.idrule != "") {
				toastr.success("Successfully updated!");
			} else {
				toastr.success("Successfully created!");
			}
			$http.get('api/mission/' + $scope.rule.idmission).success(function(data) {
				$scope.curmission = data;
				$scope.rule = {
					idrule : "",
					description : "{\"type\":\"\",\"amount\":0, \"meta\":\"\"}",
					idmission : $routeParams.idmission
				};
			}).error(function(error) {
				console.error(error);
			});
		}).error(function(error) {
			console.error(error);
		});
	};

	$scope.editRule = function(idrule) {
		$http.get('api/rules/' + idrule, $scope.rule).success(function(data) {
			$scope.rule = data;
			$scope.rule.idmission = $routeParams.idmission;
		}).error(function(error) {
			console.error(error);
		});
	};

	$scope.delRule = function(idrule) {
		$http({
			method : 'DELETE',
			url : 'api/rules/' + idrule
		}).success(function(data) {
			if ("ok" == data) {
				toastr.success("Rule successfully deleted!");
				$http.get('api/mission/' + $scope.rule.idmission).success(function(data) {
					$scope.curmission = data;
					$scope.rule = {
						idrule : "",
						description : "",
						idmission : $routeParams.idmission
					};
				}).error(function(error) {
					console.error(error);
				});
			} else {
				showError(data);
			}
		}).error(function(error) {
			console.error(error);
		});
	};
});