angular.module('shopAdmin', [])

.controller('ShopListCtrl', function($scope, $http, $location) {
	$scope.page = 1;
	$http.get('api/shop').success(function(data) {
		$scope.items = data;
	}).error(function(error) {
		console.error(error);
	});

	$scope.del = function(id, title) {
		modalConfirm("Delete Shop item", "About to delete " + title + ".<br />Are you sure?", function() {
			$http({
				method : 'DELETE',
				url : 'api/shop/' + id
			}).success(function(data) {
				if ("ok" == data) {
					toastr.success(title + " successfully deleted!");
					$location.path('/shop');
				} else {
					showError(data);
				}
			});
		});
	};

	$scope.ensureP = function(txt) {
		return $(txt).text();
	};

})

.controller('NewShopCtrl', function($scope, $http, $location) {
	$scope.isDone = false;
	$scope.curitem = {};
	$scope.curitem.title = "Sans titre";
	$http.get('api/game').success(function(data) {
		$scope.games = data;
	}).error(function(error) {
		console.error(error);
	});
	$scope.startUploading = function() {
		$scope.isDone = true;
	};

	$scope.complete = function(content) {
		if ($scope.isDone) {
			toastr.success($scope.curitem.title + " successfully created!");
			$location.path('/shop');
		}
	};
})

.controller('EditShopCtrl', function($scope, $http, $routeParams, $location) {
	$scope.isDone = false;
	var id = $routeParams.id;
	$http.get('api/game').success(function(data) {
		$scope.games = data;
	}).error(function(error) {
		console.error(error);
	});
	$scope.startUploading = function() {
		$scope.isDone = true;
	};

	$scope.complete = function(content) {
		if ($scope.isDone) {
			toastr.success($scope.curitem.title + " successfully updated!");
			$location.path('/shop');
		}
	};

	$http.get('api/shop/' + id).success(function(data) {
		$scope.curitem = data;
	}).error(function(error) {
		console.error(error);
	});
});