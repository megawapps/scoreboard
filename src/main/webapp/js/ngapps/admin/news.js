angular.module('newsAdmin', [])

.controller('NewsListCtrl', function($scope, $http, $location) {
	$scope.page = 1;
	$http.get('api/news').success(function(data) {
		$scope.news = data;
	}).error(function(error) {
		console.error(error);
	});

	$scope.del = function(idnews, title) {
		modalConfirm("Delete News", "About to delete " + title + ".<br />Are you sure?", function() {
			$http({
				method : 'DELETE',
				url : 'api/news/' + idnews
			}).success(function(data) {
				if ("ok" == data) {
					toastr.success(title + " successfully deleted!");
					$location.path('/news');
				} else {
					showError(data);
				}
			});
		});
	};

	$scope.ensureP = function(txt) {
		return $(txt).text();
	};

	// TODO : peut mieux faire avec un écran de détail
	$scope.show = function(idnews) {
		$http.get('api/news/' + idnews).success(function(data) {
			modalDetail(data.title, "<blockquote><p>" + data.content + "</p></blockquote>");
		}).error(function(error) {
			console.error(error);
		});
	};
})

.controller('NewNewsCtrl', function($scope, $http, $location) {
	$scope.user = {};
	$scope.isDone = false;
	$scope.curnews = {};
	$scope.curnews.title = "Sans titre";
	$http.get('api/users/current').success(function(data) {
		$scope.user = data;
	}).error(function(error) {
		console.error(error);
	});
	$scope.startUploading = function() {
		toastr.success($scope.curnews.title + " successfully created!");
		$scope.isDone = true;
	};

	$scope.complete = function(content) {
		if ($scope.isDone) {
			$location.path('/news');
		}
	};
})

.controller('EditNewsCtrl', function($scope, $http, $routeParams, $location) {
	var idnews = $routeParams.idnews;
	$scope.user = {};
	$scope.isDone = false;

	$http.get('api/users/current').success(function(data) {
		$scope.user = data;
	}).error(function(error) {
		console.error(error);
	});
	$scope.startUploading = function() {
		toastr.success($scope.curnews.title + " successfully updated!");
		$scope.isDone = true;
	};

	$scope.complete = function(content) {
		if ($scope.isDone) {
			toastr.success($scope.curnews.title + " successfully updated!");
			$location.path('/news');
		}
	};

	$http.get('api/news/' + idnews).success(function(data) {
		$scope.curnews = data;
	}).error(function(error) {
		console.error(error);
	});
});