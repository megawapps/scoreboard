String.prototype.replaceAll = function(find, replace) {
	var str = this;
	return str.replace(new RegExp(find, 'g'), replace);
};
if (!String.prototype.encodeHTML) {
	String.prototype.encodeHTML = function() {
		return this.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
	};
}
if (typeof String.prototype.startsWith != 'function') {
	// see below for better implementation!
	String.prototype.startsWith = function(str) {
		return this.indexOf(str) == 0;
	};
}
/**
 * @param heading
 * @param question
 * @param callback
 */
function modalConfirm(heading, question, callback) {
	$("#modal-title").html(heading);
	$("#modal-body").html(question);
	$("#modal-ok-btn").click(function(event) {
		callback();
		$("#myModal").modal('hide');
	});
	$("#myModal").modal('show');
};

/**
 * @param heading
 * @param content
 */
function modalDetail(heading, content) {
	$("#Detailmodal-title").html(heading);
	$("#Detailmodal-body").html(content);
	$("#myDetailModal").modal('show');
};

/**
 * @param error
 */
function showError(error) {
	$("#ok-message").hide();
	$("#error-message").html('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + error);
	$("#error-message").show();
}

/**
 * @param message
 */
function showMessage(message) {
	$("#error-message").hide();
	$("#ok-message").html('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + message);
	$("#ok-message").show();
}

/**
 * @param p
 */
function parsePlateformIcon(p) {
	var src = "";
	if (!p) {
		return src;
	}
	var ptab = p.split(":");
	for ( var i = 0; i < ptab.length; i++) {
		if ("Android" == ptab[i]) {
			src += ' <img  src="imgs/android.png" alt="Android" />';
		}
		if ("IPhone" == ptab[i]) {
			src += ' <img  src="imgs/apple.png" alt="IPhone"/>';
		}
		if ("Web" == ptab[i]) {
			src += ' <img  src="imgs/web.png" alt="Web"/>';
		}
	}
	return src;
}

function ensureP(txt) {
	if (txt.startsWith('<p>')) {
		return txt + '</p>';
	} else {
		return txt;
	}
}
