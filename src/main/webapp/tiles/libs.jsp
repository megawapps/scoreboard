<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="com.megawapps.scoreboard.i18n.text" />
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><fmt:message key="main.title" /></title>

<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/angular-csp.css" rel="stylesheet"/>
<link href="css/animations.css" rel="stylesheet"/>
<link href="css/toastr.css" rel="stylesheet"/>
<link href="css/theme.css" rel="stylesheet" media="screen">

<script type="text/javascript" src="js/libs/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="js/libs/tinymce/jquery.tinymce.min.js"></script>   

<script type="text/javascript" src="js/libs/bootstrap.min.js"></script>
<script type="text/javascript" src="js/libs/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="js/libs/sugar.min.js"></script>
<script type="text/javascript" src="js/libs/toastr.min.js"></script>
<script type="text/javascript" src="js/tools.js"></script>

<script type="text/javascript" src="js/libs/angular.min.js"></script>
<script type="text/javascript" src="js/libs/angular-animate.min.js"></script>
<script type="text/javascript" src="js/libs/angular-resource.min.js"></script>
<script type="text/javascript" src="js/libs/angular-route.min.js"></script>
<script type="text/javascript" src="js/libs/angular-sanitize.min.js"></script>
<script type="text/javascript" src="js/libs/angular-translate.min.js"></script>
<script type="text/javascript" src="js/libs/angular-translate-loader-url.min.js"></script>

<script type="text/javascript" src="js/i18n/angular-locale_fr.js"></script>
<script type="text/javascript" src="js/i18n/angular-locale_fr-fr.js"></script>
<script type="text/javascript" src="js/i18n/angular-locale_en.js"></script>

<script type="text/javascript" src="js/libs/ng-upload.js"></script>

<script type="text/javascript" src="js/ngapps/tools/eventBus.js"></script>
<script type="text/javascript" src="js/ngapps/tools/utilities.js"></script>
<script type="text/javascript" src="js/ngapps/public/login.js"></script>



<style type="text/css">
.css-form input.ng-invalid.ng-dirty {
	background-color: #FA787E;
}

.css-form input.ng-valid.ng-dirty {
	background-color: #78FA89;
}
</style>