<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="navbar navbar-inverse navbar-static-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.jsp"><span class="glyphicon glyphicon-tower"></span> MegaWapps</a>
		</div>
		<div class="collapse navbar-collapse">
			<div sb-login-form page-name="${param.curPage}"></div>
		</div>
	</div>
</div>