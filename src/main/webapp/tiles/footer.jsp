<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="language"
	value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
	scope="session" />
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="com.megawapps.scoreboard.i18n.text" />
<div id="footer">
	<div class="container">
		<div class="pull-left">
			&copy; <a href="http://www.megawapps.com">Megawapp</a> 2013. | <a href="index.jsp#/about"><fmt:message key="link.about" /></a>
		</div>
		<div class="pull-right">
			<a href="#"><img src="imgs/twitter.png" /></a> <a href="#"><img src="imgs/google-plus.png" /></a> <a href="#"><img
				src="imgs/facebook.png" /></a>
		</div>
	</div>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41593372-2', 'scoreboard-megawapps.rhcloud.com');
  ga('send', 'pageview');

</script>